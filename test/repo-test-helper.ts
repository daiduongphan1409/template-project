/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */
import { Repository } from 'typeorm';

/* istanbul ignore file */
export const repositoryMockFactory: () => MockType<Repository<any>> = jest.fn(() => ({
  find: jest.fn((entity) => entity),
  getAllValid: jest.fn((entity) => entity),
  // ...
}));

/* istanbul ignore file */
export type MockType<T> = {
  [P in keyof T]?: jest.Mock<{}>;
};
