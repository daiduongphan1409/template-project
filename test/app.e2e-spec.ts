import { INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientKafka } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import { getKafkaConfigs } from 'src/utils/event';
import * as request from 'supertest';
jest.setTimeout(60000);

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let kafkaClient: ClientKafka;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = await moduleFixture.createNestApplication();

    const configService = await app.get(ConfigService);

    await app.connectMicroservice({ ...getKafkaConfigs(configService) });

    await app.startAllMicroservices();
    await app.init();

    kafkaClient = moduleFixture.get<ClientKafka>('KAFKA_DEFAULT_CLIENT');
    await kafkaClient.connect();

    await app.listen(3000);
  });

  afterAll(async () => {
    await app.close();
    await kafkaClient.close();
  });

  it('should be defined', () => {
    expect(app).toBeDefined();
  });
  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/').expect(404);
  });

  it('/ (GET) Template', () => {
    return request(app.getHttpServer())
      .get('/template')
      .expect(200)
      .expect((res) => {
        res.body.data = [];
      });
  });
  it('/ (POST) Template', () => {
    return request(app.getHttpServer())
      .post('/template')
      .send({ title: 'Test title' })
      .expect(201)
      .expect((res) => {
        res.body.data = [];
      });
  });
  it('/ (POST) Template with empty title', () => {
    return request(app.getHttpServer())
      .post('/template')
      .send({ title: '' })
      .expect(400) //Bad Request
      .expect((res) => {
        res.body.data = [];
      });
  });
});
