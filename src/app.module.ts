/* istanbul ignore file */
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RepositoryModule } from './shared/repository/repository.module';
import { ApiModule } from './shared/api/api.module';
import { OpusContainerDomainModule } from './opus-container-domain/opus-container-domain.module';
import { OpusContainerIotModule } from './opus-container-iot/opus-container-iot.module';
import { validate } from 'src/utils/config/env.validation';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validate: validate,
    }),
    OpusContainerDomainModule,
    OpusContainerIotModule,
    RepositoryModule,
    ApiModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
