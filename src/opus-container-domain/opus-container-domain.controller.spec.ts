import { KafkaContext } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { Consumer, KafkaMessage, Producer } from 'kafkajs';
import { ApiResponse } from 'src/utils/api.response';
import { DistributePayload } from './dto/distribute-payload.dto';
import { OpusContainerDomainController } from './opus-container-domain.controller';
import { OpusContainerDomainService } from './service/opus-container-domain.service';

const args = [
  'test',
  { test: true },
  undefined,
  { test: 'consumer' },
  () => {}, // eslint-disable-line
  { test: 'producer' },
];
// const context = null;
const context = new KafkaContext(args as [KafkaMessage, number, string, Consumer, () => Promise<void>, Producer]);

describe('OpusContainerDomainController', () => {
  let controller: OpusContainerDomainController;
  const type = OpusContainerDomainController.prototype;
  const mockServiceExecute = jest.fn();
  const jestMockResolveData = ApiResponse.success<void>();

  beforeEach(async () => {
    mockServiceExecute.mockReset();
    mockServiceExecute.mockReturnValue(jestMockResolveData);

    const module: TestingModule = await Test.createTestingModule({
      controllers: [OpusContainerDomainController],
      providers: [
        {
          provide: OpusContainerDomainService,
          useFactory: () => ({
            execute: mockServiceExecute,
          }),
        },
        {
          provide: 'KAFKA_DEFAULT_CLIENT',
          useFactory: () => ({
            connect: jest.fn().mockReturnValue(true),
          }),
        },
      ],
    }).compile();

    controller = module.get<OpusContainerDomainController>(OpusContainerDomainController);
  });

  it(`${type.distributeContainerEvent.name}_Should_Success_Defined`, () => {
    //Assert
    expect(controller).toBeDefined();
  });

  it(`${type.distributeContainerEvent.name}_Should_Success_Request_When_Data_is_Correct`, async () => {
    //Arrange
    const dto = createOpusCdcPayloadDto('1', 'topic_test');

    //Act
    await controller.distributeContainerEvent(dto, context);

    //Assert
    expect(mockServiceExecute.mock.calls).toHaveLength(1);
  });
  it(`${type.distributeContainerEvent.name}_Should_ThrowValidateException_When_Payload_is_not_correct`, async () => {
    //Arrange
    const payloadTableNameNull = new DistributePayload({
      parent_id: '47024b74-46b9-43d3-b937-01dfc2365123',
      event_id: '47024b74-46b9-43d3-b937-01dfc2365489',
      origin_topic: 'OPS-TRS01_T_EDH_TRS01',
      origin_data: {
        metadata: {
          TableID: 49,
          TableName: null,
          TxnID: '',
          OperationName: 'UPDATE',
          FileName: '',
          FileOffset: 0,
          TimeStamp: JSON.stringify(new Date()),
          'Oracle ROWID': '',
          CSN: '',
          RecordStatus: '',
        },
        data: {
          BKG_STS_CD: '123',
        },
        before: {
          BKG_STS_CD: '1234',
        },
        userdata: {
          current_time: '2022-06-21T13:32:28.775+08:00',
        },
        __striimmetadata: {
          position: null,
        },
      },
      change_columns: ['BKG_STS_CD'],
    });
    mockServiceExecute.mockRejectedValue(new Error('Async error'));

    //Act
    await controller.distributeContainerEvent(payloadTableNameNull, context);

    //Assert
    await expect(mockServiceExecute()).rejects.toThrow(Error);
  });

  it(`${type.distributeContainerEvent.name}_Should_NotCallService_WhenPayloadNull`, async () => {
    //Arrange
    //Act
    const result = controller.distributeContainerEvent(null, context);

    //Assert
    await expect(result).rejects.toThrow('Payload is null');
  });

  it(`${type.onModuleInit.name}_Should_Run_When_Kafka_is_ready`, async () => {
    //Act
    const result = controller.onModuleInit();

    //Assert
    await expect(result).resolves.toBeUndefined();
  });
});

function createOpusCdcPayloadDto(id: string, topic: string, changedColumn = undefined, data = undefined) {
  const dto = new DistributePayload();
  dto.event_id = id;
  dto.origin_topic = topic;
  if (!changedColumn) {
    dto.change_columns = ['BKG_STS_CD'];
  } else {
    dto.change_columns = changedColumn;
  }
  if (!data) {
    dto.event_id = '47024b74-46b9-43d3-b937-01dfc2365489';
    dto.origin_topic = 'OPS-TRS01_T_EDH_TRS01';
    dto.origin_data = {
      metadata: {
        TableID: 49,
        TableName: 'OPUSADM.BKG_BOOKING',
        TxnID: '',
        OperationName: 'UPDATE',
        FileName: '',
        FileOffset: 0,
        TimeStamp: JSON.stringify(new Date()),
        'Oracle ROWID': '',
        CSN: '',
        RecordStatus: '',
      },
      data: {
        BKG_STS_CD: '123',
      },
      before: {
        BKG_STS_CD: '1234',
      },
      userdata: {
        current_time: '2022-06-21T13:32:28.775+08:00',
      },
      __striimmetadata: {
        position: null,
      },
    };
  } else {
    dto.origin_data = data;
  }
  return dto;
}
