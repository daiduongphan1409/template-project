import { Test, TestingModule } from '@nestjs/testing';
import { ContainerDomainLog, EventMatrixFilter, EventMatrixLogic } from 'src/shared/domain/entity';
import { ContainerDomainLogState } from 'src/shared/domain/model/enum/filter-log-state.enum';
import { Trigger } from 'src/shared/domain/model/enum/matrix.enum';
import { ContainerDomainConfigRepositoryName } from 'src/shared/domain/repository/container-domain-config.interface';
import { ContainerDomainLogRepositoryName } from 'src/shared/domain/repository/container-domain-log.interface';
import { DistributePayload } from '../dto/distribute-payload.dto';
import { MatrixProcessorName } from '../matrix-processor/matrix-processor';
import { OpusContainerDomainService } from './opus-container-domain.service';

describe(`${OpusContainerDomainService.name}`, () => {
  const type = OpusContainerDomainService.prototype;
  const payload = new DistributePayload({
    parent_id: '47024b74-46b9-43d3-b937-01dfc2365123',
    event_id: '47024b74-46b9-43d3-b937-01dfc2365489',
    origin_topic: 'OPS-TRS01_T_EDH_TRS01',
    origin_data: {
      metadata: {
        TableID: 49,
        TableName: 'OPUSADM.BKG_BOOKING',
        TxnID: '',
        OperationName: 'UPDATE',
        FileName: '',
        FileOffset: 0,
        TimeStamp: JSON.stringify(new Date()),
        'Oracle ROWID': '',
        CSN: '',
        RecordStatus: '',
      },
      data: {
        BKG_STS_CD: '123',
      },
      before: {
        BKG_STS_CD: '1234',
      },
      userdata: {
        current_time: '2022-06-21T13:32:28.775+08:00',
      },
      __striimmetadata: {
        position: null,
      },
    },
    change_columns: ['BKG_STS_CD'],
  });

  const eventMatrixFilter = [
    new EventMatrixFilter({
      id: 1,
      matrixId: '1',
      triggerId: Trigger.ACTUAL,
      tableName: 'BKG_BOOKING',
      columnName: 'BKG_STS_CD',
      isValid: true,
    }),
    new EventMatrixFilter({
      id: 2,
      matrixId: '2',
      triggerId: Trigger.ESTIMATE,
      tableName: 'BKG_BOOKING',
      columnName: 'BKG_STS_CD',
      isValid: true,
    }),
  ];
  const containerDomainLog = new ContainerDomainLog({
    eventId: 'fjkdjfkdf',
    matrixMatch: [],
    matrixPotential: [],
    createdTime: new Date(),
    updatedTime: new Date(),
    state: ContainerDomainLogState.NEW,
  });

  const eventMatrixLogic = [
    new EventMatrixLogic({
      logicType: 1,
      triggerId: 1,
      matrixId: '1',
      description: 'string',
      script: 'string',
      isValid: true,
      filters: eventMatrixFilter,
    }),
  ];
  let service: OpusContainerDomainService;
  const mockMatrixProcessorExecute = jest.fn();
  const mockContainerDomainLogRepositorySave = jest.fn();
  const mockContainerDomainConfigRepositoryGetAllMatrixFilter = jest.fn();
  const mockContainerDomainConfigRepositoryGetAllMatrixLogic = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OpusContainerDomainService,
        {
          provide: MatrixProcessorName,
          useFactory: () => ({
            execute: mockMatrixProcessorExecute,
          }),
        },
        {
          provide: ContainerDomainLogRepositoryName,
          useFactory: () => ({
            save: mockContainerDomainLogRepositorySave.mockReturnValue(containerDomainLog),
          }),
        },
        {
          provide: ContainerDomainConfigRepositoryName,
          useFactory: () => ({
            getAllMatrixFilter: mockContainerDomainConfigRepositoryGetAllMatrixFilter,
            getAllMatrixLogic: mockContainerDomainConfigRepositoryGetAllMatrixLogic,
          }),
        },
      ],
    }).compile();

    service = module.get<OpusContainerDomainService>(OpusContainerDomainService);
  });

  it(`${type.execute.name}__Should_matrix_processor_excute_when_data_correct`, async () => {
    //Arrange
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue(eventMatrixFilter);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue(eventMatrixLogic);

    //Act
    await service.execute(payload);

    //Assert
    expect(mockMatrixProcessorExecute.mock.calls).toHaveLength(1);
  });

  it(`${type.execute.name}__Should_ThrowValidateException_When_Payload_Is_Null`, async () => {
    //Arrange
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue(eventMatrixFilter);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue(eventMatrixLogic);

    //Act
    const result = service.execute(null);

    //Assert
    await expect(result).rejects.toThrow('Payload invalid');
  });

  it(`${type.execute.name}__Should_ThrowValidateException_When_Get_Matrix_Filter_Falied`, async () => {
    //Arrange
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue([]);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue(eventMatrixLogic);

    //Act
    const result = service.execute(payload);

    //Assert
    await expect(result).rejects.toThrow('Get matrix filter failed');
  });

  it(`${type.execute.name}__Should_ThrowValidateException_When_Get_Target_Falied`, async () => {
    //Arrange
    const payloadFaildTableName = new DistributePayload({
      parent_id: '47024b74-46b9-43d3-b937-01dfc2365123',
      event_id: '47024b74-46b9-43d3-b937-01dfc2365489',
      origin_topic: 'OPS-TRS01_T_EDH_TRS01',
      origin_data: {
        metadata: {
          TableID: 49,
          TableName: 'OPUSADM.a',
          TxnID: '',
          OperationName: 'UPDATE',
          FileName: '',
          FileOffset: 0,
          TimeStamp: JSON.stringify(new Date()),
          'Oracle ROWID': '',
          CSN: '',
          RecordStatus: '',
        },
        data: {
          BKG_STS_CD: '123',
        },
        before: {
          BKG_STS_CD: '1234',
        },
        userdata: {
          current_time: '2022-06-21T13:32:28.775+08:00',
        },
        __striimmetadata: {
          position: null,
        },
      },
      change_columns: ['BKG_STS_CD'],
    });
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue(eventMatrixFilter);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue(eventMatrixLogic);

    //Act
    const result = service.execute(payloadFaildTableName);

    //Assert
    await expect(result).rejects.toThrow('Not found target for event');
  });

  it(`${type.execute.name}__Should_ThrowValidateException_When_TableName_is _null`, async () => {
    //Arrange
    const payloadTableNameNull = new DistributePayload({
      parent_id: '47024b74-46b9-43d3-b937-01dfc2365123',
      event_id: '47024b74-46b9-43d3-b937-01dfc2365489',
      origin_topic: 'OPS-TRS01_T_EDH_TRS01',
      origin_data: {
        metadata: {
          TableID: 49,
          TableName: null,
          TxnID: '',
          OperationName: 'UPDATE',
          FileName: '',
          FileOffset: 0,
          TimeStamp: JSON.stringify(new Date()),
          'Oracle ROWID': '',
          CSN: '',
          RecordStatus: '',
        },
        data: {
          BKG_STS_CD: '123',
        },
        before: {
          BKG_STS_CD: '1234',
        },
        userdata: {
          current_time: '2022-06-21T13:32:28.775+08:00',
        },
        __striimmetadata: {
          position: null,
        },
      },
      change_columns: ['BKG_STS_CD'],
    });
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue(eventMatrixFilter);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue([]);

    //Act
    const result = service.execute(payloadTableNameNull);

    //Assert
    await expect(result).rejects.toThrow('Table is null');
  });

  it(`${type.execute.name}__Should_ThrowValidateException_When_Get_Target_Falied`, async () => {
    //Arrange
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue(eventMatrixFilter);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue([]);

    //Act
    const result = service.execute(payload);

    //Assert
    await expect(result).rejects.toThrow('Get matrix logic failed');
  });

  it(`${type.execute.name}__Should_ThrowValidateException_When_Potentialevents_is_null`, async () => {
    //Arrange
    const eventMatrixFilterFaild = [
      new EventMatrixFilter({
        id: 1,
        matrixId: '4',
        triggerId: Trigger.ACTUAL,
        tableName: 'BKG_BOOKING',
        columnName: 'BKG_STS_CD',
        isValid: true,
      }),
      new EventMatrixFilter({
        id: 2,
        matrixId: '3',
        triggerId: Trigger.ESTIMATE,
        tableName: 'BKG_BOOKING',
        columnName: 'BKG_STS_CD',
        isValid: true,
      }),
    ];
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue(eventMatrixFilterFaild);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue(eventMatrixLogic);

    //Act
    const result = service.execute(payload);

    //Assert
    await expect(result).rejects.toThrow('Potentialevents is null');
  });

  it(`__Should_ThrowValidateException_When_Get_Target_Falied`, async () => {
    //Arrange
    const eventMatrixFilterFaild = [
      new EventMatrixFilter({
        id: 1,
        matrixId: '4',
        triggerId: Trigger.ACTUAL,
        tableName: 'BKG_BOOKING',
        columnName: 'BKG_STS_CD',
        isValid: true,
      }),
      new EventMatrixFilter({
        id: 2,
        matrixId: '3',
        triggerId: Trigger.ESTIMATE,
        tableName: 'BKG_BOOKING',
        columnName: 'BKG_STS_CD',
        isValid: true,
      }),
    ];
    mockContainerDomainConfigRepositoryGetAllMatrixFilter.mockReturnValue(eventMatrixFilterFaild);
    mockContainerDomainConfigRepositoryGetAllMatrixLogic.mockReturnValue(eventMatrixLogic);

    //Act
    const result = service.execute(payload);

    //Assert
    await expect(result).rejects.toThrow('Potentialevents is null');
  });
});
