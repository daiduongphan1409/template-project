import { Inject, Injectable, Logger } from '@nestjs/common';
import { ContainerDomainLog, EventMatrixFilter, EventMatrixLogic } from 'src/shared/domain/entity';
import { ContainerDomainLogState } from 'src/shared/domain/model/enum/filter-log-state.enum';
import { ValidateException } from 'src/shared/domain/model/exception';
import { ContainerDomainLogRepository, ContainerDomainLogRepositoryName } from 'src/shared/domain/repository/container-domain-log.interface';
import { ErrorCode } from 'src/utils/error.code';
import { DistributePayload } from '../dto/distribute-payload.dto';
import { MatrixProcessor, MatrixProcessorName } from '../matrix-processor/matrix-processor';
import { ContainerDomainConfigRepositoryName, ContainerDomainConfigRepository } from 'src/shared/domain/repository/container-domain-config.interface';
import { EvaluationResult, EventMatrixDto, EventRequest } from '../dto';

@Injectable()
export class OpusContainerDomainService {
  private logger = new Logger(OpusContainerDomainService.name);

  constructor(
    @Inject(MatrixProcessorName)
    private readonly matrixProcessor: MatrixProcessor,
    @Inject(ContainerDomainLogRepositoryName)
    private readonly containerDomainLogRepository: ContainerDomainLogRepository,
    @Inject(ContainerDomainConfigRepositoryName)
    private readonly eventRepository: ContainerDomainConfigRepository,
  ) {}

  public async execute(payload: DistributePayload): Promise<void> {
    const containerLog = await this.storeEventLog(payload, [], []);
    if (!containerLog || !payload) {
      throw new ValidateException(ErrorCode.PAYLOAD_INVALID, 'Payload invalid');
    }
    if (payload.origin_data?.metadata && !payload.origin_data.metadata.TableName) {
      await this.containerDomainLogRepository.save(this.setState(containerLog, ContainerDomainLogState.REJECT));
      throw new ValidateException(ErrorCode.TABLE_IS_NULL, 'Table is null');
    }
    const filterConfigs = await this.eventRepository.getAllMatrixFilter();
    if (!filterConfigs || filterConfigs.length <= 0) {
      await this.containerDomainLogRepository.save(this.setState(containerLog, ContainerDomainLogState.REJECT));
      throw new ValidateException(ErrorCode.GET_MATRIX_FILTER_FAILED, 'Get matrix filter failed');
    }

    const targets = this.getTargets(payload, filterConfigs);
    if (!targets || targets.length <= 0) {
      await this.containerDomainLogRepository.save(this.setState(containerLog, ContainerDomainLogState.REJECT));
      throw new ValidateException(ErrorCode.NOT_FOUND_TARGET_FOR_EVENT, 'Not found target for event');
    }

    const logicConfigs = await this.eventRepository.getAllMatrixLogic();
    if (!logicConfigs || logicConfigs.length <= 0) {
      await this.containerDomainLogRepository.save(this.setState(containerLog, ContainerDomainLogState.REJECT));
      throw new ValidateException(ErrorCode.GET_MATRIX_LOGIC_FAILED, 'Get matrix logic failed');
    }

    const potentialEvents = this.getPotentialEvent(targets, logicConfigs);
    const request = this.getRequest(payload);

    if (!potentialEvents || potentialEvents.length <= 0) {
      throw new ValidateException(ErrorCode.POTENTIALEVENTS_IS_NULL, 'Potentialevents is null');
    }
    containerLog.matrixPotential = potentialEvents;
    try {
      const results = await this.matrixProcessor.execute(potentialEvents, request);
      for (const result in results) {
        containerLog.matrixMatch.push(results[result]);
      }
      await this.containerDomainLogRepository.save(this.setState(containerLog, ContainerDomainLogState.ACCEPT));
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(`this event run have been error ${error.message}`);
    }
  }

  private async storeEventLog(
    payload: DistributePayload,
    matrixMatch: EvaluationResult[],
    matrixPotential: EventMatrixDto[],
  ): Promise<ContainerDomainLog> {
    try {
      if (!payload) return null;
      const log = new ContainerDomainLog({
        eventId: payload.event_id,
        matrixMatch: matrixMatch,
        matrixPotential: matrixPotential,
        createdTime: new Date(),
        updatedTime: new Date(),
        state: ContainerDomainLogState.NEW,
      });
      return await this.containerDomainLogRepository.save(log);
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(`Process store event error , Message: ${error.message}`, error.stack);
      return null;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private getTargets(payload: DistributePayload, configs: EventMatrixFilter[]): any {
    const tableName = this.getTableName(payload);
    const configsFilterByTableName = configs.filter((x) => x.tableName === tableName);
    const columnChange = payload.change_columns || [];
    const targetDomain = configsFilterByTableName
      .filter((x) => columnChange.includes(x.columnName))
      .map((x) => {
        return {
          matrixId: x.matrixId,
          triggerId: x.triggerId,
        };
      });
    return targetDomain;
  }

  private getTableName(payload: DistributePayload): string {
    const tableName = payload.origin_data.metadata.TableName.split('.');
    return tableName[tableName.length - 1];
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private getPotentialEvent(listId: any, configs: EventMatrixLogic[]): EventMatrixDto[] {
    const listLogic = configs.filter((x) => listId.some((y) => y.matrixId === x.matrixId && y.triggerId === x.triggerId));
    const lsResult = listLogic.map((logic) => {
      return new EventMatrixDto({
        logicType: logic.logicType,
        matrixId: logic.matrixId,
        script: logic.script,
        trigger: logic.triggerId,
      });
    });
    return lsResult;
  }

  private getRequest(payload: DistributePayload): EventRequest {
    const bookingNo = Object.fromEntries(Object.entries(payload.origin_data.data).filter(([key]) => key === 'BKG_NO'));
    const containerNo = Object.fromEntries(Object.entries(payload.origin_data.data).filter(([key]) => key === 'CNTR_NO'));
    const getReq = new EventRequest({
      parentId: payload.parent_id,
      eventId: payload.event_id,
      originTopic: payload.origin_topic,
      originData: payload.origin_data,
      bookingNo: bookingNo['BKG_NO'] ? bookingNo['BKG_NO'] : null,
      containerNo: containerNo['CNTR_NO'] ? containerNo['CNTR_NO'] : null,
    });

    return getReq;
  }

  private setState(log: ContainerDomainLog, state: ContainerDomainLogState): ContainerDomainLog {
    log.state = state;
    return log;
  }
}
