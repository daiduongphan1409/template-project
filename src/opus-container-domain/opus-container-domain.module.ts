import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientsModule } from '@nestjs/microservices';
import { configKafkaEventFactory, KAFKA_CLIENT } from 'src/utils/event';
import { MatrixProcessorModule } from './matrix-processor/matrix-processor.module';
import { OpusContainerDomainController } from './opus-container-domain.controller';
import { OpusContainerDomainService } from './service/opus-container-domain.service';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: KAFKA_CLIENT,
        useFactory: configKafkaEventFactory,
        inject: [ConfigService],
      },
    ]),
    MatrixProcessorModule,
  ],
  providers: [OpusContainerDomainService],
  controllers: [OpusContainerDomainController],
})
export class OpusContainerDomainModule {}
