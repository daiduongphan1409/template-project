/* eslint-disable no-await-in-loop */
import { EvaluationResult, EventMatrixDto, EventRequest, MatrixResult } from 'src/opus-container-domain/dto';
import { MatrixEvaluator } from '../interface';
import { DedicatedLogicEvaluator } from '../interface/dedicated-logic-evaluator.interface';

export class DedicatedMatrixEvaluator implements MatrixEvaluator {
  async execute(matrices: EventMatrixDto[], request: EventRequest): Promise<EvaluationResult> {
    const matrixResults: MatrixResult[] = [];

    for (const matrix of matrices) {
      if (matrix) {
        const evaluator = this.getDedicatedLogicEvaluator(matrix);
        if (evaluator) {
          matrixResults.push(await evaluator.execute(matrix, request));
        }
      }
    }

    return new EvaluationResult({ matrixResults: matrixResults });
  }

  private getDedicatedLogicEvaluator(matrix: EventMatrixDto): DedicatedLogicEvaluator {
    //Choose logic base on matrix id (matrix code)
    switch (matrix.matrixId) {
      default:
        return null;
    }
  }
}
