import { Test, TestingModule } from '@nestjs/testing';
import { CdcEventPayload, EvaluationResult, EventMatrixDto, EventRequest } from 'src/opus-container-domain/dto';
import { Any } from 'typeorm';
import { DedicatedMatrixEvaluator } from './dedicated-matrix-evaluator';

describe(`${DedicatedMatrixEvaluator.name}`, () => {
  const eventDataMatrix = [
    new EventMatrixDto({
      logicType: 1,
      trigger: 1,
      matrixId: '01',
      script: 'string',
    }),
    new EventMatrixDto({
      logicType: 1,
      trigger: 1,
      matrixId: '02',
      script: 'string',
    }),
  ];
  const requestData = new EventRequest({
    parentId: 'string',
    eventId: 'string',
    originTopic: 'string',
    originData: new CdcEventPayload(),
    containerNo: '02',
  });
  let target: DedicatedMatrixEvaluator;
  const type = DedicatedMatrixEvaluator.prototype;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DedicatedMatrixEvaluator,
        {
          provide: Any,
          useFactory: Any,
        },
      ],
    }).compile();

    target = module.get(DedicatedMatrixEvaluator);
  });

  it(`${type.execute.name}_should_return_null_when_eventdatamatrix_undefined`, async () => {
    //Arrange
    //Act
    const result = await target.execute(eventDataMatrix, requestData);
    //Assert
    expect(result).toEqual(new EvaluationResult({ matrixResults: [] }));
  });
});
