export const PostActionInjectorName = 'PostActionInjector.Interface';

export interface ScriptInjector {
  append(script: string): string;
}
