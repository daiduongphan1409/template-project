import { EventMatrixDto } from 'src/opus-container-domain/dto';
import { EventRequest } from 'src/opus-container-domain/dto/event-request.dto';
import { MatrixResult } from 'src/opus-container-domain/dto/matrix-result.dto';

export interface DedicatedLogicEvaluator {
  execute(matrix: EventMatrixDto, request: EventRequest): Promise<MatrixResult>;
}
