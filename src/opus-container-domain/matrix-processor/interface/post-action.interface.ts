import { Context } from 'src/opus-container-domain/dto';
import { LoaderTask } from './loader-task.interface';

export interface PostAction {
  getLoaderTasks(): LoaderTask[];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  execute(context: Context, action: any): Promise<void>;

  isExist(suggestActions: string[], logic: string): boolean;

  getInjectScript(): string;

  getName(): string;
}
