import { EventMatrixDto } from 'src/opus-container-domain/dto';
import { LogicAnalyzeResult } from 'src/opus-container-domain/dto/logic-analyze-result.dto';

export const DefaultMatrixLogicAnalyzerName = 'MatrixLogicAnalyzer.Interface';

export interface MatrixLogicAnalyzer {
  execute(event: EventMatrixDto): LogicAnalyzeResult;
}
