import { EventRequest } from 'src/opus-container-domain/dto/event-request.dto';
import { EventMatrixDto } from 'src/opus-container-domain/dto';
import { LoaderTask } from './loader-task.interface';

export const DefaultLoaderTaskCollectorName = 'LoaderTaskCollectorName.Interface';

export interface LoaderTaskCollector {
  collect(eventMatrix: EventMatrixDto, request: EventRequest): LoaderTask[];
}
