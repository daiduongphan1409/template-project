import { EventRequest } from 'src/opus-container-domain/dto';
import { Context } from 'src/opus-container-domain/dto/context.dto';
import { LoaderContext } from '../dynamic/loader/loader-context';

export const BkgClzTmLoaderName = 'BkgClzTmLoaderName.Interface';
export const SceCopDtlLoaderName = 'SceCopDtloaderName.Interface';
export const SceCopHdrLoaderName = 'SceCopHdrLoaderName.Interface';
export const SceCopDtlsLoaderName = 'SceCopDtlsLoaderName.Interface';

export interface LoaderTask {
  load(context: Context, loader: LoaderContext, event: EventRequest): Promise<void>;
}
