import { LoaderTask } from './loader-task.interface';

export interface DefinedVariable {
  getLoaderTasks(): LoaderTask[];

  isExist(suggestVariables: string[], logic: string): boolean;

  getInjectScript(): string;

  getName(): string;
}
