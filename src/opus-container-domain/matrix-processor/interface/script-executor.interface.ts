import { Context } from 'src/opus-container-domain/dto/context.dto';
import { ScriptResult } from 'src/opus-container-domain/dto/script-result.dto';

export const DefaultScriptExecutorName = 'DefaultScriptExecutor.Interface';

export interface ScriptExecutor {
  execute(context: Context): ScriptResult;
}
