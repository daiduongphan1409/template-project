import { EvaluationResult, EventMatrixDto, EventRequest } from 'src/opus-container-domain/dto';

export const DynamicMatrixEvaluatorName = 'DynamicMatrixEvaluator.Interface';
export const DedicatedMatrixEvaluatorName = 'DedicatedMatrixEvaluator.Interface';

export interface MatrixEvaluator {
  execute(matrixs: EventMatrixDto[], request: EventRequest): Promise<EvaluationResult>;
}
