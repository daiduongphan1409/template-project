import { EventRequest } from '../../dto/event-request.dto';
import { Context } from 'src/opus-container-domain/dto/context.dto';
import { EventMatrixDto } from 'src/opus-container-domain/dto';

export const DefaultContextLoaderName = 'ContextLoaderName.Interface';

export interface ContextLoader {
    load(eventMatrix: EventMatrixDto[], request: EventRequest): Promise<Context[]>;
}
