export const DefaultScriptBuilderName = 'ScriptBuilder.DefaultScriptBuilderName';

export interface ScriptBuilder {
  build(script: string): string;
}
