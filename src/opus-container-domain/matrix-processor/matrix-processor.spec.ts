/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { LogicType } from 'src/shared/domain/model/enum/matrix.enum';
import { CdcEventPayload, Context, EvaluationResult, EventMatrixDto, EventRequest, MatrixResult } from '../dto';
import { PostActionFactoryName } from './dynamic/post-action';
import { DedicatedMatrixEvaluatorName, DynamicMatrixEvaluatorName, LoaderTask, PostAction } from './interface';
import { MatrixProcessor } from './matrix-processor';

function getContextWithDynamicMatrix() {
  const context = new Context({
    request: createEventRequest(),
    event: new EventMatrixDto({
      matrixId: 'E026',
      logicType: LogicType.DYNAMIC,
    }),
  });
  return context;
}

function getContextWithDedicatedMatrix() {
  const context = new Context({
    request: createEventRequest(),
    event: new EventMatrixDto({
      matrixId: 'E026',
      logicType: LogicType.DEDICATED,
    }),
  });
  return context;
}

function createEventRequest() {
  return new EventRequest({
    parentId: 'parentId',
    eventId: 'eventId',
    originTopic: '',
    bookingNo: '',
    containerNo: '',
  });
}

describe(`${MatrixProcessor.name}`, () => {
  const fakeName = 'fakeName';
  let target: MatrixProcessor;
  const type = MatrixProcessor.prototype;
  const mockDedicatedExecute = jest.fn();
  const mockDynamicExecute = jest.fn();
  const mockPostActionFatory = jest.fn();
  const dynamicActions = [];
  const dedicatedActions = [];
  beforeEach(async () => {
    dynamicActions.splice(0);
    dedicatedActions.splice(0);
    mockDynamicExecute.mockReset();
    mockDynamicExecute.mockReturnValue(
      new EvaluationResult({
        matrixResults: [
          new MatrixResult({
            context: getContextWithDynamicMatrix(),
            actions: dynamicActions,
          }),
        ],
      }),
    );
    mockDedicatedExecute.mockReset();
    mockDedicatedExecute.mockReturnValue(
      new EvaluationResult({
        matrixResults: [
          new MatrixResult({
            context: getContextWithDedicatedMatrix(),
            actions: dedicatedActions,
          }),
        ],
      }),
    );
    mockPostActionFatory.mockReset();
    mockPostActionFatory.mockReturnValue(new FakePostAction());
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MatrixProcessor,
        {
          provide: DedicatedMatrixEvaluatorName,
          useFactory: () => ({
            execute: mockDedicatedExecute,
          }),
        },
        {
          provide: DynamicMatrixEvaluatorName,
          useFactory: () => ({
            execute: mockDynamicExecute,
          }),
        },
        {
          provide: PostActionFactoryName,
          useFactory: () => ({
            get: mockPostActionFatory,
          }),
        },
      ],
    }).compile();

    target = module.get(MatrixProcessor);
  });

  it(`${type.execute.name}_Should_Call_DedicatedExecute_One`, async () => {
    //Arrange
    const matrices = [new EventMatrixDto({ logicType: LogicType.DEDICATED, matrixId: 'E026', script: 'if() {}' })];
    const request = new EventRequest({
      bookingNo: 'HGDU98374OIRU',
      containerNo: 'UIOIR7837483',
      eventId: 'jij-ieỉui-974748403',
      originData: new CdcEventPayload({
        data: {
          BKG_NO: 'RICCPF024600',
          CLZ_TP_CD: 'R',
          MNL_SET_DT: '2022-11-16T12:00:00.000',
        },
      }),
    });

    //Act
    await target.execute(matrices, request);

    //Assert
    expect(mockDedicatedExecute.mock.calls).toHaveLength(1);
  });

  it(`${type.execute.name}_Should_Call_DynamicExecute_One`, async () => {
    //Arrange
    const matrices = [new EventMatrixDto({ logicType: LogicType.DYNAMIC, matrixId: 'E026', script: 'if() {}' })];
    const request = new EventRequest({
      bookingNo: 'HGDU98374OIRU',
      containerNo: 'UIOIR7837483',
      eventId: 'jij-ieỉui-974748403',
      originData: new CdcEventPayload({
        data: {
          BKG_NO: 'RICCPF024600',
          CLZ_TP_CD: 'R',
          MNL_SET_DT: '2022-11-16T12:00:00.000',
        },
      }),
    });

    //Act
    await target.execute(matrices, request);

    //Assert
    expect(mockDynamicExecute.mock.calls).toHaveLength(1);
  });

  it(`${type.execute.name}_Should_Call_Actions_Zero`, async () => {
    //Arrange
    const matrices = [new EventMatrixDto({ logicType: LogicType.DYNAMIC, matrixId: 'E026', script: 'if() {}' })];
    const request = new EventRequest({
      bookingNo: 'HGDU98374OIRU',
      containerNo: 'UIOIR7837483',
      eventId: 'jij-ieỉui-974748403',
      originData: new CdcEventPayload({
        data: {
          BKG_NO: 'RICCPF024600',
          CLZ_TP_CD: 'R',
          MNL_SET_DT: '2022-11-16T12:00:00.000',
        },
      }),
    });

    //Act
    await target.execute(matrices, request);

    //Assert
    expect(mockPostActionFatory.mock.calls).toHaveLength(0);
  });
  it(`${type.execute.name}_Should_Call_Actions_One`, async () => {
    //Arrange
    dynamicActions.push({ name: fakeName, parameters: [] });
    const matrices = [new EventMatrixDto({ logicType: LogicType.DYNAMIC, matrixId: 'E026', script: 'if() {}' })];
    const request = new EventRequest({
      bookingNo: 'HGDU98374OIRU',
      containerNo: 'UIOIR7837483',
      eventId: 'jij-ieỉui-974748403',
      originData: new CdcEventPayload({
        data: {
          BKG_NO: 'RICCPF024600',
          CLZ_TP_CD: 'R',
          MNL_SET_DT: '2022-11-16T12:00:00.000',
        },
      }),
    });

    //Act
    await target.execute(matrices, request);

    //Assert
    expect(mockPostActionFatory.mock.calls).toHaveLength(1);
  });
  it(`${type.execute.name}_Should_Call_Actions_Two`, async () => {
    //Arrange
    dynamicActions.push({ name: fakeName, parameters: [] });
    dedicatedActions.push({ name: fakeName, parameters: [] });
    const matrices = [new EventMatrixDto({ logicType: LogicType.DYNAMIC, matrixId: 'E026', script: 'if() {}' })];
    const request = new EventRequest({
      bookingNo: 'HGDU98374OIRU',
      containerNo: 'UIOIR7837483',
      eventId: 'jij-ieỉui-974748403',
      originData: new CdcEventPayload({
        data: {
          BKG_NO: 'RICCPF024600',
          CLZ_TP_CD: 'R',
          MNL_SET_DT: '2022-11-16T12:00:00.000',
        },
      }),
    });

    //Act
    await target.execute(matrices, request);

    //Assert
    expect(mockPostActionFatory.mock.calls).toHaveLength(2);
  });
  class FakePostAction implements PostAction {
    getLoaderTasks(): LoaderTask[] {
      throw new Error('Method not implemented.');
    }

    async execute(action: any): Promise<void> {
      return;
    }

    isExist(suggestActions: string[], logic: string): boolean {
      throw new Error('Method not implemented.');
    }

    getInjectScript(): string {
      throw new Error('Method not implemented.');
    }

    getName(): string {
      return fakeName;
    }
  }
});
