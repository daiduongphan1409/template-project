import { Test, TestingModule } from '@nestjs/testing';
import * as fs from 'fs';
import { ValidateException } from 'src/shared/domain/model/exception';
import { ScriptInjector } from '../../interface';
import { ScriptInjectors } from '../post-action';
import { DefaultScriptBuilder } from './default-script-builder';

describe(`${DefaultScriptBuilder.name}`, () => {
  let target: DefaultScriptBuilder;
  const type = DefaultScriptBuilder.prototype;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DefaultScriptBuilder,
        {
          provide: ScriptInjectors,
          useValue: [new FakePostActionInjector()],
        },
      ],
    }).compile();

    target = module.get(DefaultScriptBuilder);
  });
  it(`${type.build.name}_Should_Return_Script`, () => {
    //Arrange
    const script = 'Check the Script';
    jest.spyOn(fs, 'readFileSync').mockReturnValueOnce("'<<logic>>'");

    //Act
    const rs = target.build(script);

    //Assert
    expect(rs).toBe(script);
  });
  it(`${type.build.name}_Should_Throw_Exception_When_readFileSync_Empty`, async () => {
    //Arrange
    const script = 'Check the Script';
    jest.spyOn(fs, 'readFileSync').mockReturnValueOnce('');
    //Act

    //Assert
    expect(() => target.build(script)).toThrow('Base Script is null or empty');
  });
  it(`${type.build.name}_Should_Throw_Exception_When_readFileSync_Exception`, async () => {
    //Arrange
    const script = 'Check the Script';
    jest.spyOn(fs, 'readFileSync').mockImplementation(() => {
      throw new ValidateException(77837, 'nfhfhfhf');
    });
    //Act

    //Assert
    expect(() => target.build(script)).toThrow('Base Script is null or empty');
  });
  class FakePostActionInjector implements ScriptInjector {
    append(script: string): string {
      return script;
    }
  }
});
