import * as fs from 'fs';
import * as path from 'path';
import { Logger, Injectable, Inject } from '@nestjs/common';
import { ScriptBuilder, ScriptInjector } from '../../interface';
import { ScriptInjectors } from '../post-action';
import { ValidateException } from 'src/shared/domain/model/exception';
import { ErrorCode } from 'src/utils/error.code';

@Injectable()
export class DefaultScriptBuilder implements ScriptBuilder {
  constructor(@Inject(ScriptInjectors) private readonly scriptInjectors: ScriptInjector[]) {}

  public build(script: string): string {
    let baseScript = this.loadBaseScript();

    for (const injector of this.scriptInjectors) {
      baseScript = injector.append(baseScript);
    }

    return this.removeUseStrict(this.appendLogic(script, baseScript));
  }

  private loadBaseScript(): string {
    try {
      const script = fs.readFileSync(path.join(__dirname, 'base-file', 'event_base.js'), { encoding: 'utf-8' });
      if (script && script.length > 0) {
        return script;
      }
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Load base script error. Message: ${error.message}`, error.stack);
    }

    throw new ValidateException(ErrorCode.LOADSCRIPT_FILE_FAILED, 'Base Script is null or empty');
  }

  private appendLogic(script: string, base: string): string {
    return base.replace("'<<logic>>'", script);
  }

  private removeUseStrict(script: string) {
    return script.replace(/"use strict";/g, '');
  }
}
