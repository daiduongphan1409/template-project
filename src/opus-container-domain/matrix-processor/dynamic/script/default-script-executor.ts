import * as vm from 'node:vm';
import { Inject, Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common/services';
import { DefaultScriptBuilderName, ScriptBuilder, ScriptExecutor } from '../../interface';
import { Context, ScriptContext, ScriptResult } from 'src/opus-container-domain/dto';
import { ScriptResultAction } from 'src/opus-container-domain/dto/scrip-result-action.dto';

@Injectable()
export class DefaultScriptExecutor implements ScriptExecutor {
  private readonly logger = new Logger(DefaultScriptExecutor.name);

  constructor(
    @Inject(DefaultScriptBuilderName)
    private readonly scriptBuilder: ScriptBuilder,
  ) {}

  public execute(context: Context): ScriptResult {
    const scriptContext = this.createScriptContext(context);
    const script = this.scriptBuilder.build(context.event.script);

    return this.executeJavascript(script, scriptContext);
  }

  private createScriptContext(context: Context): ScriptContext {
    return new ScriptContext({
      request: context.request,
      event: context.event,
      vars: context.vars,
      logger: this.logger,
      actions: {},
      result: new ScriptResult({ event: context.event, actions: [] }),
    });
  }

  private executeJavascript(scriptText: string, scriptContext: ScriptContext): ScriptResult {
    try {
      const context = vm.createContext({ context: scriptContext });
      const script = new vm.Script(scriptText);
      script.runInContext(context);

      return new ScriptResult({
        event: scriptContext.event,
        actions: context['context']['result']['actions'] as ScriptResultAction[],
      });
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(
        `Execute script for matrix failed. Message: ${error.message}, MatrixId: ${scriptContext.event.matrixId}, Trigger: ${scriptContext.event.trigger}, Script: ${scriptContext.event.script}`,
        error.stack,
      );
    }

    return null;
  }
}
