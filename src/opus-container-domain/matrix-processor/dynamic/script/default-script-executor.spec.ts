import { Test } from '@nestjs/testing';
import { CdcEventPayload, Context, EventMatrixDto, EventRequest, MetaPayload } from 'src/opus-container-domain/dto';
import { LogicType } from 'src/shared/domain/model/enum/matrix.enum';
import { DefaultScriptBuilderName } from '../../interface/script-builder.interface';
import { DefaultScriptExecutor } from './default-script-executor';

describe(`${DefaultScriptExecutor.name}`, () => {
  const type = DefaultScriptExecutor.prototype;
  let target: DefaultScriptExecutor;
  const mockScriptReturn = jest.fn();
  beforeEach(async () => {
    mockScriptReturn.mockReset();
    const module = await Test.createTestingModule({
      providers: [
        DefaultScriptExecutor,
        {
          provide: DefaultScriptBuilderName,
          useFactory: () => ({
            build: mockScriptReturn,
          }),
        },
      ],
    }).compile();

    target = module.get(DefaultScriptExecutor);
  });
  it(`${type.execute.name}_Should_Return_Null`, () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
      }),
      request: new EventRequest({
        eventId: 'ccc-ffff98374',
        originData: new CdcEventPayload({
          metadata: new MetaPayload({
            TableName: 'HHHHHHH',
          }),
        }),
      }),
      vars: [''],
    });
    mockScriptReturn.mockReturnValue('Script not javascript');

    //Act
    const result = target.execute(context);

    //Assert
    expect(result).toBeNull();
  });
  it(`${type.execute.name}_Should_Return_With_No_Action`, () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
      }),
      request: new EventRequest({
        eventId: 'ccc-ffff98374',
      }),
      vars: {
        abc: 0,
      },
    });
    mockScriptReturn.mockReturnValue("if (context.vars['abc'] === 1) {context['result']['actions'].push('abc')}");

    //Act
    const result = target.execute(context);

    //Assert
    expect(result.actions).toHaveLength(0);
  });
  it(`${type.execute.name}_Should_Return_With_Have_Action`, () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
      }),
      request: new EventRequest({
        eventId: 'ccc-ffff98374',
      }),
      vars: {
        abc: 1,
      },
    });
    mockScriptReturn.mockReturnValue("if (context.vars['abc'] === 1) {context['result']['actions'].push('abc')}");

    //Act
    const result = target.execute(context);

    //Assert
    expect(result.actions).toHaveLength(1);
  });
});
