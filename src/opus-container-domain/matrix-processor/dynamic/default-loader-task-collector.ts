import { Inject } from '@nestjs/common';
import { EventMatrixDto, EventRequest } from 'src/opus-container-domain/dto';
import { InternalException } from 'src/shared/domain/model/exception';
import { ErrorCode } from 'src/utils/error.code';
import { DefaultMatrixLogicAnalyzerName, LoaderTask, LoaderTaskCollector, MatrixLogicAnalyzer } from '../interface';
import { PostActionFactory, PostActionFactoryName } from './post-action/post-action-factory';
import { DefinedVariableFactory, DefinedVariableFactoryName } from './variable/defined-variable-factory';

export class DefaultLoaderTaskCollector implements LoaderTaskCollector {
  constructor(
    @Inject(DefaultMatrixLogicAnalyzerName) private readonly matrixLogicAnalyzer: MatrixLogicAnalyzer,
    @Inject(DefinedVariableFactoryName) private readonly definedVariableFactory: DefinedVariableFactory,
    @Inject(PostActionFactoryName) private readonly postActionFactory: PostActionFactory,
  ) {}

  public collect(matrix: EventMatrixDto, request: EventRequest): LoaderTask[] {
    if (!matrix || !request) {
      throw new InternalException(ErrorCode.PARAMS_INVALID, 'Param should be not undefined');
    }
    const analyzedData = this.matrixLogicAnalyzer.execute(matrix);
    const definedVarLoadedTasks = this.getDefinedVariableLoaderTasks(matrix, analyzedData.variables);
    const postActionLoadedTasks = this.getPostActionLoaderTasks(matrix, analyzedData.functions);

    return [...definedVarLoadedTasks, ...postActionLoadedTasks];
  }

  private getDefinedVariableLoaderTasks(matrix: EventMatrixDto, suggestVars: string[]): LoaderTask[] {
    const definedVariableLoaderTasks: LoaderTask[] = [];
    const definedVariables = this.definedVariableFactory.getList();
    const existVariables = definedVariables.filter((p) => p.isExist(suggestVars, matrix.script));

    for (const item of existVariables) {
      definedVariableLoaderTasks.push(...item.getLoaderTasks());
    }

    return definedVariableLoaderTasks;
  }

  private getPostActionLoaderTasks(matrix: EventMatrixDto, suggestFuncs: string[]): LoaderTask[] {
    const postActionLoaderTasks: LoaderTask[] = [];
    const definedPostActions = this.postActionFactory.getList();
    const existFunctions = definedPostActions.filter((p) => p.isExist(suggestFuncs, matrix.script));

    for (const item of existFunctions) {
      postActionLoaderTasks.push(...item.getLoaderTasks());
    }

    return postActionLoaderTasks;
  }
}
