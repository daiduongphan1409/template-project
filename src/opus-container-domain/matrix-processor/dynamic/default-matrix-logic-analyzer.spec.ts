import { DefaultMatrixLogicAnalyzer } from './default-matrix-logic-analyzer';
import { Test, TestingModule } from '@nestjs/testing';
import { EventMatrixDto } from 'src/opus-container-domain/dto/event-matrix.dto';
import { LogicAnalyzeResult } from 'src/opus-container-domain/dto';

describe(`${DefaultMatrixLogicAnalyzer.name}`, () => {
  const logicAnalyzeResult = new LogicAnalyzeResult({
    variables: ['BKG_BOOKING.BKG_NO', 'asd.S_s', 'asd.actualDate', 'actualDate'],
    functions: ['storeActual'],
    tables: [
      {
        name: 'BKG_BOOKING',
        columns: ['BKG_NO'],
      },
      {
        name: 'asd',
        columns: ['S_s', 'actualDate'],
      },
    ],
  });

  let logic: DefaultMatrixLogicAnalyzer;
  const type = DefaultMatrixLogicAnalyzer.prototype;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DefaultMatrixLogicAnalyzer],
    }).compile();
    logic = module.get<DefaultMatrixLogicAnalyzer>(DefaultMatrixLogicAnalyzer);
  });

  it(`${type.execute.name}_Should_Run_When_DataCorrect`, () => {
    //Arrange
    const config = createMatrixConfig();

    //Act
    const result = logic.execute(config);

    //Assert
    expect(result).toEqual(logicAnalyzeResult);
  });

  it.each([undefined, null])(`${type.execute.name}_Should_Return_Empty_AnalyzeResult_When_Script_Is_Empty`, (script) => {
    //Arrange
    const config = createMatrixConfig();
    config.script = script;
    const emptyResult = new LogicAnalyzeResult({
      variables: [],
      tables: [],
      functions: [],
    });

    //Act
    const result = logic.execute(config);

    //Assert
    expect(result).toEqual(emptyResult);
  });

  it.each([undefined, null])(`${type.execute.name}_Should_Return_Empty_AnalyzeResult_When_Matrix_Object_Empty`, (matrixObj) => {
    //Arrange
    const emptyResult = new LogicAnalyzeResult({
      variables: [],
      tables: [],
      functions: [],
    });

    //Act
    const result = logic.execute(matrixObj);

    //Assert
    expect(result).toEqual(emptyResult);
  });
});

function createMatrixConfig() {
  return new EventMatrixDto({
    logicType: 1,
    trigger: 1,
    matrixId: 'E001',
    script: `function execute(context) {
      if (context.vars.BKG_BOOKING.BKG_NO != null && context.vars.asd.S_s != null && context.vars.asd.actualDate != null) {
        context.actions.storeActual(context.vars.actualDate);
      }
    }`,
  });
}
