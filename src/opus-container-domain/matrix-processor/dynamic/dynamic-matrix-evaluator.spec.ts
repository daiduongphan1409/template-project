import { Test, TestingModule } from '@nestjs/testing';
import { EventMatrixDto } from 'src/opus-container-domain/dto/event-matrix.dto';
import { DynamicMatrixEvaluator } from './dynamic-matrix-evaluator';
import { EvaluationResult, EventRequest, MatrixResult, ScriptResult } from 'src/opus-container-domain/dto';
import { DefaultContextLoaderName, DefaultScriptExecutorName } from '../interface';
import { Context } from 'src/opus-container-domain/dto/context.dto';
import { ScriptResultAction } from 'src/opus-container-domain/dto/scrip-result-action.dto';

describe(`${DynamicMatrixEvaluator.name}`, () => {
  let logic: DynamicMatrixEvaluator;
  const type = DynamicMatrixEvaluator.prototype;
  const mockContextLoaderLoad = jest.fn();
  const mockScriptExecutorExecute = jest.fn();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DynamicMatrixEvaluator,
        {
          provide: DefaultContextLoaderName,
          useFactory: () => ({
            load: mockContextLoaderLoad,
          }),
        },
        {
          provide: DefaultScriptExecutorName,
          useFactory: () => ({
            execute: mockScriptExecutorExecute,
          }),
        },
      ],
    }).compile();
    logic = module.get<DynamicMatrixEvaluator>(DynamicMatrixEvaluator);
  });

  it(`${type.execute.name}_Should_Run_When_DataCorrect`, async () => {
    mockContextLoaderLoad.mockReset();
    mockScriptExecutorExecute.mockReset();

    const eventMatrixDto = createMatrixDto();
    const context = [getContext()];
    const eventMatrixDtos = [eventMatrixDto];
    const scriptResult = getScriptResult(eventMatrixDto);
    const dynamicResult = new EvaluationResult({
      matrixResults: [
        new MatrixResult({
          context: getContext(),
          actions: scriptResult.actions,
        }),
      ],
    });
    const request = createEventRequest();

    mockContextLoaderLoad.mockReturnValue(context);
    mockScriptExecutorExecute.mockReturnValue(scriptResult);
    const result = await logic.execute(eventMatrixDtos, request);

    expect(result).toEqual(dynamicResult);
    expect(mockContextLoaderLoad.mock.calls).toHaveLength(1);
    expect(mockScriptExecutorExecute.mock.calls).toHaveLength(1);
  });

  it(`${type.execute.name}_Should_Run_Correct_When_Matrix_Have_More_Than_1_Item`, async () => {
    mockContextLoaderLoad.mockReset();
    mockScriptExecutorExecute.mockReset();

    const request = createEventRequest();
    const eventMatrixDto = createMatrixDto();
    const context = getContext();
    const contexts = [context, context];
    const eventMatrixDtos = [eventMatrixDto, eventMatrixDto];
    const scriptResult = getScriptResult(eventMatrixDto);
    const dynamicResult = new EvaluationResult({
      matrixResults: [
        new MatrixResult({
          context: getContext(),
          actions: scriptResult.actions,
        }),
        new MatrixResult({
          context: getContext(),
          actions: scriptResult.actions,
        }),
      ],
    });

    mockContextLoaderLoad.mockReturnValue(contexts);
    mockScriptExecutorExecute.mockReturnValue(scriptResult);

    const result = await logic.execute(eventMatrixDtos, request);

    expect(result).toEqual(dynamicResult);
    expect(mockContextLoaderLoad.mock.calls).toHaveLength(1);
    expect(mockScriptExecutorExecute.mock.calls).toHaveLength(2);
  });

  it(`${type.execute.name}_Should_Run_Correct_When_No_Script_Result`, async () => {
    mockContextLoaderLoad.mockReset();
    mockScriptExecutorExecute.mockReset();

    const request = createEventRequest();
    const eventMatrixDto = createMatrixDto();
    const context = getContext();
    const contexts = [context, context];
    const eventMatrixDtos = [eventMatrixDto, eventMatrixDto];
    const scriptResult = getScriptResult(eventMatrixDto);
    scriptResult.actions = [
      new ScriptResultAction({
        name: 'abc',
        parameters: {},
      }),
    ];
    const dynamicResult = new EvaluationResult({
      matrixResults: [
        new MatrixResult({
          context: getContext(),
          actions: [
            new ScriptResultAction({
              name: 'abc',
              parameters: {},
            }),
          ],
        }),
        new MatrixResult({
          context: getContext(),
          actions: [
            new ScriptResultAction({
              name: 'abc',
              parameters: {},
            }),
          ],
        }),
      ],
    });

    mockContextLoaderLoad.mockReturnValue(contexts);
    mockScriptExecutorExecute.mockReturnValue(scriptResult);

    const result = await logic.execute(eventMatrixDtos, request);

    expect(result).toEqual(dynamicResult);
    expect(mockContextLoaderLoad.mock.calls).toHaveLength(1);
    expect(mockScriptExecutorExecute.mock.calls).toHaveLength(2);
  });

  it(`${type.execute.name}_Should_Run_Correct_When_NoMatrix`, async () => {
    //Arrange
    mockContextLoaderLoad.mockReset();
    mockScriptExecutorExecute.mockReset();

    const request = createEventRequest();
    const eventMatrixDtos = [];
    const dynamicResult = new EvaluationResult({
      matrixResults: [],
    });

    //Act
    const result = await logic.execute(eventMatrixDtos, request);
    expect(result).toEqual(dynamicResult);

    //Asssert
    expect(mockContextLoaderLoad.mock.calls).toHaveLength(0);
    expect(mockScriptExecutorExecute.mock.calls).toHaveLength(0);
  });

  it.each([undefined, null])(`${type.execute.name}_Should_Controll_When_Event_Matrix_Is_abnomal `, async (matrixDto) => {
    //Arrange
    mockContextLoaderLoad.mockReset();
    mockScriptExecutorExecute.mockReset();

    const request = createEventRequest();
    const eventMatrixDtos = matrixDto;
    const dynamicResult = new EvaluationResult({
      matrixResults: [],
    });

    //Act
    const result = await logic.execute(eventMatrixDtos, request);
    expect(result).toEqual(dynamicResult);

    //Assert
    expect(mockContextLoaderLoad.mock.calls).toHaveLength(0);
    expect(mockScriptExecutorExecute.mock.calls).toHaveLength(0);
  });

  it.each([undefined, null])(`${type.execute.name}_Should_Controll_When_Request_Is_abnomal `, async (request) => {
    //Arrange
    mockContextLoaderLoad.mockReset();
    mockScriptExecutorExecute.mockReset();

    const eventMatrixDto = createMatrixDto();
    const eventMatrixDtos = [eventMatrixDto];
    const dynamicResult = new EvaluationResult({
      matrixResults: [],
    });

    //Act
    const result = await logic.execute(eventMatrixDtos, request);
    expect(result).toEqual(dynamicResult);

    //Assert
    expect(mockContextLoaderLoad.mock.calls).toHaveLength(0);
    expect(mockScriptExecutorExecute.mock.calls).toHaveLength(0);
  });

  function getScriptResult(event) {
    return new ScriptResult({
      event: event,
      actions: [
        new ScriptResultAction({
          name: 'action_name',
          parameters: {},
        }),
      ],
    });
  }

  function getContext() {
    const context = new Context({
      request: createEventRequest(),
    });
    return context;
  }

  function createEventRequest() {
    return new EventRequest({
      parentId: 'parentId',
      eventId: 'eventId',
      originTopic: '',
      bookingNo: '',
      containerNo: '',
    });
  }

  function createMatrixDto() {
    return new EventMatrixDto({
      logicType: 1,
      trigger: 1,
      matrixId: 'E001',
      script:
        'function execute(context){if (context.vars.BKG_BOOKING.BKG_NO != null && context.vars.asd.S_s != null && context.vars.asd.actualDate != null){context.actions.storeActual(context.vars.actualDate);}}',
    });
  }
});
