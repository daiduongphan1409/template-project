/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { ListDefinedVariablesImpl } from '.';
import { DefinedVariable, LoaderTask } from '../../interface';
import { DefinedVariableFactory } from './defined-variable-factory';

describe(`${DefinedVariableFactory.name}`, () => {
  let target: DefinedVariableFactory;
  const fakeName = 'Check test';
  const type = DefinedVariableFactory.prototype;
  const mockPostAction = jest.fn();
  beforeEach(async () => {
    mockPostAction.mockReturnValue([new FakeDefinedVariable()]);
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DefinedVariableFactory,
        {
          provide: ListDefinedVariablesImpl,
          useValue: [new FakeDefinedVariable()],
        },
      ],
    }).compile();

    target = module.get(DefinedVariableFactory);
  });

  it(`${type.getList.name}_Should_Return_Length_Is_One`, () => {
    //Assert
    expect(target.getList()).toHaveLength(1);
  });
  it(`${type.get.name}_Should_Return_Item`, () => {
    //Assert
    expect(target.get(fakeName)).toBeInstanceOf(FakeDefinedVariable);
  });
  it(`${type.get.name}_Should_Return_Undefine`, () => {
    //Assert
    expect(target.get('Test')).toBeUndefined();
  });
  class FakeDefinedVariable implements DefinedVariable {
    getLoaderTasks(): LoaderTask[] {
      throw new Error('Method not implemented.');
    }

    execute(action: any): Promise<void> {
      throw new Error('Method not implemented.');
    }

    isExist(suggestActions: string[], logic: string): boolean {
      throw new Error('Method not implemented.');
    }

    getInjectScript(): string {
      throw new Error('Method not implemented.');
    }

    getName(): string {
      return fakeName;
    }
  }
});
