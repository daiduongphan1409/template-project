import { Inject } from '@nestjs/common';
import { ListDefinedVariablesImpl } from '.';
import { DefinedVariable } from '../../interface';

export const DefinedVariableFactoryName = 'DefinedVariableFactoryName';
export class DefinedVariableFactory {
  constructor(@Inject(ListDefinedVariablesImpl) private readonly definedVariables: DefinedVariable[]) {}

  public getList(): DefinedVariable[] {
    return this.definedVariables;
  }

  public get(name: string): DefinedVariable {
    return this.definedVariables.find((p) => p.getName() === name);
  }
}
