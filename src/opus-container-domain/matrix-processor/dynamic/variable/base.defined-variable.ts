import { StringUtils } from 'src/utils/string.utils';
import { DefinedVariable, LoaderTask } from '../../interface';

export abstract class BaseDefinedVariable implements DefinedVariable {
  public isExist(suggestVariables: string[], logic: string): boolean {
    if (suggestVariables.some((x) => StringUtils.equalIgnoreCase(x, this.getName()) || x.includes(`${this.getName()}.`))) {
      return true;
    }

    const regex = new RegExp(`(^|([^a-zA-z0-9_]))(context\\.vars\\.${this.getName()})($|[^a-zA-z0-9_])`, 'g');
    return regex.test(logic);
  }

  abstract getLoaderTasks(): LoaderTask[];
  abstract getInjectScript(): string;
  abstract getName(): string;
}
