import { Inject } from '@nestjs/common';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { OdsBookingCutOfTimeLoader } from '../../loader/loader-task-impl/ods-booking-cutoff-time.loader';
import { BaseDefinedVariable } from '../base.defined-variable';

export class TableBookingCutOffTimeVariable extends BaseDefinedVariable {
  private readonly name = 'BKG_CLZ_TM';

  constructor(@Inject(odsRepositoryName) private readonly odbRepository: ODSRepository) {
    super();
  }

  public getLoaderTasks(): LoaderTask[] {
    return [new OdsBookingCutOfTimeLoader(this.name, this.odbRepository)];
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
