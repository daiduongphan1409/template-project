import { Test, TestingModule } from '@nestjs/testing';
import { SceCopDtlsLoaderName } from 'src/opus-container-domain/matrix-processor/interface';
import { SceCopDtlsVariable } from './sce-cop-dtls.variable';

describe(`${SceCopDtlsVariable.name}`, () => {
  let temp: SceCopDtlsVariable;
  const type = SceCopDtlsVariable.prototype;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SceCopDtlsVariable,
        {
          provide: SceCopDtlsLoaderName,
          useFactory: () => ({}),
        },
      ],
    }).compile();

    temp = module.get(SceCopDtlsVariable);
  });

  it(`${type.isExist.name}_Should_Return_False`, async () => {
    const rs = temp.isExist(['SCE_COL_DTLS.sasd', 'SCE_COL_DTLS.sasd'], '');
    expect(rs).toBe(false);
  });
  it(`${type.isExist.name}_Should_Return_True`, async () => {
    const rs = temp.isExist(['SCE_COP_DTLS', 'SCE_COP_DTLS'], '');
    expect(rs).toBe(true);
  });
  it(`${type.getInjectScript.name}_Should_Return_Empty`, async () => {
    const rs = temp.getInjectScript();
    expect(rs).toBe('');
  });
  it(`${type.getLoaderTasks.name}_Should_Return_One_Action`, async () => {
    const rs = temp.getLoaderTasks();
    expect(rs).toHaveLength(1);
  });
  it(`${type.getName.name}_Should_Return_SCE_COP_DTL`, async () => {
    const rs = temp.getName();
    expect(rs).toBe('SCE_COP_DTLS');
  });
});
