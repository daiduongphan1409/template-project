import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ContainerNumberLoader } from '../../loader/loader-task-impl/container-number.loader';
import { BaseDefinedVariable } from '../base.defined-variable';

export class ContainerNumberVariable extends BaseDefinedVariable {
  private readonly name = 'containerNumber';

  constructor() {
    super();
  }

  public getLoaderTasks(): LoaderTask[] {
    return [new ContainerNumberLoader(this.name)];
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
