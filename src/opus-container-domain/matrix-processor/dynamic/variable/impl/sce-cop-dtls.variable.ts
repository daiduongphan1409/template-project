import { Inject } from '@nestjs/common';
import { DefinedVariable, LoaderTask, SceCopDtlsLoaderName } from 'src/opus-container-domain/matrix-processor/interface';
export class SceCopDtlsVariable implements DefinedVariable {
  private readonly name = 'SCE_COP_DTLS';
  constructor(@Inject(SceCopDtlsLoaderName) private readonly loader: LoaderTask) {}

  getLoaderTasks(): LoaderTask[] {
    return [this.loader];
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  isExist(suggestVariables: string[], logic: string): boolean {
    for (const variable of suggestVariables) {
      if (variable.includes(this.name)) {
        return true;
      }
    }
    return false;
  }

  getInjectScript(): string {
    return '';
  }

  getName(): string {
    return this.name;
  }
}
