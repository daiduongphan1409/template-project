/* eslint-disable @typescript-eslint/no-unused-vars */
import { Inject } from '@nestjs/common';
import { DefinedVariable, LoaderTask, SceCopDtlLoaderName } from 'src/opus-container-domain/matrix-processor/interface';
export class SceCopDtlVariable implements DefinedVariable {
  private readonly name = 'SCE_COP_DTL';
  constructor(@Inject(SceCopDtlLoaderName) private readonly loader: LoaderTask) {}

  getLoaderTasks(): LoaderTask[] {
    return [this.loader];
  }

  isExist(suggestVariables: string[], logic: string): boolean {
    for (const variable of suggestVariables) {
      if (variable.includes(this.name)) {
        return true;
      }
    }
    return false;
  }

  getInjectScript(): string {
    return '';
  }

  getName(): string {
    return this.name;
  }
}
