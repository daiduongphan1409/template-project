import { Inject } from '@nestjs/common';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { OdsListBookingCutOffTimeTableLoader } from '../../loader/loader-task-impl/ods-list-booking-cutoff-time-by-booking.loader';
import { BaseDefinedVariable } from '../base.defined-variable';

export class ListBookingCutOffTimeVariable extends BaseDefinedVariable {
  private readonly name = 'listBookingCutOffTime';

  constructor(@Inject(odsRepositoryName) private readonly odbRepository: ODSRepository) {
    super();
  }

  public getLoaderTasks(): LoaderTask[] {
    return [new OdsListBookingCutOffTimeTableLoader(this.name, this.odbRepository)];
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
