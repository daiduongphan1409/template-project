import { Test, TestingModule } from '@nestjs/testing';
import { BkgClzTmLoaderName } from 'src/opus-container-domain/matrix-processor/interface';

import { BkgClzTmVariable } from './bkg-clz-tm.variable';

describe(`${BkgClzTmVariable.name}`, () => {
  let temp: BkgClzTmVariable;
  const type = BkgClzTmVariable.prototype;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BkgClzTmVariable,
        {
          provide: BkgClzTmLoaderName,
          useFactory: () => ({}),
        },
      ],
    }).compile();

    temp = module.get(BkgClzTmVariable);
  });

  it(`${type.isExist.name}_Should_Return_False`, async () => {
    const rs = temp.isExist(['BKG_CLT_TM.cbh', 'BKG_CLT_TM.cbh'], '');
    expect(rs).toBe(false);
  });
  it(`${type.isExist.name}_Should_Return_True`, async () => {
    const rs = temp.isExist(['BKG_CLZ_TM.cbh', 'BKG_CLT_TM.cbh'], '');
    expect(rs).toBe(true);
  });
  it(`${type.getInjectScript.name}_Should_Return_Empty`, async () => {
    const rs = temp.getInjectScript();
    expect(rs).toBe('');
  });
  it(`${type.getLoaderTasks.name}_Should_Return_One_Action`, async () => {
    const rs = temp.getLoaderTasks();
    expect(rs).toHaveLength(1);
  });
  it(`${type.getName.name}_Should_Return_BKG_CLZ_TM`, async () => {
    const rs = temp.getName();
    expect(rs).toBe('BKG_CLZ_TM');
  });
});
