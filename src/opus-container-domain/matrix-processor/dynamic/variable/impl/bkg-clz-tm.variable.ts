import { Inject } from '@nestjs/common';
import { BkgClzTmLoaderName, DefinedVariable, LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
export class BkgClzTmVariable implements DefinedVariable {
  private readonly name = 'BKG_CLZ_TM';
  constructor(@Inject(BkgClzTmLoaderName) private readonly loader: LoaderTask) {}

  public getLoaderTasks(): LoaderTask[] {
    return [this.loader];
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public isExist(suggestVariables: string[], logic: string): boolean {
    for (const variable of suggestVariables) {
      if (variable.includes(this.name)) {
        return true;
      }
    }
    return false;
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
