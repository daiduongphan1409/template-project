import { Inject } from '@nestjs/common';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { OdsListContainerNumberLoader } from '../../loader/loader-task-impl/ods-list-container-number.loader';
import { BaseDefinedVariable } from '../base.defined-variable';

export class ListContainerNumberVariable extends BaseDefinedVariable {
  private readonly name = 'listContainerNumber';

  constructor(@Inject(odsRepositoryName) private readonly odbRepository: ODSRepository) {
    super();
  }

  public getLoaderTasks(): LoaderTask[] {
    return [new OdsListContainerNumberLoader(this.name, this.odbRepository)];
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
