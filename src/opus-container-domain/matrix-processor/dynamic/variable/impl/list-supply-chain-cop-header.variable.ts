import { Inject } from '@nestjs/common';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { OdsListSupplyChainCopHeaderLoader } from '../../loader/loader-task-impl/ods-list-supply-chain-cop-header.loader';
import { BaseDefinedVariable } from '../base.defined-variable';

export class ListSupplyChainCopHeaderVariable extends BaseDefinedVariable {
  private readonly name = 'listSupplyChainCopHeader';

  constructor(@Inject(odsRepositoryName) private readonly odbRepository: ODSRepository) {
    super();
  }

  public getLoaderTasks(): LoaderTask[] {
    return [new OdsListSupplyChainCopHeaderLoader(this.name, this.odbRepository)];
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
