import { Test, TestingModule } from '@nestjs/testing';
import { SceCopDtlLoaderName } from 'src/opus-container-domain/matrix-processor/interface';
import { SceCopDtlVariable } from './sce-cop-dtl.variable';

describe(`${SceCopDtlVariable.name}`, () => {
  let temp: SceCopDtlVariable;
  const type = SceCopDtlVariable.prototype;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SceCopDtlVariable,
        {
          provide: SceCopDtlLoaderName,
          useFactory: () => ({}),
        },
      ],
    }).compile();

    temp = module.get(SceCopDtlVariable);
  });

  it(`${type.isExist.name}_Should_Return_False`, async () => {
    const rs = temp.isExist(['SCE_COL_DTL.sasd', 'SCE_COL_DTL.sasd'], '');
    expect(rs).toBe(false);
  });
  it(`${type.isExist.name}_Should_Return_True`, async () => {
    const rs = temp.isExist(['SCE_COP_DTL', 'SCE_COP_DTL'], '');
    expect(rs).toBe(true);
  });
  it(`${type.getInjectScript.name}_Should_Return_Empty`, async () => {
    const rs = temp.getInjectScript();
    expect(rs).toBe('');
  });
  it(`${type.getLoaderTasks.name}_Should_Return_One_Action`, async () => {
    const rs = temp.getLoaderTasks();
    expect(rs).toHaveLength(1);
  });
  it(`${type.getName.name}_Should_Return_SCE_COP_DTL`, async () => {
    const rs = temp.getName();
    expect(rs).toBe('SCE_COP_DTL');
  });
});
