import { Inject } from '@nestjs/common';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { OdsListSupplyChainCopDetailLoader } from '../../loader/loader-task-impl/ods-list-supply-chain-cop-detail.loader';
import { BaseDefinedVariable } from '../base.defined-variable';

export class ListSupplyChainCopDetailVariable extends BaseDefinedVariable {
  private readonly name = 'listSupplyChainCopDetail';

  constructor(@Inject(odsRepositoryName) private readonly odbRepository: ODSRepository) {
    super();
  }

  public getLoaderTasks(): LoaderTask[] {
    return [new OdsListSupplyChainCopDetailLoader(this.name, this.odbRepository)];
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
