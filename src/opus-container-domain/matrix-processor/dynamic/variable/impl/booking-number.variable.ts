import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { BookingNumberLoader } from '../../loader/loader-task-impl/booking-number.loader';
import { BaseDefinedVariable } from '../base.defined-variable';

export class BookingNumberVariable extends BaseDefinedVariable {
  private readonly name = 'bookingNumber';

  constructor() {
    super();
  }

  public getLoaderTasks(): LoaderTask[] {
    return [new BookingNumberLoader(this.name)];
  }

  public getInjectScript(): string {
    return '';
  }

  public getName(): string {
    return this.name;
  }
}
