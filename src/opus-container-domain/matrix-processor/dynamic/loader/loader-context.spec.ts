import { Test, TestingModule } from '@nestjs/testing';
import { BookingClzTime } from 'src/shared/domain/entity-ods';
import { LoaderContext } from './loader-context';

describe(`${LoaderContext.name}`, () => {
  let target: LoaderContext;
  const type = LoaderContext.prototype;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LoaderContext],
    }).compile();

    target = module.get(LoaderContext);
  });

  it(`${type.isCache}_Should_Return_True`, () => {
    //Arrange
    const catchData = new BookingClzTime({
      BKG_NO: 'TM87364IRUTJG',
      CLZ_TP_CD: 'R',
      CLZ_YD_CD: 'YUR9874',
    });
    target.putCache('BKG_CLZ_TM', catchData);

    //Act
    const result = target.isCache('BKG_CLZ_TM');

    //Assert
    expect(result).toBe(true);
  });
  it(`${type.isCache}_Should_Return_False`, () => {
    //Act
    const result = target.isCache('BKG_CLZ_TM');

    //Assert
    expect(result).toBe(false);
  });
  it(`${type.getCache}_Should_Return_Undefine`, async () => {
    //Act
    const data = target.getCache('BKG_CLZ_TM');

    //Assert
    expect(data).toBeUndefined();
  });
  it(`${type.getCache}_Should_Return_Object`, () => {
    //Arrange
    const catchData = new BookingClzTime({
      BKG_NO: 'TM87364IRUTJG',
      CLZ_TP_CD: 'R',
      CLZ_YD_CD: 'YUR9874',
    });
    target.putCache('BKG_CLZ_TM', catchData);

    //Act
    const result = target.getCache('BKG_CLZ_TM');

    //Assert
    expect(result).toMatchObject(catchData);
  });
});
