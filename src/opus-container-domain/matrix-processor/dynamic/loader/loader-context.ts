export class LoaderContext {
  private cache: Map<string, unknown>;
  constructor() {
    this.cache = new Map();
  }

  public isCache(key: string): boolean {
    return this.cache.has(key);
  }

  public getCache(key: string): unknown {
    return this.cache.get(key);
  }

  public putCache(key: string, value: unknown) {
    this.cache.set(key, value);
  }
}
