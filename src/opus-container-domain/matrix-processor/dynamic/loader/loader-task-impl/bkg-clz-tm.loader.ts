/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable require-atomic-updates */
import { Inject } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { ColumnNames, Common, TableNames } from 'src/utils/constanst';
import { getObjectProperty } from 'src/utils/func';
import { LoaderContext } from '../loader-context';

export class BkgClzTmLoader implements LoaderTask {
  private tableName = TableNames.BKG_CLZ_TM;
  //ODS data for script run variable
  private copDetailName = TableNames.SCE_COP_DTL;
  //ODS data for create event with each container for booking recive from event data
  private copDetailsName = TableNames.SCE_COP_DTLS;

  constructor(@Inject(odsRepositoryName) private odsRepository: ODSRepository) {}

  public async load(context: Context, loader: LoaderContext, event: EventRequest): Promise<void> {
    const originData = event.originData['data'];

    if (loader.isCache(this.tableName)) {
      context[Common.Vars][this.tableName] = loader.getCache(this.tableName);
      if (loader.isCache(this.copDetailsName)) {
        context[Common.Vars][this.copDetailName] = loader.getCache(this.copDetailsName)[0];
        context[Common.Vars][this.copDetailsName] = loader.getCache(this.copDetailsName);
      }
      return;
    }

    const sysSetDate = getObjectProperty(ColumnNames.SYS_SET_DT, originData);
    const mnlSetDate = getObjectProperty(ColumnNames.MNL_SET_DT, originData);
    const ntcFlag = getObjectProperty(ColumnNames.NTC_FLG, originData);
    const clzYdCd = getObjectProperty(ColumnNames.CLZ_YD_CD, originData);
    const bkgNo = getObjectProperty(ColumnNames.BKG_NO, originData);
    const clzTpCd = getObjectProperty(ColumnNames.CLZ_TP_CD, originData);

    if ((sysSetDate || mnlSetDate) && ntcFlag && clzYdCd) {
      context[Common.Vars][this.tableName] = originData;
    } else {
      const data = await this.odsRepository.getBkgClzTm(bkgNo, clzTpCd);

      if (sysSetDate) {
        data[ColumnNames.SYS_SET_DT] = sysSetDate;
      }
      if (mnlSetDate) {
        data[ColumnNames.MNL_SET_DT] = mnlSetDate;
      }
      context[Common.Vars][this.tableName] = data;
    }

    const copDetails = await this.odsRepository.getCopDltByBkgClzTm(
      context[Common.Vars][this.tableName][ColumnNames.BKG_NO],
      context[Common.Vars][this.tableName][ColumnNames.CLZ_YD_CD],
      ['FOTRAD', 'FOTYAD', 'FOTMAD'],
    );

    if (copDetails && copDetails.length > 0) {
      /**
       * get first cop detail to compare variable in script logic of event
       * because data with specific container will be the same with the other container
       */
      context[Common.Vars][this.copDetailName] = copDetails[0];
      context[Common.Vars][this.copDetailsName] = copDetails;
      loader.putCache(this.copDetailsName, copDetails);
    }
    loader.putCache(this.tableName, context[Common.Vars][this.tableName]);
  }
}
