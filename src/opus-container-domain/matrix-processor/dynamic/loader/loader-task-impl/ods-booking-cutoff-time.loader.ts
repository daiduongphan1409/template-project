import { Logger } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { BookingClzTime } from 'src/shared/domain/entity-ods';
import { ODSRepository } from 'src/shared/domain/repository/ods-db-repository.interface';
import { StringUtils } from 'src/utils/string.utils';
import { LoaderContext } from '../loader-context';

export class OdsBookingCutOfTimeLoader implements LoaderTask {
  private readonly logger = new Logger(OdsBookingCutOfTimeLoader.name);

  constructor(private variableName: string, private odsRepository: ODSRepository) {}

  public async load(context: Context, loaderContext: LoaderContext, request: EventRequest): Promise<void> {
    if (this.getTableName(context.request.originData.metadata.TableName) === 'BKG_CLZ_TM') {
      if (this.isEnoughColumns(context, request)) {
        this.loadByRequestData(context, request);
        return;
      }

      if (StringUtils.isNotEmpty(request.originData.data['CLZ_TP_CD'])) {
        await this.loadByBookingNumberAndCode(context, loaderContext, request.originData.data['BKG_NO'], request.originData.data['CLZ_TP_CD']);
        return;
      }
    }

    this.logger.warn('Could not found the way to load Booking Cutoff Time');
    context.vars[this.variableName] = null;
  }

  private isEnoughColumns(context: Context, request: EventRequest): boolean {
    const currentTable = context.analyzed.tables.find((x) => x.name === 'BKG_CLZ_TM');
    if (currentTable) {
      return currentTable.columns.every((x) => request.originData.data[x] !== undefined);
    }

    return true;
  }

  private loadByRequestData(context: Context, request: EventRequest) {
    context.vars[this.variableName] = Object.assign({}, request.originData.data);
  }

  private async loadByBookingNumberAndCode(context: Context, loaderContext: LoaderContext, bookingNumber: string, code: string): Promise<void> {
    const cacheKey = `BKG_CLZ_TM:BKG-NO-${bookingNumber}:CLZ_TP_CD-${code}`;

    if (loaderContext.isCache(cacheKey)) {
      context.vars[this.variableName] = loaderContext.getCache(cacheKey) as BookingClzTime;
    } else {
      const bookingCutOffTime = await this.odsRepository.getBkgClzTm(bookingNumber, code);
      context.vars[this.variableName] = bookingCutOffTime;
      loaderContext.putCache(cacheKey, bookingCutOffTime);
    }
  }

  private getTableName(name: string): string {
    const tableName = name.split('.');
    return tableName[tableName.length - 1].toUpperCase();
  }
}
