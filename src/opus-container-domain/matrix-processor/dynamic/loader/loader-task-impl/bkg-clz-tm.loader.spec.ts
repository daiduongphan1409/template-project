import { Test, TestingModule } from '@nestjs/testing';
import { CdcEventPayload, Context, EventMatrixDto, EventRequest } from 'src/opus-container-domain/dto';
import { BookingClzTime, SupplyChainManagementCopDetail } from 'src/shared/domain/entity-ods';
import { LogicType, TriggerType } from 'src/shared/domain/model/enum/matrix.enum';
import { odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { LoaderContext } from '../loader-context';
import { BkgClzTmLoader } from './bkg-clz-tm.loader';

// eslint-disable-next-line jest/valid-title
describe(`${BkgClzTmLoader.name}`, () => {
  let temp: BkgClzTmLoader;
  const type = BkgClzTmLoader.prototype;
  const bkgClzTm = new BookingClzTime({
    BKG_NO: 'abc',
  });
  const copDetail = new SupplyChainManagementCopDetail({
    COP_NO: 'abc',
  });
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BkgClzTmLoader,
        {
          provide: odsRepositoryName,
          useFactory: () => ({
            getBkgClzTm: jest.fn().mockReturnValue(bkgClzTm),
            getCopDltByBkgClzTm: jest.fn().mockReturnValue(copDetail),
          }),
        },
      ],
    }).compile();

    temp = module.get(BkgClzTmLoader);
  });

  it(`${type.load}_Should_load_Run_Cache`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    loader.putCache('BKG_CLZ_TM', 'abc');
    loader.putCache('SCE_COP_DTLS', ['abc']);
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {},
      }),
    });

    //Act
    await temp.load(context, loader, event);

    //Assert
    expect(context.vars['BKG_CLZ_TM']).toBe('abc');
  });
  it(`${type.load}_Should_load_Run_Orgin_Data`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {
          BKG_NO: 'NHUFOIIRO',
          CLZ_YD_CD: 'R',
          SYS_SET_DT: '2022-10-10 05:20:30.000',
          MNL_SET_DT: '2022-10-10 05:20:30.000',
          NTC_FLG: 'Y',
        },
      }),
    });

    //Act
    await temp.load(context, loader, event);

    //Assert
    expect(context.vars['BKG_CLZ_TM']).toMatchObject(event.originData.data);
  });
  it(`${type.load}_Should_load_Run_ODS`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {
          BKG_NO: 'NHUFOIIRO',
          CLZ_YD_CD: 'R',
          SYS_SET_DT: '2022-10-10 05:20:30.000',
        },
      }),
    });

    //Act
    await temp.load(context, loader, event);

    //Assert
    expect(context.vars['BKG_CLZ_TM']).toMatchObject(bkgClzTm);
  });
});
