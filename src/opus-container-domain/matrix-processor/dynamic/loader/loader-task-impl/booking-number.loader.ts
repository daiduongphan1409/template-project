import { Logger } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { LoaderContext } from '../loader-context';

export class BookingNumberLoader implements LoaderTask {
  private readonly logger = new Logger(BookingNumberLoader.name);

  constructor(private variableName: string) {}

  public async load(context: Context, loaderContext: LoaderContext, request: EventRequest): Promise<void> {
    if (request.originData.data['BKG_NO'] !== undefined) {
      context.vars[this.variableName] = request.originData.data['BKG_NO'];
      return;
    }

    this.logger.warn('Could not found the booking number');
    context.vars[this.variableName] = null;
  }
}
