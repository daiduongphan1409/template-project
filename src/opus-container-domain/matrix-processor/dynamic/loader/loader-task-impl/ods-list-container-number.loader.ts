import { Logger } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository } from 'src/shared/domain/repository/ods-db-repository.interface';
import { LoaderContext } from '../loader-context';

export class OdsListContainerNumberLoader implements LoaderTask {
  private readonly logger = new Logger(OdsListContainerNumberLoader.name);

  constructor(private variableName: string, private odsRepository: ODSRepository) {}

  public async load(context: Context, loaderContext: LoaderContext, request: EventRequest): Promise<void> {
    if (request.originData.data['BKG_NO'] !== undefined) {
      await this.loadByBookingNumber(context, loaderContext, request.originData.data['BKG_NO']);
      return;
    }

    this.logger.warn('Could not found the way to load list container number');
    context.vars[this.variableName] = [];
  }

  private async loadByBookingNumber(context: Context, loaderContext: LoaderContext, bookingNumber: string): Promise<void> {
    const cacheKey = `LIST_CONTAINER_NUMBER:BKG-NO-${bookingNumber}`;

    if (loaderContext.isCache(cacheKey)) {
      context.vars[this.variableName] = loaderContext.getCache(cacheKey) as string[];
    } else {
      const records = await this.odsRepository.getListBookingNumber(bookingNumber);
      const containerNumbers = [...new Set(records.map((x) => x.CNTR_NO))];
      context.vars[this.variableName] = containerNumbers;
      loaderContext.putCache(cacheKey, containerNumbers);
    }
  }
}
