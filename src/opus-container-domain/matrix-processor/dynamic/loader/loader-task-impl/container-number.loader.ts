import { Logger } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { LoaderContext } from '../loader-context';

export class ContainerNumberLoader implements LoaderTask {
  private readonly logger = new Logger(ContainerNumberLoader.name);

  constructor(private variableName: string) {}

  public async load(context: Context, loaderContext: LoaderContext, request: EventRequest): Promise<void> {
    if (request.originData.data['CNTR_NO'] !== undefined) {
      context.vars[this.variableName] = request.originData.data['CNTR_NO'];
      return;
    }

    this.logger.warn('Could not found the container number');
    context.vars[this.variableName] = null;
  }
}
