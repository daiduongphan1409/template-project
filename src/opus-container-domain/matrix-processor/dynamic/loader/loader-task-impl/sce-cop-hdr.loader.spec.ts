import { Test, TestingModule } from '@nestjs/testing';
import { CdcEventPayload, Context, EventMatrixDto, EventRequest } from 'src/opus-container-domain/dto';
import { SupplyChainManagementCopDetail } from 'src/shared/domain/entity-ods';
import { LogicType, TriggerType } from 'src/shared/domain/model/enum/matrix.enum';
import { odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { LoaderContext } from '../loader-context';
import { SceCopHdrLoader } from './sce-cop-hdr.loader';

// eslint-disable-next-line jest/valid-title
describe(`${SceCopHdrLoader.name}`, () => {
  const supplyChainManagementCopDetail = new SupplyChainManagementCopDetail({
    COP_NO: 'abc',
  });
  const type = SceCopHdrLoader.prototype;
  let loaderTask: SceCopHdrLoader;
  const mockgetSceCopDtl = jest.fn();
  beforeEach(async () => {
    mockgetSceCopDtl.mockReset;
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SceCopHdrLoader,
        {
          provide: odsRepositoryName,
          useFactory: () => ({
            getSceCopHdr: mockgetSceCopDtl,
          }),
        },
      ],
    }).compile();

    loaderTask = module.get(SceCopHdrLoader);
  });

  it(`${type.load.name}_Should_SceCopDtlLoader_toBeDefined`, () => {
    //Arrange
    //Act
    //Assert
    expect(loaderTask).toBeDefined();
  });

  it(`${type.load.name}_Should_SceCopDtlLoader_load_Run_Cache`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    loader.putCache('SCE_COP_HDR', 'abc');
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {},
      }),
    });

    //Act
    await loaderTask.load(context, loader, event);

    //Assert
    expect(context.vars['SCE_COP_HDR']).toBe('abc');
  });
  it(`${type.load.name}_Should_SceCopDtlLoader_load_Run_ODS`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {
          COP_NO: 'NHUFOIIRO',
          CLZ_YD_CD: 'R',
          SYS_SET_DT: 'abc',
          MNL_SET_DT: 'abc',
        },
      }),
    });
    mockgetSceCopDtl.mockReturnValue(supplyChainManagementCopDetail);

    //Act
    await loaderTask.load(context, loader, event);

    //Assert
    expect(context.vars['SCE_COP_HDR']).toMatchObject(supplyChainManagementCopDetail);
  });
});
