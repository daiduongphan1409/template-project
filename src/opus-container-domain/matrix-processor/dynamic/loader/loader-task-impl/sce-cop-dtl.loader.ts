import { Inject } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { ColumnNames, Common, TableNames } from 'src/utils/constanst';
import { getObjectProperty } from 'src/utils/func';
import { LoaderContext } from '../loader-context';

export class SceCopDtlLoader implements LoaderTask {
  constructor(@Inject(odsRepositoryName) private odsRepository: ODSRepository) {}

  public async load(context: Context, loader: LoaderContext, event: EventRequest): Promise<void> {
    const tableName = TableNames.SCE_COP_DTL;
    const originData = event.originData['data'];

    if (loader.isCache(tableName)) {
      context[Common.Vars][tableName] = loader.getCache(tableName);
      return;
    }

    if (
      ColumnNames.ACT_CD in originData &&
      ColumnNames.ESTM_DT in originData &&
      ColumnNames.PLN_DT in originData &&
      ColumnNames.NOD_CD in originData &&
      ColumnNames.SKD_VOY_NO in originData &&
      ColumnNames.SKD_DIR_CD in originData &&
      ColumnNames.CLPT_IND_SEQ in originData &&
      ColumnNames.VPS_PORT_CD in originData
    ) {
      context[Common.Vars][tableName] = originData;
      return;
    }
    if (ColumnNames.COP_NO in originData && ColumnNames.COP_DTL_SEQ in originData) {
      const copNo = getObjectProperty(ColumnNames.COP_NO, originData);
      const copSeq = getObjectProperty(ColumnNames.COP_DTL_SEQ, originData);
      const copDetails = await this.odsRepository.getSceCopDtl(copNo, copSeq);
      context[Common.Vars][tableName] = copDetails;

      if (copDetails) {
        context[Common.Vars][tableName] = copDetails;
        loader.putCache(tableName, copDetails);
      }

      loader.putCache(tableName, context[Common.Vars][tableName]);
    }
  }
}
