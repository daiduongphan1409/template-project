import { Logger } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { SupplyChainManagementCopDetail } from 'src/shared/domain/entity-ods';
import { ODSRepository } from 'src/shared/domain/repository/ods-db-repository.interface';
import { LoaderContext } from '../loader-context';

export class OdsListSupplyChainCopDetailLoader implements LoaderTask {
  private readonly logger = new Logger(OdsListSupplyChainCopDetailLoader.name);

  constructor(private variableName: string, private odsRepository: ODSRepository) {}

  public async load(context: Context, loaderContext: LoaderContext, request: EventRequest): Promise<void> {
    if (request.originData.data['BKG_NO'] && request.originData.data['CNTR_NO']) {
      await this.loadByBookingNumberAndContainerNumber(context, loaderContext, request.originData.data['BKG_NO'], request.originData.data['CNTR_NO']);
      return;
    }

    if (request.originData.data['BKG_NO']) {
      await this.loadByBookingNumber(context, loaderContext, request.originData.data['BKG_NO']);
      return;
    }

    this.logger.warn('Could not found the way to load supply chain cop detail list');
    context.vars[this.variableName] = [];
  }

  private async loadByBookingNumber(context: Context, loaderContext: LoaderContext, bookingNumber: string): Promise<void> {
    const cacheKey = `LIST_SCE_COP_DTL:BKG-NO-${bookingNumber}`;

    if (loaderContext.isCache(cacheKey)) {
      context.vars[this.variableName] = loaderContext.getCache(cacheKey) as SupplyChainManagementCopDetail[];
    } else {
      const copDetails = await this.odsRepository.getSceCopDtlsByBooking(bookingNumber);
      context.vars[this.variableName] = copDetails;
      loaderContext.putCache(cacheKey, copDetails);
    }
  }

  private async loadByBookingNumberAndContainerNumber(
    context: Context,
    loaderContext: LoaderContext,
    bookingNumber: string,
    containerNumber: string,
  ): Promise<void> {
    const cacheKey = `LIST_SCE_COP_DTL:BKG-NO-${bookingNumber}:CNTR-NO-${containerNumber}`;

    if (loaderContext.isCache(cacheKey)) {
      context.vars[this.variableName] = loaderContext.getCache(cacheKey) as SupplyChainManagementCopDetail[];
    } else {
      const copDetails = await this.odsRepository.getSceCopDtlsByBookingAndContainer(bookingNumber, containerNumber);
      context.vars[this.variableName] = copDetails;
      loaderContext.putCache(cacheKey, copDetails);
    }
  }
}
