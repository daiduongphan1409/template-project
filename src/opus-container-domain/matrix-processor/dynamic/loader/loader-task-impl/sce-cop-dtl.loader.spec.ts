import { Test, TestingModule } from '@nestjs/testing';
import { CdcEventPayload, Context, EventMatrixDto, EventRequest } from 'src/opus-container-domain/dto';
import { SupplyChainManagementCopDetail } from 'src/shared/domain/entity-ods';
import { LogicType, TriggerType } from 'src/shared/domain/model/enum/matrix.enum';
import { odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { LoaderContext } from '../loader-context';
import { SceCopDtlLoader } from './sce-cop-dtl.loader';

// eslint-disable-next-line jest/valid-title
describe(`${SceCopDtlLoader.name}`, () => {
  const supplyChainManagementCopDetail = new SupplyChainManagementCopDetail({
    COP_NO: 'abc',
  });
  const type = SceCopDtlLoader.prototype;
  let loaderTask: SceCopDtlLoader;
  const mockgetSceCopDtl = jest.fn();
  beforeEach(async () => {
    mockgetSceCopDtl.mockReset;
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SceCopDtlLoader,
        {
          provide: odsRepositoryName,
          useFactory: () => ({
            getSceCopDtl: mockgetSceCopDtl,
          }),
        },
      ],
    }).compile();

    loaderTask = module.get(SceCopDtlLoader);
  });

  it(`${type.load.name}_Should_SceCopDtlLoader_toBeDefined`, () => {
    //Assert
    expect(loaderTask).toBeDefined();
  });

  it(`${type.load.name}_Should_SceCopDtlLoader_load_Run_Cache`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    loader.putCache('SCE_COP_DTL', 'abc');
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {},
      }),
    });

    //Act
    await loaderTask.load(context, loader, event);

    //Assert
    expect(context.vars['SCE_COP_DTL']).toBe('abc');
  });
  it(`${type.load.name}_Should_SceCopDtlLoader_load_Run_Orgin_Data`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {
          ACT_CD: 'a',
          ESTM_DT: new Date(),
          PLN_DT: new Date(),
          NOD_CD: 'a',
          SKD_VOY_NO: 'a',
          SKD_DIR_CD: 'a',
          CLPT_IND_SEQ: 'a',
          VPS_PORT_CD: 'a',
        },
      }),
    });

    //Act
    await loaderTask.load(context, loader, event);

    //Assert
    expect(context.vars['SCE_COP_DTL']).toBe(event.originData.data);
  });
  it(`${type.load.name}_Should_SceCopDtlLoader_load_Run_ODS`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {
          COP_NO: 'NHUFOIIRO',
          COP_DTL_SEQ: 1341,
          CLZ_YD_CD: 'R',
          SYS_SET_DT: 'abc',
          MNL_SET_DT: 'abc',
        },
      }),
    });
    mockgetSceCopDtl.mockReturnValue(supplyChainManagementCopDetail);

    //Act
    await loaderTask.load(context, loader, event);

    //Assert
    expect(context.vars['SCE_COP_DTL']).toMatchObject(supplyChainManagementCopDetail);
  });
  it(`${type.load.name}_Should_SceCopDtlLoader_Load_Undefine`, async () => {
    //Arrange
    const context = new Context({
      event: new EventMatrixDto({
        logicType: LogicType.DYNAMIC,
        matrixId: 'E040',
        trigger: TriggerType.ESTIMATED,
      }),
      vars: {},
    });
    const loader = new LoaderContext();
    const event = new EventRequest({
      eventId: 'xxxfdfdfdfd',
      parentId: 'xxxfdfdfdfd',
      originData: new CdcEventPayload({
        data: {
          CLZ_YD_CD: 'R',
          SYS_SET_DT: 'abc',
          MNL_SET_DT: 'abc',
        },
      }),
    });
    mockgetSceCopDtl.mockReturnValue(supplyChainManagementCopDetail);

    //Act
    await loaderTask.load(context, loader, event);

    //Assert
    expect(context.vars['SCE_COP_DTL']).toBeUndefined();
  });
});
