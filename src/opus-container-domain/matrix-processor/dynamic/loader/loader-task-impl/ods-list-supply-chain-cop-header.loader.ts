import { Logger } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { SupplyChainManagementCopDetail } from 'src/shared/domain/entity-ods';
import { ODSRepository } from 'src/shared/domain/repository/ods-db-repository.interface';
import { LoaderContext } from '../loader-context';

export class OdsListSupplyChainCopHeaderLoader implements LoaderTask {
  private readonly logger = new Logger(OdsListSupplyChainCopHeaderLoader.name);

  constructor(private variableName: string, private odsRepository: ODSRepository) {}

  public async load(context: Context, loaderContext: LoaderContext, request: EventRequest): Promise<void> {
    if (request.originData.data['BKG_NO'] && request.originData.data['CNTR_NO']) {
      await this.loadByBookingNumberAndContainerNumber(context, loaderContext, request.originData.data['BKG_NO'], request.originData.data['CNTR_NO']);
      return;
    }

    if (request.originData.data['BKG_NO']) {
      await this.loadByBookingNumber(context, loaderContext, request.originData.data['BKG_NO']);
      return;
    }

    this.logger.warn('Could not found the way to load supply chain cop header list');
    context.vars[this.variableName] = [];
  }

  private async loadByBookingNumber(context: Context, loaderContext: LoaderContext, bookingNumber: string): Promise<void> {
    const cacheKey = `LIST_SCE_COP_HDR:BKG-NO-${bookingNumber}`;

    if (loaderContext.isCache(cacheKey)) {
      context.vars[this.variableName] = loaderContext.getCache(cacheKey) as SupplyChainManagementCopDetail[];
    } else {
      const copHeaders = await this.odsRepository.getSceCopHdrByBooking(bookingNumber);
      context.vars[this.variableName] = copHeaders;
      loaderContext.putCache(cacheKey, copHeaders);
    }
  }

  private async loadByBookingNumberAndContainerNumber(
    context: Context,
    loaderContext: LoaderContext,
    bookingNumber: string,
    containerNumber: string,
  ): Promise<void> {
    const cacheKey = `LIST_SCE_COP_HDR:BKG-NO-${bookingNumber}:CNTR-NO-${containerNumber}`;

    if (loaderContext.isCache(cacheKey)) {
      context.vars[this.variableName] = loaderContext.getCache(cacheKey) as SupplyChainManagementCopDetail[];
    } else {
      const copHeader = await this.odsRepository.getSceCopHdrByBookingAndContainer(bookingNumber, containerNumber);
      context.vars[this.variableName] = [copHeader];
      loaderContext.putCache(cacheKey, [copHeader]);
    }
  }
}
