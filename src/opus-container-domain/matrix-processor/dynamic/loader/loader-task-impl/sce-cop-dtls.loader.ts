import { Inject } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { ColumnNames, Common, TableNames } from 'src/utils/constanst';
import { getObjectProperty } from 'src/utils/func';
import { LoaderContext } from '../loader-context';

export class SceCopDtlsLoader implements LoaderTask {
  constructor(@Inject(odsRepositoryName) private odsRepository: ODSRepository) {}

  public async load(context: Context, loader: LoaderContext, event: EventRequest): Promise<void> {
    const tableName = TableNames.SCE_COP_DTLS;
    const originData = event.originData['data'];

    if (loader.isCache(tableName)) {
      context[Common.Vars][tableName] = loader.getCache(tableName);
      return;
    }
    const copNo = getObjectProperty(ColumnNames.COP_NO, originData);
    if (!copNo) {
      return;
    }
    const sceCopDtls = await this.odsRepository.getSceCopDtls(copNo);
    context[Common.Vars][tableName] = sceCopDtls;

    if (sceCopDtls) {
      context[Common.Vars][tableName] = sceCopDtls;
      loader.putCache(tableName, sceCopDtls);
    }

    loader.putCache(tableName, context[Common.Vars][tableName]);
  }
}
