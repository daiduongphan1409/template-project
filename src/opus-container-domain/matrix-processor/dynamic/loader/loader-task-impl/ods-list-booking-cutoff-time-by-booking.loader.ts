import { Logger } from '@nestjs/common';
import { Context, EventRequest } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { BookingClzTime } from 'src/shared/domain/entity-ods';
import { ODSRepository } from 'src/shared/domain/repository/ods-db-repository.interface';
import { LoaderContext } from '../loader-context';

export class OdsListBookingCutOffTimeTableLoader implements LoaderTask {
  private readonly logger = new Logger(OdsListBookingCutOffTimeTableLoader.name);

  constructor(private variableName: string, private odsRepository: ODSRepository) {}

  public async load(context: Context, loaderContext: LoaderContext, request: EventRequest): Promise<void> {
    if (request.originData.data['BKG_NO'] !== undefined) {
      await this.loadByBookingNumber(context, loaderContext, request.originData.data['BKG_NO']);
      return;
    }

    this.logger.warn('Could not found the way to load Booking Cutoff Time list');
    context.vars[this.variableName] = [];
  }

  private async loadByBookingNumber(context: Context, loaderContext: LoaderContext, bookingNumber: string): Promise<void> {
    const cacheKey = `BKG_CLZ_TM:BKG-NO-${bookingNumber}`;

    if (loaderContext.isCache(cacheKey)) {
      context.vars[this.variableName] = loaderContext.getCache(cacheKey) as BookingClzTime[];
    } else {
      const records = await this.odsRepository.getBookingCutoffTimeList(bookingNumber);
      context.vars[this.variableName] = records;
      loaderContext.putCache(cacheKey, records);
    }
  }
}
