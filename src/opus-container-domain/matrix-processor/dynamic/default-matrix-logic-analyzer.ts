import { EventMatrixDto, LogicAnalyzedTable, LogicAnalyzeResult } from 'src/opus-container-domain/dto';
import { MatrixLogicAnalyzer } from '../interface';

export class DefaultMatrixLogicAnalyzer implements MatrixLogicAnalyzer {
  public execute(matrix: EventMatrixDto): LogicAnalyzeResult {
    if (!matrix?.script) {
      return new LogicAnalyzeResult({
        variables: [],
        tables: [],
        functions: [],
      });
    }
    const logicAnalyzeResult = new LogicAnalyzeResult({
      variables: this.getVariables(matrix.script),
      functions: this.getPostActions(matrix.script),
      tables: this.getTables(matrix.script),
    });

    return logicAnalyzeResult;
  }

  private getVariables(logic: string): string[] {
    const regexp = /context\.vars\.([0-9a-zA-Z_.]+)([^0-9a-zA-Z_(])/g;
    const matches = logic.matchAll(regexp);
    const variables = [];

    for (const match of matches) {
      variables.push(match[1]);
    }

    return this.removeDuplicate(variables);
  }

  private getPostActions(logic: string): string[] {
    const regexp = /context\.actions\.([0-9a-zA-Z_.]+)\(/g;
    const matches = logic.matchAll(regexp);
    const postActions = [];

    for (const match of matches) {
      postActions.push(match[1]);
    }

    return this.removeDuplicate(postActions);
  }

  private getTables(logic: string): LogicAnalyzedTable[] {
    const variables = this.getVariables(logic);
    const tables: LogicAnalyzedTable[] = [];

    for (const variable of variables) {
      const parts = variable.split('.');

      if (parts.length !== 2) {
        continue;
      }

      const tableItemIndex = tables.findIndex((t) => t.name === parts[0]);
      if (tableItemIndex >= 0) {
        tables[tableItemIndex].columns.push(parts[1]);
        continue;
      }

      const tableItem = new LogicAnalyzedTable({
        name: parts[0],
        columns: [parts[1]],
      });
      tables.push(tableItem);
    }

    return this.removeDuplicate(tables);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private removeDuplicate(data: Array<any>) {
    if (!data) return [];
    return data.filter((item, pos) => {
      return data.indexOf(item) === pos;
    });
  }
}
