/* eslint-disable jest/no-conditional-expect */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { Context, EventRequest, LogicAnalyzedTable, LogicAnalyzeResult } from 'src/opus-container-domain/dto';
import { EventMatrixDto } from 'src/opus-container-domain/dto/event-matrix.dto';
import { InternalException } from 'src/shared/domain/model/exception';
import { DefaultMatrixLogicAnalyzerName, DefinedVariable, LoaderTask, PostAction } from '../interface';
import { DefaultLoaderTaskCollector } from './default-loader-task-collector';
import { LoaderContext } from './loader/loader-context';
import { PostActionFactoryName } from './post-action';
import { DefinedVariableFactoryName } from './variable/defined-variable-factory';

describe(`${DefaultLoaderTaskCollector.name}`, () => {
  let logic: DefaultLoaderTaskCollector;
  const type = DefaultLoaderTaskCollector.prototype;

  const mockAnalyzerExecute = jest.fn();
  const mockVariableFactoryGetList = jest.fn();
  const mockActionFactoryGetList = jest.fn();

  beforeEach(async () => {
    mockAnalyzerExecute.mockReset();
    mockVariableFactoryGetList.mockReset();
    mockActionFactoryGetList.mockReset();

    mockAnalyzerExecute.mockReturnValue(
      new LogicAnalyzeResult({
        variables: ['var1'],
        functions: ['func1'],
        tables: [
          new LogicAnalyzedTable({
            name: 'BKG_BOOKING',
            columns: ['BOOKING_NO', 'BOOKING_ID'],
          }),
        ],
      }),
    );

    mockVariableFactoryGetList.mockReturnValue([new FakeDefinedVariables()]);
    mockActionFactoryGetList.mockReturnValue([new FakePostAction()]);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DefaultLoaderTaskCollector,
        {
          provide: DefaultMatrixLogicAnalyzerName,
          useFactory: () => ({
            execute: mockAnalyzerExecute,
          }),
        },
        {
          provide: DefinedVariableFactoryName,
          useFactory: () => ({
            getList: mockVariableFactoryGetList,
          }),
        },
        {
          provide: PostActionFactoryName,
          useFactory: () => ({
            getList: mockActionFactoryGetList,
          }),
        },
      ],
    }).compile();
    logic = module.get<DefaultLoaderTaskCollector>(DefaultLoaderTaskCollector);
  });

  it(`${type.collect.name}_Should_Run_When_DataCorrect`, () => {
    //Arrange
    const loader = new FakeLoaderTask();
    const logicResult = [loader, loader];
    const config = createMatrixConfig();
    const request = createEventRequest();

    //Act
    const result = logic.collect(config, request);

    //Assert
    expect(result).toEqual(logicResult);
  });

  it.each([undefined, null])(`${type.collect.name}_Should_Return_Empty_When_EvenMatrixDto_Abnormal`, (config) => {
    //Arrange
    const request = createEventRequest();

    //Act
    //Assert
    expect(() => logic.collect(config, request)).toThrow(InternalException);
  });

  it.each([undefined, null])(`${type.collect.name}_Should_Return_Empty_When_Request_Abnormal`, (request) => {
    //Arrange
    const config = createMatrixConfig();

    //Act
    //Assert
    expect(() => logic.collect(config, request)).toThrow(InternalException);
  });

  it(`${type.collect.name}_Should_Throw_Exception_When_Analyzer_Has_Exception`, () => {
    //Arrange
    mockAnalyzerExecute.mockReset();
    mockAnalyzerExecute.mockImplementation(() => {
      throw new InternalException(-1001, 'UNKNOWN ERROR');
    });

    const config = createMatrixConfig();
    const request = createEventRequest();

    //Act
    //Assert
    expect(() => {
      logic.collect(config, request);
    }).toThrow(InternalException);
  });

  it(`${type.collect.name}_Should_Throw_Exception_When_VariableFactory_Has_Exception`, () => {
    //Arrange
    mockVariableFactoryGetList.mockReset();
    mockVariableFactoryGetList.mockImplementation(() => {
      throw new InternalException(-1001, 'UNKNOWN ERROR');
    });

    const config = createMatrixConfig();
    const request = createEventRequest();

    //Act
    //Assert
    expect(() => {
      logic.collect(config, request);
    }).toThrow(InternalException);
  });

  it(`${type.collect.name}_Should_Throw_Exception_When_ActionFactory_Has_Exception`, () => {
    //Arrange
    mockActionFactoryGetList.mockReset();
    mockActionFactoryGetList.mockImplementation(() => {
      throw new InternalException(-1001, 'UNKNOWN ERROR');
    });

    const config = createMatrixConfig();
    const request = createEventRequest();

    //Act
    //Assert
    expect(() => {
      logic.collect(config, request);
    }).toThrow(InternalException);
  });

  function createMatrixConfig() {
    return new EventMatrixDto({
      logicType: 1,
      trigger: 1,
      matrixId: 'E001',
      script: `function execute(context) {
        if (context.vars.BKG_BOOKING.BKG_NO != null && context.vars.asd.S_s != null && context.vars.asd.actualDate != null) {
          context.actions.storeActual(context.vars.actualDate);
        }
      }`,
    });
  }

  function createEventRequest() {
    return new EventRequest({
      parentId: 'parentId',
      eventId: 'eventId',
      originTopic: '',
      bookingNo: '',
      containerNo: '',
    });
  }

  class FakePostAction implements PostAction {
    async execute(_action: any): Promise<void> {
      return;
    }

    getLoaderTasks(): LoaderTask[] {
      return [new FakeLoaderTask()];
    }

    isExist(_suggestVariables: string[], _logic: string): boolean {
      return true;
    }

    getInjectScript(): string {
      return '';
    }

    getName(): string {
      return 'FakeDefinedVariables';
    }
  }

  class FakeDefinedVariables implements DefinedVariable {
    getLoaderTasks(): LoaderTask[] {
      return [new FakeLoaderTask()];
    }

    isExist(_suggestVariables: string[], _logic: string): boolean {
      return true;
    }

    getInjectScript(): string {
      return '';
    }

    getName(): string {
      return 'FakeDefinedVariables';
    }
  }

  class FakeLoaderTask implements LoaderTask {
    async load(_context: Context, _loader: LoaderContext, _event: EventRequest): Promise<void> {
      return;
    }
  }
});
