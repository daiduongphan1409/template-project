import { Inject, Injectable, Logger } from '@nestjs/common';
import { Context, EventMatrixDto, EventRequest } from 'src/opus-container-domain/dto';
import {
  ContextLoader,
  LoaderTaskCollector,
  DefaultLoaderTaskCollectorName,
  MatrixLogicAnalyzer,
  DefaultMatrixLogicAnalyzerName,
} from '../interface';
import { LoaderContext } from './loader/loader-context';

@Injectable()
export class DefaultContextLoader implements ContextLoader {
  private logger = new Logger(DefaultContextLoader.name);

  constructor(
    @Inject(DefaultLoaderTaskCollectorName) private readonly loaderTaskCollector: LoaderTaskCollector,
    @Inject(DefaultMatrixLogicAnalyzerName) private readonly matrixLogicAnalyzer: MatrixLogicAnalyzer,
  ) {}

  public load(eventMatrices: EventMatrixDto[], request: EventRequest): Promise<Context[]> {
    const loaderContext = new LoaderContext();
    return this.loadContexts(eventMatrices, request, loaderContext);
  }

  private async loadContexts(eventMatrices: EventMatrixDto[], request: EventRequest, loaderContext: LoaderContext): Promise<Context[]> {
    const contexts = [];
    for (const matrix of eventMatrices) {
      if (matrix.script && matrix.script.length > 0) {
        // eslint-disable-next-line no-await-in-loop
        const context = await this.loadContext(matrix, request, loaderContext);
        if (context && Object.keys(context.vars).length > 0) {
          contexts.push(context);
        }
      }
    }

    return contexts;
  }

  private async loadContext(eventMatrix: EventMatrixDto, request: EventRequest, loaderContext: LoaderContext): Promise<Context> {
    try {
      const loaders = this.loaderTaskCollector.collect(eventMatrix, request);
      const context = new Context({
        request: request,
        event: eventMatrix,
        analyzed: this.matrixLogicAnalyzer.execute(eventMatrix),
        vars: {},
      });

      for (const loader of loaders) {
        await loader.load(context, loaderContext, request);
      }

      return context;
    } catch (error) {
      const err = error as Error;
      this.logger.error(`loadContext run error Input: ${JSON.stringify(eventMatrix)}, Message: ${err.message}`, err.stack);
    }
    return null;
  }
}
