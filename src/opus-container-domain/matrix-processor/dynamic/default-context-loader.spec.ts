import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';
import { CdcEventPayload, Context, EventMatrixDto, EventRequest, LogicAnalyzeResult } from 'src/opus-container-domain/dto';
import { LoaderTask, DefaultLoaderTaskCollectorName, MatrixLogicAnalyzer, DefaultMatrixLogicAnalyzerName } from '../interface';
import { DefaultContextLoader } from './default-context-loader';
import { LoaderContext } from './loader/loader-context';

const eventDataMatrix = [
  new EventMatrixDto({
    logicType: 1,
    trigger: 1,
    matrixId: '01',
    script: 'string',
  }),
  new EventMatrixDto({
    logicType: 1,
    trigger: 1,
    matrixId: '02',
    script: 'string',
  }),
];

const requestData = new EventRequest({
  parentId: 'string',
  eventId: 'string',
  originTopic: 'string',
  originData: new CdcEventPayload(),
  containerNo: '02',
});
describe(`${DefaultContextLoader.name}`, () => {
  let target: DefaultContextLoader;
  let mockMatrixLogicAnalyzer: DeepMocked<MatrixLogicAnalyzer>;
  const type = DefaultContextLoader.prototype;
  const mockDefaultContextLoader = jest.fn();

  beforeEach(async () => {
    mockDefaultContextLoader.mockReset();
    mockDefaultContextLoader.mockReturnValue([new FakeLoaderTask()]);
    mockMatrixLogicAnalyzer = createMock<MatrixLogicAnalyzer>();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DefaultContextLoader,
        {
          provide: DefaultLoaderTaskCollectorName,
          useFactory: () => ({
            collect: mockDefaultContextLoader,
          }),
        },
        {
          provide: DefaultMatrixLogicAnalyzerName,
          useValue: mockMatrixLogicAnalyzer,
        },
      ],
    }).compile();
    target = module.get<DefaultContextLoader>(DefaultContextLoader);
  });

  it(`${type.load.name}_Should_Run_When_DataCorrect`, async () => {
    //Arrange
    mockMatrixLogicAnalyzer.execute.mockReturnValue(
      new LogicAnalyzeResult({
        variables: [],
        tables: [],
        functions: [],
      }),
    );

    const config = [
      new Context({
        request: new EventRequest({
          parentId: 'string',
          eventId: 'string',
          originTopic: 'string',
          originData: new CdcEventPayload(),
          containerNo: '02',
        }),
        event: new EventMatrixDto({
          logicType: 1,
          matrixId: '01',
          script: 'string',
          trigger: 1,
        }),
        analyzed: new LogicAnalyzeResult({
          functions: [],
          tables: [],
          variables: [],
        }),
        vars: { abc: '' },
      }),
      new Context({
        request: new EventRequest({
          parentId: 'string',
          eventId: 'string',
          originTopic: 'string',
          originData: new CdcEventPayload(),
          containerNo: '02',
        }),
        event: new EventMatrixDto({
          logicType: 1,
          trigger: 1,
          matrixId: '02',
          script: 'string',
        }),
        analyzed: new LogicAnalyzeResult({
          functions: [],
          tables: [],
          variables: [],
        }),
        vars: { abc: '' },
      }),
    ];

    //Act
    const result = await target.load(eventDataMatrix, requestData);

    //Assert
    expect(result).toEqual(config);
  });

  class FakeLoaderTask implements LoaderTask {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    async load(_context: Context, _loader: LoaderContext, _event: EventRequest): Promise<void> {
      _context.vars = { abc: '' };
      return;
    }
  }
});
