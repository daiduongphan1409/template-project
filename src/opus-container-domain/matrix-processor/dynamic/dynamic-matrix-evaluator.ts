import { Inject, Injectable } from '@nestjs/common';
import { Context, EvaluationResult, EventMatrixDto, EventRequest, MatrixResult } from 'src/opus-container-domain/dto';
import { ContextLoader, DefaultContextLoaderName, DefaultScriptExecutorName, MatrixEvaluator, ScriptExecutor } from '../interface';

@Injectable()
export class DynamicMatrixEvaluator implements MatrixEvaluator {
  constructor(
    @Inject(DefaultContextLoaderName) private readonly contextLoader: ContextLoader,
    @Inject(DefaultScriptExecutorName) private readonly scriptExecutor: ScriptExecutor,
  ) {}

  public async execute(matrices: EventMatrixDto[], request: EventRequest): Promise<EvaluationResult> {
    if (!matrices || matrices.length <= 0 || !request) {
      return this.createEmptyEvaluationResult();
    }

    const contexts = await this.loadContext(matrices, request);
    const evaluationResults = this.evaluate(contexts);

    return evaluationResults;
  }

  private async loadContext(matrices: EventMatrixDto[], request: EventRequest): Promise<Context[]> {
    return await this.contextLoader.load(matrices, request);
  }

  private evaluate(contexts: Context[]): EvaluationResult {
    const matrixResults = new Array<MatrixResult>();

    for (const context of contexts) {
      const scriptResult = this.scriptExecutor.execute(context);
      const matrixResult: MatrixResult = new MatrixResult({
        context: context,
        actions: scriptResult.actions,
      });

      matrixResults.push(matrixResult);
    }

    return new EvaluationResult({
      matrixResults: matrixResults,
    });
  }

  private createEmptyEvaluationResult() {
    return new EvaluationResult({
      matrixResults: [],
    });
  }
}
