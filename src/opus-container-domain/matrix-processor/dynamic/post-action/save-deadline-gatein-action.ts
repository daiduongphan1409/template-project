/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-await-in-loop */
import { Inject, Injectable, Logger } from '@nestjs/common';
import { randomUUID } from 'crypto';
import * as fs from 'fs';
import * as path from 'path';
import { Context } from 'src/opus-container-domain/dto';
import {
  BookingContainer,
  BookingContainerActivity,
  BookingContainerActivityDeadline,
  BookingContainerActivityDeadlineHistories,
} from 'src/shared/domain/entity';
import {
  BookingContainerActivityDeadlineHistoriesRepository,
  BookingContainerActivityDeadlineHistoriesRepositoryName,
} from 'src/shared/domain/repository/booking-container-activity-deadline-histories.interface';
import {
  BookingContainerActivityDeadlineRepository,
  BookingContainerActivityDeadlineRepositoryName,
} from 'src/shared/domain/repository/booking-container-activity-deadline.interface';
import {
  BookingContainerActivityRepository,
  BookingContainerActivityRepositoryName,
} from 'src/shared/domain/repository/booking-container-activity.interface';
import { BookingContainerRepository, BookingContainerRepositoryName } from 'src/shared/domain/repository/booking-container.interface';
import { LoaderTask } from '../../interface';
import { PostAction } from '../../interface/post-action.interface';

@Injectable()
export class SaveDeadlineGateInAction implements PostAction {
  private readonly name = 'STORE_DEADLINE_GATEIN';
  private readonly logger = new Logger(SaveDeadlineGateInAction.name);

  constructor(
    @Inject(BookingContainerRepositoryName) private readonly bookingContainerRepository: BookingContainerRepository,
    @Inject(BookingContainerActivityRepositoryName) private readonly activityRepository: BookingContainerActivityRepository,
    @Inject(BookingContainerActivityDeadlineRepositoryName) private readonly deadlineRepository: BookingContainerActivityDeadlineRepository,
    @Inject(BookingContainerActivityDeadlineHistoriesRepositoryName)
    private readonly historyRepository: BookingContainerActivityDeadlineHistoriesRepository,
  ) {}

  public getLoaderTasks(): LoaderTask[] {
    return [];
  }

  public async execute(context: Context, parameters: any): Promise<void> {
    const eventBookingContainers = parameters['bookingContainers'];
    const bookingContainers: Array<BookingContainer> = [];
    const activities: Array<BookingContainerActivity> = [];
    const deadlines: Array<BookingContainerActivityDeadline> = [];
    const histories: Array<BookingContainerActivityDeadlineHistories> = [];
    const currentMatrixId = parameters['eventId'];
    const summaryMatrixId = parameters['eventSummary'];
    for (const eventBookingContainer of eventBookingContainers) {
      const currentBookingContainer = await this.bookingContainerRepository.getEvent(
        eventBookingContainer['BKG_NO'],
        eventBookingContainer['CNTR_NO'],
        eventBookingContainer['COP_NO'],
        [currentMatrixId, summaryMatrixId],
      );
      let currentActivity: BookingContainerActivity = undefined;
      let summaryActivity: BookingContainerActivity = undefined;

      if (currentBookingContainer?.activities) {
        currentActivity = currentBookingContainer.activities.find((t) => t.eventMatrixId === currentMatrixId);
        summaryActivity = currentBookingContainer.activities.find((t) => t.eventMatrixId === summaryMatrixId);
      }
      const temp = this.createActivity(currentActivity, eventBookingContainer, currentMatrixId, parameters['date']);
      activities.push(temp.activity);
      histories.push(temp.history);
      deadlines.push(temp.deadline);
      if (!summaryActivity || summaryActivity.copSeq >= eventBookingContainer['COP_DTL_SEQ']) {
        const temp1 = this.createActivity(summaryActivity, eventBookingContainer, summaryMatrixId, parameters['date']);
        activities.push(temp1.activity);
        histories.push(temp1.history);
        deadlines.push(temp1.deadline);
      }
      const eventData = this.initBookingContainer(eventBookingContainer);
      bookingContainers.push(eventData);
    }

    await this.saveEventData(bookingContainers, activities, deadlines, histories);
  }

  public isExist(suggestActions: string[], logic: string): boolean {
    return true;
  }

  public getInjectScript(): string {
    try {
      const data = fs.readFileSync(path.join(__dirname, 'script', 'save-deadline-gatein.js'), { encoding: 'utf-8' });
      if (data && data.length > 0) {
        return data;
      }
    } catch (error) {
      this.logger.error('load Base Script run error', error);
    }
    return '';
  }

  public getName(): string {
    return this.name;
  }

  private createActivity(activity: BookingContainerActivity, eventBookingContainer: any, matrixId: string, date: string) {
    let deadline = undefined;
    if (activity) {
      activity.bookingNo = eventBookingContainer['BKG_NO'];
      activity.containerNo = eventBookingContainer['CNTR_NO'];
      activity.copNo = eventBookingContainer['COP_NO'];
      activity.copSeq = eventBookingContainer['COP_DTL_SEQ'];
      activity.opusCode = eventBookingContainer['ACT_CD'];
      deadline = this.createDeadline(activity.activitydeadLines[0], date, activity.id);
    } else {
      activity = this.initActivity(eventBookingContainer, matrixId, date);
      deadline = activity.activitydeadLines[0];
      delete activity.activitydeadLines;
    }
    const history = this.initDeadlineHistory(date, activity.id);
    return { activity, history, deadline };
  }

  private createDeadline(deadline: BookingContainerActivityDeadline, date: string, activityId: string): BookingContainerActivityDeadline {
    if (deadline) {
      deadline.deadlineDate = new Date(date);
    } else {
      deadline = this.initDeadline(date, activityId);
    }
    return deadline;
  }

  private initBookingContainer(bookingContainer: any): BookingContainer {
    return new BookingContainer({
      bookingNo: bookingContainer['BKG_NO'],
      containerNo: bookingContainer['CNTR_NO'],
      copNo: bookingContainer['COP_NO'],
      typeSizeCode: 'a',
      movementYear: '2022',
      movementId: 1,
      movementCycleNo: 1,
      movementStatus: 'a',
    });
  }

  private initActivity(bookingContainer: any, matrixId: string, date: string): BookingContainerActivity {
    const id = randomUUID();
    return new BookingContainerActivity({
      id: id,
      bookingNo: bookingContainer['BKG_NO'],
      containerNo: bookingContainer['CNTR_NO'],
      copNo: bookingContainer['COP_NO'],
      copSeq: parseInt(bookingContainer['COP_DTL_SEQ']),
      eventMatrixId: matrixId,
      opusCode: bookingContainer['ACT_CD'],
      activitydeadLines: [this.initDeadline(date, id)],
      activityDeadLineHistories: [],
    });
  }

  private initDeadline(date: string, activityId: string): BookingContainerActivityDeadline {
    return new BookingContainerActivityDeadline({
      id: randomUUID(),
      activityId: activityId,
      deadlineDate: new Date(date),
    });
  }

  private initDeadlineHistory(date: string, activityId: string): BookingContainerActivityDeadlineHistories {
    return new BookingContainerActivityDeadlineHistories({
      id: randomUUID(),
      activityId: activityId,
      deadlineDate: new Date(date),
    });
  }

  private async saveEventData(
    bookingContainers: BookingContainer[],
    activities: BookingContainerActivity[],
    deadlines: BookingContainerActivityDeadline[],
    histories: BookingContainerActivityDeadlineHistories[],
  ) {
    try {
      let result = await this.bookingContainerRepository.save(bookingContainers);
      if (result) {
        result = await this.activityRepository.save(activities);
      }

      if (result) {
        result = await this.deadlineRepository.save(deadlines);
      }

      if (result) {
        result = await this.historyRepository.save(histories);
      }
    } catch (error) {
      const err = error as Error;
      this.logger.error(`saveEventData have some error message ${err.message}`);
    }
  }
}
