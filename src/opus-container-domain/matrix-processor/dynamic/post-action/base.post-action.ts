import { StringUtils } from 'src/utils/string.utils';
import { LoaderTask, PostAction } from '../../interface';
import * as fs from 'fs';
import * as path from 'path';
import { Logger } from '@nestjs/common';
import { Context } from 'src/opus-container-domain/dto';

export abstract class BasePostAction implements PostAction {
  private readonly regex = new RegExp(`(^|([^a-zA-z0-9_]))(context\\.actions\\.${this.getName()})($|[^a-zA-z0-9_])`, 'g');

  public isExist(suggestVariables: string[], logic: string): boolean {
    if (suggestVariables.some((x) => StringUtils.equalIgnoreCase(x, this.getName()))) {
      return true;
    }

    return this.regex.test(logic);
  }

  protected loadScript(fileName: string): string {
    try {
      const script = fs.readFileSync(path.join(__dirname, 'script', fileName), { encoding: 'utf-8' });
      if (StringUtils.isNotEmpty(script)) {
        return script;
      }
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Load post action script error. Message: ${error.message}, ActionName: ${this.getName()}, FileName: ${fileName}`, error.stack);
    }

    return null;
  }

  abstract getLoaderTasks(): LoaderTask[];
  abstract getInjectScript(): string;
  abstract getName(): string;
  abstract execute(context: Context, args: any): Promise<void>; // eslint-disable-line @typescript-eslint/no-explicit-any
}
