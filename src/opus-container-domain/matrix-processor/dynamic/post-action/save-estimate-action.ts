/* eslint-disable @typescript-eslint/no-explicit-any */
import { Inject, Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import { Context } from 'src/opus-container-domain/dto';
import { BookingContainer, BookingContainerActivity, BookingContainerActivityEstimate } from 'src/shared/domain/entity';
import { BookingContainerActivityEstimateHistories } from 'src/shared/domain/entity/booking-container-estimate-history.entity';
import { BaseException, ValidateException } from 'src/shared/domain/model/exception';
import {
  ContainerDomainEventEstimateRepository,
  ContainerDomainEventEstimateRepositoryName,
} from 'src/shared/domain/repository/container-domain-event-estimate.interface';
import { DataEventEstimate } from 'src/utils/constanst';
import { ErrorCode } from 'src/utils/error.code';
import { LoaderTask } from '../../interface';
import { PostAction } from '../../interface/post-action.interface';

@Injectable()
export class SaveEstimateAction implements PostAction {
  private readonly name = 'STORE_ESTIMATE';
  private readonly logger = new Logger(SaveEstimateAction.name);

  constructor(@Inject(ContainerDomainEventEstimateRepositoryName) private eventRepository: ContainerDomainEventEstimateRepository) {}

  getLoaderTasks(): LoaderTask[] {
    return [];
  }

  public async execute(context: Context, parameters: any): Promise<void> {
    try {
      const bookingContainerActivityEstimate = new BookingContainerActivityEstimate({
        estimateDate: parameters[DataEventEstimate.estimatedDate],
        planDate: parameters[DataEventEstimate.plannedDate],
      });
      const bookingContainerActivityEstimateHistories = new BookingContainerActivityEstimateHistories({
        estimate: parameters[DataEventEstimate.estimatedDate],
      });

      const countryId = parameters[DataEventEstimate.nodeCode].slice(0, 2);
      const bookingContainerActivity = new BookingContainerActivity({
        bookingNo: parameters[DataEventEstimate.bookingNumber],
        containerNo: parameters[DataEventEstimate.containerNumber],
        eventMatrixId: parameters[DataEventEstimate.eventID],
        copSeq: parseInt(parameters[DataEventEstimate.copDetailSequence]),
        ediCode: null,
        opusCode: parameters[DataEventEstimate.actualCode],
        vesselCode: parameters[DataEventEstimate.vesselCode],
        countryId: countryId,
        locationId: parameters[DataEventEstimate.nodeCode],
        isValid: 1,
        activityEstimates: [bookingContainerActivityEstimate],
        activityEstimateHistories: [bookingContainerActivityEstimateHistories],
        copNo: parameters[DataEventEstimate.copNumber],
      });

      const bookingContainer = new BookingContainer({
        bookingNo: parameters[DataEventEstimate.bookingNumber],
        containerNo: parameters[DataEventEstimate.containerNumber],
        copNo: parameters[DataEventEstimate.copNumber],
        isValid: true,
        activities: [bookingContainerActivity],
        typeSizeCode: 'a',
        movementYear: '2022',
        movementId: 1,
        movementCycleNo: 1,
        movementStatus: 'a',
      });
      await this.eventRepository.saveBookingContainer(bookingContainer);
    } catch (ex) {
      if (ex instanceof BaseException) {
        this.logger.warn(`Save booking container faild .Acction: ${parameters}, Code: ${ex.code}, Message: ${ex.message}`);
      } else {
        const error = ex as Error;
        this.logger.error(`Save booking container error. Acction: ${parameters}, Message: ${error.message}`, error.stack);
      }
    }
  }

  public isExist(suggestActions: string[], logic: string): boolean {
    suggestActions.includes(logic);
    return true;
  }

  public getInjectScript(): string {
    try {
      const data = fs.readFileSync(path.join(__dirname, 'script', 'save-estimate.js'), { encoding: 'utf-8' });
      if (data && data.length > 0) {
        return data;
      }
    } catch (error) {
      this.logger.error('load Base Script run error', error);
    }
    throw new ValidateException(ErrorCode.JAVASCRIPT_ERROR, 'Base script is null or empty');
  }

  public getName(): string {
    return this.name;
  }
}
