import { PostActionFactory, PostActionFactoryName } from './post-action-factory';
import { Inject, Injectable } from '@nestjs/common';
import { PostAction, ScriptInjector } from '../../interface';

@Injectable()
export class PostActionInjector implements ScriptInjector {
  constructor(@Inject(PostActionFactoryName) private readonly postActionFactory: PostActionFactory) {}

  public append(script: string): string {
    const postActions = this.postActionFactory.getList();
    const scripts = postActions.map((action: PostAction) => action.getInjectScript());
    return script.replace("'<<post-actions>>'", scripts.join('\n\n'));
  }
}
