/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Injectable } from '@nestjs/common';
import { Inject } from '@nestjs/common/decorators';
import { Context } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { BookingContainerActivity, BookingContainerActivityDeadline } from 'src/shared/domain/entity';
import { ValidateException } from 'src/shared/domain/model/exception';
import {
  BookingContainerActivityDeadlineRepository,
  BookingContainerActivityDeadlineRepositoryName,
} from 'src/shared/domain/repository/booking-container-activity-deadline.interface';
import {
  BookingContainerActivityRepository,
  BookingContainerActivityRepositoryName,
} from 'src/shared/domain/repository/booking-container-activity.interface';
import { BookingContainerRepository, BookingContainerRepositoryName } from 'src/shared/domain/repository/booking-container.interface';
import { ErrorCode } from 'src/utils/error.code';
import { StringUtils } from 'src/utils/string.utils';
import { BasePostAction } from '../base.post-action';
import { StoreBookingContainerAction } from './store-booking-container.action';

@Injectable()
export class StoreDeadlinePostAction extends BasePostAction {
  constructor(
    @Inject(BookingContainerRepositoryName) private readonly bookingContainerRepository: BookingContainerRepository,
    @Inject(BookingContainerActivityRepositoryName) private readonly bookingContainerActivityRepository: BookingContainerActivityRepository,
    @Inject(BookingContainerActivityDeadlineRepositoryName)
    private readonly bookingContainerActivityDeadlineRepository: BookingContainerActivityDeadlineRepository,
    @Inject(StoreBookingContainerAction) private readonly storeBookingContainerAction: StoreBookingContainerAction,
  ) {
    super();
  }

  getLoaderTasks(): LoaderTask[] {
    return [];
  }

  getName(): string {
    return 'STORE_DEADLINE_ACTION';
  }

  getInjectScript(): string {
    const script = this.loadScript('store-deadline-action.js');
    if (StringUtils.isEmpty(script)) {
      throw new ValidateException(ErrorCode.LOADSCRIPT_FILE_FAILED, `Load post action script failed. Action: ${this.getName()}`);
    }

    return script;
  }

  async execute(context: Context, args: any): Promise<void> {
    // eslint-disable-line @typescript-eslint/no-explicit-any
    //Store booking if not exist
    await this.storeBookingContainerAction.execute(context, {
      bookingNumber: args.bookingNumber,
      containerNumber: args.containerNumber,
    });

    if (await this.bookingContainerRepository.isExist(args.bookingNumber, args.containerNumber)) {
      //Add or update container activity if not exist
      let containerActivity = await this.getExistedActivity(args);
      if (!containerActivity) {
        containerActivity = await this.storeActivity(args);
      } else {
        await this.updateActivity(containerActivity, args);
      }

      //Add or update deadline activity
      if (containerActivity) {
        const deadline = await this.bookingContainerActivityDeadlineRepository.findValidBy(containerActivity.id);
        if (!deadline) {
          await this.storeDeadline(containerActivity, args.deadlineDate);
        } else {
          deadline.deadlineDate = args.deadlineDate;
          await this.bookingContainerActivityDeadlineRepository.save(deadline);
        }
      }
    }
  }

  private async getExistedActivity(args: any): Promise<BookingContainerActivity> {
    // eslint-disable-line @typescript-eslint/no-explicit-any
    return await this.bookingContainerActivityRepository.findValidBy(args.bookingNumber, args.containerNumber, args.matrixId);
  }

  private async storeActivity(args: any): Promise<BookingContainerActivity> {
    // eslint-disable-line @typescript-eslint/no-explicit-any
    const activity = new BookingContainerActivity({
      bookingNo: args.bookingNumber,
      containerNo: args.containerNumber,
      copSeq: args.copSequence,
      eventMatrixId: args.matrixId,
      opusCode: args.opusCode,
      vesselCode: args.vesselCode,
      countryId: args.countryId,
      locationId: args.locationId,
      isValid: 1,
      skdVoyNo: args.vesselScheduleNumber,
      skdDirCd: args.vesselScheduleDirectionCode,
    });

    await this.bookingContainerActivityRepository.save(activity);
    return activity;
  }

  private async updateActivity(activity: BookingContainerActivity, args: any): Promise<void> {
    // eslint-disable-line @typescript-eslint/no-explicit-any
    if (StringUtils.isNotEmpty(args.copSequence)) {
      activity.copSeq = args.copSequence;
    }

    if (StringUtils.isNotEmpty(args.opusCode)) {
      activity.opusCode = args.opusCode;
    }

    if (StringUtils.isNotEmpty(args.vesselCode)) {
      activity.vesselCode = args.vesselCode;
    }

    if (StringUtils.isNotEmpty(args.vesselScheduleNumber)) {
      activity.skdVoyNo = args.vesselScheduleNumber;
    }

    if (StringUtils.isNotEmpty(args.vesselScheduleDirectionCode)) {
      activity.skdDirCd = args.vesselScheduleDirectionCode;
    }

    if (StringUtils.isNotEmpty(args.countryId)) {
      activity.countryId = args.countryId;
    }

    if (StringUtils.isNotEmpty(args.locationId)) {
      activity.locationId = args.locationId;
    }

    await this.bookingContainerActivityRepository.save(activity);
  }

  private async storeDeadline(activity: BookingContainerActivity, deadlineDate: Date): Promise<BookingContainerActivityDeadline> {
    const deadline = new BookingContainerActivityDeadline({
      activityId: activity.id,
      deadlineDate: deadlineDate,
    });

    await this.bookingContainerActivityDeadlineRepository.save(deadline);
    return deadline;
  }
}
