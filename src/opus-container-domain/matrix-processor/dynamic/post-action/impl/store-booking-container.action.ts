/* eslint-disable  @typescript-eslint/no-explicit-any */
import { Injectable } from '@nestjs/common';
import { Inject } from '@nestjs/common/decorators';
import { Context } from 'src/opus-container-domain/dto';
import { LoaderTask } from 'src/opus-container-domain/matrix-processor/interface';
import { BookingContainer } from 'src/shared/domain/entity';
import { ValidateException } from 'src/shared/domain/model/exception';
import { BookingContainerRepository, BookingContainerRepositoryName } from 'src/shared/domain/repository/booking-container.interface';
import { ODSRepository, odsRepositoryName } from 'src/shared/domain/repository/ods-db-repository.interface';
import { ErrorCode } from 'src/utils/error.code';
import { StringUtils } from 'src/utils/string.utils';
import { BasePostAction } from '../base.post-action';

@Injectable()
export class StoreBookingContainerAction extends BasePostAction {
  bookingContainerActivityDeadlineRepository: any;
  bookingContainerActivityRepository: any;

  constructor(
    @Inject(BookingContainerRepositoryName) private readonly bookingContainerRepository: BookingContainerRepository,
    @Inject(odsRepositoryName) private readonly obsRepository: ODSRepository,
  ) {
    super();
  }

  getLoaderTasks(): LoaderTask[] {
    return [];
  }

  getName(): string {
    return 'STORE_BOOKING_CONTAINER_ACTION';
  }

  getInjectScript(): string {
    const script = this.loadScript('store-booking-container-action.js');
    if (StringUtils.isEmpty(script)) {
      throw new ValidateException(ErrorCode.LOADSCRIPT_FILE_FAILED, `Load post action script failed. Action: ${this.getName()}`);
    }

    return script;
  }

  async execute(context: Context, args: any): Promise<void> {
    if (args.containerNumber && args.bookingNumber) {
      if (!(await this.bookingContainerRepository.isExist(args.bookingNumber, args.containerNumber))) {
        const container = await this.obsRepository.getBookingContainer(args.bookingNumber, args.containerNumber);
        const supplyChainHeader = await this.obsRepository.getSceCopHdrByBookingAndContainer(args.bookingNumber, args.containerNumber);
        if (container && supplyChainHeader) {
          const bookingContainer = new BookingContainer({
            bookingNo: args.bookingNumber,
            containerNo: args.containerNumber,
            copNo: supplyChainHeader.COP_NO,
            typeSizeCode: container.CNTR_TPSZ_CD,
            movementYear: container.CNMV_YR,
            movementId: container.CNMV_ID_NO,
            movementCycleNo: container.CNMV_CYC_NO,
            movementStatus: container.CNMV_STS_CD,
            packageUnitCode: container.PCK_TP_CD,
            packageQuantity: container.PCK_QTY,
            packageWeightUnitCode: container.WGT_UT_CD,
            packageWeight: container.CNTR_WGT,
            measurementUnitCode: container.MEAS_UT_CD,
            measurementQuantity: container.MEAS_QTY,
            volumeQuantity: container.CNTR_VOL_QTY,
            isValid: true,
            createdBy: container.CRE_USR_ID,
            createdAt: new Date(),
            updatedBy: container.UPD_USR_ID,
            updatedAt: new Date(),
          });

          await this.bookingContainerRepository.save(bookingContainer);
        }
      }
    }
  }
}
