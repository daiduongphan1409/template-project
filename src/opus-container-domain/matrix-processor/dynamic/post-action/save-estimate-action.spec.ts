import * as fs from 'fs';
import { Test, TestingModule } from '@nestjs/testing';
import { SaveEstimateAction } from './save-estimate-action';
import { ContainerDomainEventEstimateRepositoryName } from 'src/shared/domain/repository/container-domain-event-estimate.interface';
import { ScriptResultAction } from 'src/opus-container-domain/dto/scrip-result-action.dto';

describe(`${SaveEstimateAction.name}`, () => {
  let postAction: SaveEstimateAction;
  const mockRepositorySave = jest.fn();
  beforeEach(async () => {
    mockRepositorySave.mockReset();
    mockRepositorySave.mockResolvedValue(true);
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SaveEstimateAction,
        {
          provide: ContainerDomainEventEstimateRepositoryName,
          useFactory: () => ({
            saveBookingContainer: mockRepositorySave,
          }),
        },
      ],
    }).compile();

    postAction = module.get(SaveEstimateAction);
  });

  it(`getLoaderTasks_should_alway_return_empty`, () => {
    //Assert
    expect(postAction).toBeDefined();
  });

  it(`getInjectScript_should_pass_with_data_mock`, () => {
    //Arrange
    const returnData = 'data check read fs function';
    //Act
    jest.spyOn(fs, 'readFileSync').mockReturnValueOnce(returnData);
    //Assert
    expect(postAction.getInjectScript()).toBe(returnData);
  });

  it('execute_should_pass_when_data_correct', async () => {
    //Arrange
    const action = new ScriptResultAction({
      name: 'STORE_ESTIMATE',
      parameters: {
        eventID: 'e026',
        bookingNumber: '1',
        containerNumber: '1',
        copNumber: '1',
        estimatedDate: '1',
        plannedDate: '1',
        scheduleVoyageNumber: '1',
        actualFilingScheduleDirectionCode: '1',
        callingPortIndicatorSequence: '1',
        vpsPortCode: '1',
        copDetailSequence: 1,
        actualCode: '1',
        vesselCode: '1',
        nodeCode: '1',
      },
    });
    //Act
    await postAction.execute(null, action.parameters);
    //Assert
    expect(mockRepositorySave.mock.calls).toHaveLength(1);
  });

  it('execute_should_pass_when_data_not_correct', async () => {
    //Arrange
    const action = new ScriptResultAction({
      name: 'STORE_ESTIMATE',
      parameters: {
        eventID: 'e026',
        bookingNumber: '1',
        containerNumber: '1',
        copNumber: '1',
        estimatedDate: '1',
        plannedDate: '1',
        scheduleVoyageNumber: '1',
        actualFilingScheduleDirectionCode: '1',
        callingPortIndicatorSequence: '1',
        vpsPortCode: '1',
        copDetailSequence: 1,
        actualCode: '1',
        vesselCode: '1',
        nodeCode: '1',
      },
    });
    //Act
    await postAction.execute(null, action);
    //Assert
    expect(mockRepositorySave.mock.calls).toHaveLength(0);
  });
});
