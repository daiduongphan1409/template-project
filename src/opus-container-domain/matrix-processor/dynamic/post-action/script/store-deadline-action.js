context.actions.storeDeadline = function (args) {
  
  if (!args.bookingNumber || args.bookingNumber.length <= 0){
    throw '[context.actions.storeDeadline] bookingNumber is invalid';
  }

  if (!args.containerNumber || args.containerNumber.length <= 0){
    throw '[context.actions.storeDeadline] containerNumber is invalid';
  }

  if (!args.matrixId || args.matrixId.length <= 0){
    throw '[context.actions.storeDeadline] matrixId is invalid';
  }

  context.result.actions.push({
    name: 'STORE_DEADLINE_ACTION',
    parameters: {
      bookingNumber: args.bookingNumber,
      containerNumber: args.containerNumber,
      matrixId: args.matrixId,
      copSequence: args.copSequence,
      vesselCode: args.vesselCode,
      vesselScheduleNumber: args.vesselScheduleNumber,
      vesselScheduleDirectionCode: args.vesselScheduleDirectionCode,
      opusCode: args.opusCode,
      countryId: args.locationId ? args.locationId.slice(0, 2) : null,
      locationId: args.locationId,
      deadlineDate: args.deadlineDate
    },
  });
};
