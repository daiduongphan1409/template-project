context.actions.storeBookingContainer = function (args) {
  context.result.actions.push({
    name: 'STORE_BOOKING_CONTAINER_ACTION',
    parameters: {
      bookingNumber: args.bookingNumber,
      containerNumber: args.containerNumber,
      bookingContainer: args.bookingContainer
    },
  });
};
