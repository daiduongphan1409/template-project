context.actions.storeDeadlineGateInData = function (context, date) {
  context.result.actions.push({
    name: 'STORE_DEADLINE',
    parameters: {
      data: context,
      date: date,
    },
  });
};