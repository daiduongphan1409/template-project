context.actions.storeEstimateData = function (args) {
  context.result.actions.push({
    name: 'STORE_ESTIMATE',
    parameters: {
      eventID: args.EVENT_ID,
      bookingNumber: args.BKG_NO,
      containerNumber: args.CNTR_NO,
      copNumber: args.COP_NO,
      estimatedDate: args.ESTM_DT,
      plannedDate: args.PLN_DT,
      scheduleVoyageNumber: args.SKD_VOY_NO,
      actualFilingScheduleDirectionCode: args.SKD_DIR_CD,
      callingPortIndicatorSequence: args.CLPT_IND_SEQ,
      vpsPortCode: args.VPS_PORT_CD,
      copDetailSequence: args.COP_DTL_SEQ,
      actualCode: args.ACT_CD,
      vesselCode: args.VSL_CD,
      nodeCode: args.NOD_CD,
    },
  });
};

function getSmallestSequence(records, actualCodes) {
  const record = records.filter((x) => actualCodes.includes(x.ACT_CD));
  if (record.lenght > 1) {
    let result = [record].flat().reduce((min, obj) => {
      return min.COP_DTL_SEQ > obj.COP_DTL_SEQ ? obj : min;
    }, []);
    return result;
  }
  return record[0];
}

function getBiggestSequence(records, actualCodes) {
  const record = records.filter((x) => actualCodes.includes(x.ACT_CD));
  if (record.lenght > 1) {
    let result = [record].flat().reduce((max, obj) => {
      return max.COP_DTL_SEQ < obj.COP_DTL_SEQ ? obj : max;
    }, []);
    return result;
  }
  return record[0];
}
