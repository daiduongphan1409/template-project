context.actions.storeFistReciveEstimateData = function (context, opusCode) {
  context.result.actions.push({
    name: 'STORE_FIST_RECIEVE_ESTIMATE',
    parameters: {
      bookingId: context.vars.bookingId,
      containerId: context.vars.containerId,
      eventId: context.event.matrixId,
      opusCode: opusCode,
      context: context,
    },
  });
};
