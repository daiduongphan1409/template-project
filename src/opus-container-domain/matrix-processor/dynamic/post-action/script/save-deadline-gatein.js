context.actions.storeDeadlineGateInData = function (context, date) {
  context.result.actions.push({
    name: 'STORE_DEADLINE_GATEIN',
    parameters: {
      eventId: context['event']['matrixId'],
      bookingContainers: context['vars']['SCE_COP_DTLS'].map((t) => {
        return { BKG_NO: t.header.BKG_NO, COP_NO: t.COP_NO, CNTR_NO: t.header.CNTR_NO, COP_DTL_SEQ: t.COP_DTL_SEQ, ACT_CD: t.ACT_CD };
      }),
      eventSummary: 'E026',
      date: date,
    },
  });
};
