import { Inject, Injectable } from '@nestjs/common';
import { PostAction } from '../../interface';

export const PostActionImpls = 'ListPostActionImpls';
export const PostActionFactoryName = 'PostActionFactoryName';

@Injectable()
export class PostActionFactory {
  constructor(@Inject(PostActionImpls) private readonly postActions: PostAction[]) {}

  public getList(): PostAction[] {
    return this.postActions;
  }

  public get(name: string): PostAction {
    return this.postActions.find((p) => p.getName() === name);
  }
}
