/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { LoaderTask, PostAction } from '../../interface';
import { PostActionFactoryName } from './post-action-factory';
import { PostActionInjector } from './post-action-injector';

describe(`${PostActionInjector.name}`, () => {
  let target: PostActionInjector;
  const fakeScript = 'Check test';
  const type = PostActionInjector.prototype;
  const mockPostActionGetInjectScript = jest.fn();
  beforeEach(async () => {
    mockPostActionGetInjectScript.mockReturnValue([new FakePostAction()]);
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostActionInjector,
        {
          provide: PostActionFactoryName,
          useFactory: () => ({
            getList: mockPostActionGetInjectScript,
          }),
        },
      ],
    }).compile();

    target = module.get(PostActionInjector);
  });

  it(`${type.append.name}_Should_Return_Fake_Script`, () => {
    //Assert
    expect(target.append("'<<post-actions>>'")).toBe(fakeScript);
  });

  class FakePostAction implements PostAction {
    getLoaderTasks(): LoaderTask[] {
      throw new Error('Method not implemented.');
    }

    execute(action: any): Promise<void> {
      throw new Error('Method not implemented.');
    }

    isExist(suggestActions: string[], logic: string): boolean {
      throw new Error('Method not implemented.');
    }

    getInjectScript(): string {
      return fakeScript;
    }

    getName(): string {
      throw new Error('Method not implemented.');
    }
  }
});
