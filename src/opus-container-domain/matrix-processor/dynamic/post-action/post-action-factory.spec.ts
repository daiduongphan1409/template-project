/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { LoaderTask, PostAction } from '../../interface';
import { PostActionFactory, PostActionImpls } from './post-action-factory';

describe(`${PostActionFactory.name}`, () => {
  let target: PostActionFactory;
  const fakeName = 'Check test';
  const type = PostActionFactory.prototype;
  const mockPostAction = jest.fn();
  beforeEach(async () => {
    mockPostAction.mockReturnValue([new FakePostAction()]);
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostActionFactory,
        {
          provide: PostActionImpls,
          useValue: [new FakePostAction()],
        },
      ],
    }).compile();

    target = module.get(PostActionFactory);
  });

  it(`${type.getList.name}_Should_Return_One_Item`, () => {
    //Assert
    expect(target.getList()).toHaveLength(1);
  });
  it(`${type.get.name}_Should_Return_Item`, () => {
    //Assert
    expect(target.get(fakeName)).toBeInstanceOf(FakePostAction);
  });
  it(`${type.get.name}_Should_Return_Undefine`, () => {
    //Assert
    expect(target.get('Test')).toBeUndefined();
  });
  class FakePostAction implements PostAction {
    getLoaderTasks(): LoaderTask[] {
      throw new Error('Method not implemented.');
    }

    execute(action: any): Promise<void> {
      throw new Error('Method not implemented.');
    }

    isExist(suggestActions: string[], logic: string): boolean {
      throw new Error('Method not implemented.');
    }

    getInjectScript(): string {
      throw new Error('Method not implemented.');
    }

    getName(): string {
      return fakeName;
    }
  }
});
