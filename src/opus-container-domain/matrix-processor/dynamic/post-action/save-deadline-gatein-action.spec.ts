import { Test, TestingModule } from '@nestjs/testing';
import { randomUUID } from 'crypto';
import * as fs from 'fs';
import { ScriptResultAction } from 'src/opus-container-domain/dto/scrip-result-action.dto';
import { BookingContainer, BookingContainerActivity, BookingContainerActivityDeadline } from 'src/shared/domain/entity';
import { BookingContainerActivityDeadlineHistoriesRepositoryName } from 'src/shared/domain/repository/booking-container-activity-deadline-histories.interface';
import { BookingContainerActivityDeadlineRepositoryName } from 'src/shared/domain/repository/booking-container-activity-deadline.interface';
import { BookingContainerActivityRepositoryName } from 'src/shared/domain/repository/booking-container-activity.interface';
import { BookingContainerRepositoryName } from 'src/shared/domain/repository/booking-container.interface';
import { SaveDeadlineGateInAction } from './save-deadline-gatein-action';

// eslint-disable-next-line jest/valid-title
describe(`${SaveDeadlineGateInAction.name}`, () => {
  let target: SaveDeadlineGateInAction;
  const type = SaveDeadlineGateInAction.prototype;
  const mockRepositorySave = jest.fn();
  const eventData = new BookingContainer({
    bookingNo: 'acb',
    activities: [],
  });
  const action = new ScriptResultAction({
    name: 'STORE_DEADLINE_GATEIN',
    parameters: {
      eventSummary: 'E026',
      eventId: 'E027',
      bookingContainers: [
        {
          COP_NO: 'fjhdfjd',
          COP_DTL_SEQ: '3',
          BKG_NO: 'hsghfghdf',
          CNTR_NO: 'fjhfjdh',
        },
      ],
    },
  });
  beforeEach(async () => {
    mockRepositorySave.mockReset();
    mockRepositorySave.mockResolvedValue(true);
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SaveDeadlineGateInAction,
        {
          provide: BookingContainerRepositoryName,
          useFactory: () => ({
            getEvent: jest.fn().mockReturnValue(eventData),
            save: mockRepositorySave,
          }),
        },
        {
          provide: BookingContainerActivityRepositoryName,
          useFactory: () => ({
            save: mockRepositorySave,
          }),
        },
        {
          provide: BookingContainerActivityDeadlineRepositoryName,
          useFactory: () => ({
            save: mockRepositorySave,
          }),
        },
        {
          provide: BookingContainerActivityDeadlineHistoriesRepositoryName,
          useFactory: () => ({
            save: mockRepositorySave,
          }),
        },
      ],
    }).compile();

    target = module.get(SaveDeadlineGateInAction);
  });

  it(`${type.getLoaderTasks.name}_should_alway_return_empty`, () => {
    //Assert
    expect(target.getLoaderTasks()).toMatchObject([]);
  });

  it(`${type.isExist.name}__should_alway_return_true`, () => {
    //Assert
    expect(target.isExist([], '')).toBe(true);
  });
  it(`${type.getName.name}__should_alway_return_STORE_DEADLINE_GATEIN`, () => {
    //Assert

    expect(target.getName()).toBe('STORE_DEADLINE_GATEIN');
  });
  it(`${type.getInjectScript.name}__should_pass_with_data_mock`, () => {
    //Act
    const returnData = 'data check read fs function';
    jest.spyOn(fs, 'readFileSync').mockReturnValueOnce(returnData);
    //Assert

    expect(target.getInjectScript()).toBe(returnData);
  });
  it(`${type.getInjectScript.name}__should_Return_Empty`, async () => {
    //Arrange
    const returnData = '';

    //Act
    jest.spyOn(fs, 'readFileSync').mockReturnValueOnce(returnData);

    //Assert
    await expect(target.getInjectScript()).toBe(returnData);
  });

  it(`${type.execute.name}__should_pass_when_not_activities`, async () => {
    //Act
    await target.execute(null, action.parameters);
    //Assert
    expect(mockRepositorySave.mock.calls).toHaveLength(4);
  });
  it(`${type.execute.name}__should_pass_when_have_activities_with_main_activity`, async () => {
    //Arrange
    eventData.activities = [
      new BookingContainerActivity({
        id: randomUUID(),
        bookingNo: '',
        containerNo: '',
        eventMatrixId: 'E027',
        activitydeadLines: [],
        activityDeadLineHistories: [],
      }),
    ];
    //Act
    await target.execute(null, action.parameters);
    //Assert
    expect(mockRepositorySave.mock.calls).toHaveLength(4);
  });
  it(`${type.execute.name}__should_pass_when_have_activities_with_main_and_summary_activity`, async () => {
    //Arrange
    eventData.activities = [
      new BookingContainerActivity({
        id: randomUUID(),
        bookingNo: '',
        containerNo: '',
        eventMatrixId: 'E027',
        activitydeadLines: [],
        activityDeadLineHistories: [],
      }),
      new BookingContainerActivity({
        id: randomUUID(),
        bookingNo: '',
        containerNo: '',
        eventMatrixId: 'E026',
        activitydeadLines: [],
        activityDeadLineHistories: [],
      }),
    ];
    //Act
    await target.execute(null, action.parameters);

    //Assert
    expect(mockRepositorySave.mock.calls).toHaveLength(4);
  });
  it(`${type.execute.name}__should_pass_when_have_activities_with_full_activity`, async () => {
    //Arrange
    eventData.activities = [
      new BookingContainerActivity({
        id: randomUUID(),
        bookingNo: '',
        containerNo: '',
        eventMatrixId: 'E027',
        activitydeadLines: [
          new BookingContainerActivityDeadline({
            id: randomUUID(),
            deadlineDate: new Date(),
          }),
        ],
        activityDeadLineHistories: [],
      }),
      new BookingContainerActivity({
        id: randomUUID(),
        bookingNo: '',
        containerNo: '',
        eventMatrixId: 'E026',
        activitydeadLines: [
          new BookingContainerActivityDeadline({
            id: randomUUID(),
            deadlineDate: new Date(),
          }),
        ],
        activityDeadLineHistories: [],
      }),
    ];
    //Act
    await target.execute(null, action.parameters);
    //Assert
    expect(mockRepositorySave.mock.calls).toHaveLength(4);
  });
});
