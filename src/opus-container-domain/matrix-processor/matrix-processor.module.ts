import { Global, Module } from '@nestjs/common';
import { MatrixProcessor, MatrixProcessorName } from './matrix-processor';
import { DefaultContextLoader } from './dynamic/default-context-loader';
import { DefaultLoaderTaskCollector } from './dynamic/default-loader-task-collector';
import { DefaultMatrixLogicAnalyzer } from './dynamic/default-matrix-logic-analyzer';
import { DynamicMatrixEvaluator } from './dynamic/dynamic-matrix-evaluator';
import { BkgClzTmLoader } from './dynamic/loader/loader-task-impl/bkg-clz-tm.loader';
import { PostActionImpls, ScriptInjectors } from './dynamic/post-action';
import { PostActionFactory, PostActionFactoryName } from './dynamic/post-action/post-action-factory';
import { PostActionInjector } from './dynamic/post-action/post-action-injector';
import { DefaultScriptBuilder } from './dynamic/script/default-script-builder';
import { DefaultScriptExecutor } from './dynamic/script/default-script-executor';
import { DefinedVariableFactory, DefinedVariableFactoryName } from './dynamic/variable/defined-variable-factory';
import { BkgClzTmVariable } from './dynamic/variable/impl/bkg-clz-tm.variable';
import { DedicatedMatrixEvaluator } from './dedicated/dedicated-matrix-evaluator';
import {
  BkgClzTmLoaderName,
  DefaultContextLoaderName,
  DedicatedMatrixEvaluatorName,
  DefaultScriptBuilderName,
  DefaultScriptExecutorName,
  DynamicMatrixEvaluatorName,
  SceCopDtlLoaderName,
  SceCopHdrLoaderName,
  SceCopDtlsLoaderName,
  DefaultLoaderTaskCollectorName,
  DefaultMatrixLogicAnalyzerName,
} from './interface';
import { ListDefinedVariablesImpl } from './dynamic/variable';
import { SceCopDtlVariable } from './dynamic/variable/impl/sce-cop-dtl.variable';
import { SceCopDtlLoader } from './dynamic/loader/loader-task-impl/sce-cop-dtl.loader';
import { SaveEstimateAction } from './dynamic/post-action/save-estimate-action';
import { SceCopHdrVariable } from './dynamic/variable/impl/sce-cop-hdr.variable';
import { SceCopHdrLoader } from './dynamic/loader/loader-task-impl/sce-cop-hdr.loader';
import { SceCopDtlsVariable } from './dynamic/variable/impl/sce-cop-dtls.variable';
import { SceCopDtlsLoader } from './dynamic/loader/loader-task-impl/sce-cop-dtls.loader';
import { LoaderContext } from './dynamic/loader/loader-context';
import { SaveDeadlineGateInAction } from './dynamic/post-action/save-deadline-gatein-action';
import { ListBookingCutOffTimeVariable } from './dynamic/variable/impl/list-booking-cutoff-time.variable';
import { TableBookingCutOffTimeVariable } from './dynamic/variable/impl/table-booking-cutoff-time.variable';
import { StoreDeadlinePostAction } from './dynamic/post-action/impl/store-deadline.action';
import { BookingNumberVariable } from './dynamic/variable/impl/booking-number.variable';
import { ContainerNumberVariable } from './dynamic/variable/impl/container-number.variable';
import { ListSupplyChainCopDetailVariable } from './dynamic/variable/impl/list-supply-chain-cop-detail.variable';
import { StoreBookingContainerAction } from './dynamic/post-action/impl/store-booking-container.action';
import { ListSupplyChainCopHeaderVariable } from './dynamic/variable/impl/list-supply-chain-cop-header.variable';
import { ListContainerNumberVariable } from './dynamic/variable/impl/list-container-number.variable';

@Global()
@Module({
  imports: [],
  providers: [
    {
      provide: PostActionFactoryName,
      useClass: PostActionFactory,
    },
    LoaderContext,
    PostActionInjector,
    {
      provide: ScriptInjectors,
      useFactory: (actionInjector) => [actionInjector],
      inject: [PostActionInjector],
    },
    {
      provide: DedicatedMatrixEvaluatorName,
      useClass: DedicatedMatrixEvaluator,
    },
    {
      provide: DynamicMatrixEvaluatorName,
      useClass: DynamicMatrixEvaluator,
    },
    {
      provide: DefaultContextLoaderName,
      useClass: DefaultContextLoader,
    },
    {
      provide: DefaultLoaderTaskCollectorName,
      useClass: DefaultLoaderTaskCollector,
    },
    {
      provide: DefinedVariableFactoryName,
      useClass: DefinedVariableFactory,
    },
    {
      provide: MatrixProcessorName,
      useClass: MatrixProcessor,
    },
    {
      provide: DefaultMatrixLogicAnalyzerName,
      useClass: DefaultMatrixLogicAnalyzer,
    },
    {
      provide: DefaultScriptBuilderName,
      useClass: DefaultScriptBuilder,
    },
    {
      provide: DefaultScriptExecutorName,
      useClass: DefaultScriptExecutor,
    },
    {
      provide: BkgClzTmLoaderName,
      useClass: BkgClzTmLoader,
    },
    {
      provide: SceCopDtlLoaderName,
      useClass: SceCopDtlLoader,
    },
    {
      provide: SceCopHdrLoaderName,
      useClass: SceCopHdrLoader,
    },
    {
      provide: SceCopDtlsLoaderName,
      useClass: SceCopDtlsLoader,
    },
    SaveEstimateAction,
    SaveDeadlineGateInAction,
    StoreBookingContainerAction,
    StoreDeadlinePostAction,
    {
      provide: PostActionImpls,
      useFactory: (gateIn, estimate, deadline, container) => [gateIn, estimate, deadline, container],
      inject: [SaveDeadlineGateInAction, SaveEstimateAction, StoreDeadlinePostAction, StoreBookingContainerAction],
    },
    BkgClzTmVariable,
    SceCopDtlVariable,
    SceCopHdrVariable,
    SceCopDtlsVariable,
    TableBookingCutOffTimeVariable,
    ListBookingCutOffTimeVariable,
    BookingNumberVariable,
    ContainerNumberVariable,
    ListSupplyChainCopDetailVariable,
    ListSupplyChainCopHeaderVariable,
    ListContainerNumberVariable,
    {
      provide: ListDefinedVariablesImpl,
      useFactory: (
        bkgClz,
        sceCopDtl,
        sceCopHdr,
        sceCopDtls,
        bookingCutOffTime,
        listBookingCutOffTime,
        bookingNumber,
        containerNumber,
        listSupplyChainCopDetail,
        listSupplyChainCopHeader,
        listContainerNumber,
      ) => [
        bkgClz,
        sceCopDtl,
        sceCopHdr,
        sceCopDtls,
        bookingCutOffTime,
        listBookingCutOffTime,
        bookingNumber,
        containerNumber,
        listSupplyChainCopDetail,
        listSupplyChainCopHeader,
        listContainerNumber,
      ],
      inject: [
        BkgClzTmVariable,
        SceCopDtlVariable,
        SceCopHdrVariable,
        SceCopDtlsVariable,
        TableBookingCutOffTimeVariable,
        ListBookingCutOffTimeVariable,
        BookingNumberVariable,
        ContainerNumberVariable,
        ListSupplyChainCopDetailVariable,
        ListSupplyChainCopHeaderVariable,
        ListContainerNumberVariable,
      ],
    },
  ],
  exports: [
    DedicatedMatrixEvaluatorName,
    DynamicMatrixEvaluatorName,
    DefaultContextLoaderName,
    DefaultLoaderTaskCollectorName,
    PostActionFactoryName,
    MatrixProcessorName,
    DefaultMatrixLogicAnalyzerName,
    ScriptInjectors,
    DefaultScriptExecutorName,
    DefaultScriptBuilderName,
    BkgClzTmLoaderName,
    PostActionImpls,
    ListDefinedVariablesImpl,
    SceCopDtlLoaderName,
    SceCopHdrLoaderName,
    SceCopDtlsLoaderName,
  ],
})
export class MatrixProcessorModule {}
