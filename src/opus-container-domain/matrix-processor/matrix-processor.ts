/* eslint-disable no-await-in-loop */
import { Inject, Injectable, Logger } from '@nestjs/common';
import { LogicType } from 'src/shared/domain/model/enum/matrix.enum';
import { EvaluationResult, EventMatrixDto, EventRequest } from '../dto';
import { PostActionFactory, PostActionFactoryName } from './dynamic/post-action/post-action-factory';
import { DedicatedMatrixEvaluatorName, DynamicMatrixEvaluatorName, MatrixEvaluator } from './interface';

export const MatrixProcessorName = 'MatrixProcessorName';

@Injectable()
export class MatrixProcessor {
  private logger = new Logger(MatrixProcessor.name);
  constructor(
    @Inject(DedicatedMatrixEvaluatorName) private readonly dedicatedMatrixEvaluator: MatrixEvaluator,
    @Inject(DynamicMatrixEvaluatorName) private readonly dynamicMatrixEvaluator: MatrixEvaluator,
    @Inject(PostActionFactoryName) private readonly postActionFactory: PostActionFactory,
  ) {}

  public async execute(matrices: EventMatrixDto[], request: EventRequest): Promise<EvaluationResult[]> {
    const dedicatedResult = await this.executeDedicated(matrices, request);
    const dynamicResult = await this.executeDynamic(matrices, request);
    return [dedicatedResult, dynamicResult];
  }

  private async executeDedicated(matrices: EventMatrixDto[], request: EventRequest): Promise<EvaluationResult> {
    try {
      const dedicatedMatrices = this.getDedicatedMatrices(matrices);
      const dedicatedResult = await this.dedicatedMatrixEvaluator.execute(dedicatedMatrices, request);
      if (dedicatedResult && dedicatedResult.matrixResults.length > 0) {
        this.executePostAction([dedicatedResult]);
      }
      return dedicatedResult;
    } catch (error) {
      const err = error as Error;
      this.logger.error(`Execute Dedicated run error Input: ${JSON.stringify(request)}, Message: ${err.message}`, err.stack);
    }
    return null;
  }

  private async executeDynamic(matrices: EventMatrixDto[], request: EventRequest) {
    try {
      const dynamicMatrices = this.getDynamicMatrices(matrices);
      const dynamicResult = await this.dynamicMatrixEvaluator.execute(dynamicMatrices, request);
      if (dynamicResult && dynamicResult.matrixResults.length > 0) {
        this.executePostAction([dynamicResult]);
      }
      return dynamicResult;
    } catch (error) {
      const err = error as Error;
      this.logger.error(`Execute Dynamic run error Input: ${JSON.stringify(request)}, Message: ${err.message}`, err.stack);
    }
    return null;
  }

  private getDedicatedMatrices(matrices: EventMatrixDto[]): EventMatrixDto[] {
    return matrices.filter((matrix) => matrix.logicType === LogicType.DEDICATED);
  }

  private getDynamicMatrices(matrices: EventMatrixDto[]): EventMatrixDto[] {
    return matrices.filter((matrix) => matrix.logicType === LogicType.DYNAMIC);
  }

  private async executePostAction(evaluationResults: EvaluationResult[]): Promise<void> {
    for (const evaluationResult of evaluationResults) {
      if (evaluationResult.matrixResults) {
        for (const matrixResult of evaluationResult.matrixResults) {
          try {
            for (const action of matrixResult.actions) {
              const postAction = this.postActionFactory.get(action.name);
              if (postAction !== null) {
                await postAction.execute(matrixResult.context, action.parameters);
              }
            }
          } catch (error) {
            const err = error as Error;
            this.logger.error(`Execute PostAction run error Input: ${JSON.stringify(evaluationResults)}, Message: ${err.message}`, err.stack);
          }
        }
      }
    }
  }
}
