import { ErrorCode } from 'src/utils/error.code';
import { ValidateException } from './../shared/domain/model/exception/validate.exception';
import { BaseException } from 'src/shared/domain/model/exception';
import { Controller, Inject, Logger } from '@nestjs/common';
import { ClientKafka, Ctx, EventPattern, KafkaContext, Payload } from '@nestjs/microservices';
import { ApiTags } from '@nestjs/swagger';
import { KAFKA_CLIENT } from 'src/utils/event';
import { DistributePayload } from './dto/distribute-payload.dto';
import { OpusContainerDomainService } from './service/opus-container-domain.service';
import { randomUUID } from 'crypto';

@ApiTags('opus-container')
@Controller('opus-container')
export class OpusContainerDomainController {
  constructor(private readonly opusContainerDomainService: OpusContainerDomainService, @Inject(KAFKA_CLIENT) private readonly client: ClientKafka) {}

  async onModuleInit() {
    await this.client.connect();
  }

  @EventPattern('CONTAINER_CREATE')
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async distributeContainerEvent(@Payload() distributePayLoad: DistributePayload, @Ctx() ctx: KafkaContext): Promise<void> {
    return await this.callExecute(distributePayLoad);
  }

  private async callExecute(distributePayLoad: DistributePayload) {
    if (!distributePayLoad) {
      throw new ValidateException(ErrorCode.PAYLOAD_IS_NULL, 'Payload is null');
    }
    const parentId = randomUUID();
    try {
      Logger.log(`Receive event , Item: ${JSON.stringify(distributePayLoad.event_id)}`);
      await this.opusContainerDomainService.execute(distributePayLoad);
    } catch (ex) {
      if (ex instanceof BaseException) {
        Logger.warn(`Event invalid. logId: ${parentId}, Code:${ex.code}, Message: ${ex.message}`);
      } else {
        const error = ex as Error;
        Logger.error(`Process event error. ParentId: ${parentId}, Massage: ${error.message}`, error.stack);
      }
    }
  }
}
