export class LogicAnalyzedTable {
  name: string;
  columns: Array<string>;
  constructor(init?: Partial<LogicAnalyzedTable>) {
    Object.assign(this, init);
  }
}
