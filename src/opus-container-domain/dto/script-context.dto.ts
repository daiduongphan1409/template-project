/* eslint-disable @typescript-eslint/no-explicit-any */
import { Logger } from '@nestjs/common';
import { EventMatrixDto } from './event-matrix.dto';
import { EventRequest } from './event-request.dto';
import { ScriptResult } from './script-result.dto';

/* eslint-disable @typescript-eslint/ban-types */
export class ScriptContext {
  request: EventRequest;
  event: EventMatrixDto;
  vars: any;
  actions: any;
  result: ScriptResult;
  logger: Logger;

  constructor(init?: Partial<ScriptContext>) {
    Object.assign(this, init);
  }
}
