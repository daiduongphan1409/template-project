import { IsNotEmpty } from 'class-validator';
import { CdcEventPayload } from './cdc-event-payload.dto';

export class EventRequest {
  @IsNotEmpty()
  parentId: string;

  @IsNotEmpty()
  eventId: string;

  @IsNotEmpty()
  originTopic: string;

  @IsNotEmpty()
  originData: CdcEventPayload;

  bookingNo: string;

  containerNo: string;

  constructor(init?: Partial<EventRequest>) {
    Object.assign(this, init);
  }
}
