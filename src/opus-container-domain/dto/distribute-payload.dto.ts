import { IsNotEmpty } from 'class-validator';
import { CdcEventPayload } from './cdc-event-payload.dto';

export class DistributePayload {
  @IsNotEmpty()
  parent_id: string;

  @IsNotEmpty()
  event_id: string;

  @IsNotEmpty()
  origin_topic: string;

  @IsNotEmpty()
  origin_data: CdcEventPayload;

  @IsNotEmpty()
  change_columns: Array<string>;

  constructor(init?: Partial<DistributePayload>) {
    Object.assign(this, init);
  }
}
