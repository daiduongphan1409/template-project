import { EventMatrixDto } from './event-matrix.dto';
import { ScriptResultAction } from './scrip-result-action.dto';

export class ScriptResult {
  event: EventMatrixDto;
  actions: Array<ScriptResultAction>;

  constructor(init?: Partial<ScriptResult>) {
    Object.assign(this, init);
  }
}
