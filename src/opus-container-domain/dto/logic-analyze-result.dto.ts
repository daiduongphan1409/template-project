import { LogicAnalyzedTable } from './logic-analyzed-table.dto';

export class LogicAnalyzeResult {
  variables: Array<string>;
  functions: Array<string>;
  tables: Array<LogicAnalyzedTable>;
  constructor(init?: Partial<LogicAnalyzeResult>) {
    Object.assign(this, init);
  }
}
