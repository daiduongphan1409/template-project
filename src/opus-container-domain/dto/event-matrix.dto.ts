import { LogicType, TriggerType } from 'src/shared/domain/model/enum/matrix.enum';

export class EventMatrixDto {
  logicType: LogicType;
  trigger: TriggerType;
  matrixId: string;
  script: string;

  constructor(init?: Partial<EventMatrixDto>) {
    Object.assign(this, init);
  }
}
