import { IsNotEmpty } from 'class-validator';

export class MetaPayload {
  @IsNotEmpty()
  TableID: number;

  @IsNotEmpty()
  TableName: string;

  @IsNotEmpty()
  TxnID: string;

  @IsNotEmpty()
  OperationName: string;

  @IsNotEmpty()
  FileName: string;

  @IsNotEmpty()
  FileOffset: number;

  TimeStamp: string;
  'Oracle ROWID': string;
  @IsNotEmpty()
  CSN: string;

  @IsNotEmpty()
  RecordStatus: string;

  constructor(init?: Partial<MetaPayload>) {
    Object.assign(this, init);
  }
}
