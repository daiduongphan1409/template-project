/* eslint-disable @typescript-eslint/no-explicit-any */
import { EventMatrixDto } from './event-matrix.dto';
import { EventRequest } from './event-request.dto';
import { LogicAnalyzeResult } from './logic-analyze-result.dto';

export class Context {
  request: EventRequest;
  event: EventMatrixDto;
  analyzed: LogicAnalyzeResult;
  vars: any = {};

  constructor(init?: Partial<Context>) {
    Object.assign(this, init);
  }
}
