/* eslint-disable @typescript-eslint/no-explicit-any */
import { Context } from './context.dto';

export class MatrixResult {
  context: Context;
  actions: any;
  constructor(init?: Partial<MatrixResult>) {
    Object.assign(this, init);
  }
}
