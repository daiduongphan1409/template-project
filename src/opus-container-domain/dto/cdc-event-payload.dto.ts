import { IsNotEmpty } from 'class-validator';
import { MetaPayload } from './meta-payload.dto';

export class CdcEventPayload {
  @IsNotEmpty()
  metadata: MetaPayload;

  @IsNotEmpty()
  data: Object; // eslint-disable-line
  before: Object; // eslint-disable-line
  userdata: Object; // eslint-disable-line
  __striimmetadata: Object; // eslint-disable-line

  constructor(init?: Partial<CdcEventPayload>) {
    Object.assign(this, init);
  }
}
