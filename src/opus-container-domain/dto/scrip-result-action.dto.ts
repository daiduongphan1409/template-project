/* eslint-disable @typescript-eslint/no-explicit-any */
export class ScriptResultAction {
  public name: string;
  public parameters: any;

  constructor(init?: Partial<ScriptResultAction>) {
    Object.assign(this, init);
  }
}
