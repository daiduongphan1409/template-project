import { MatrixResult } from './matrix-result.dto';

export class EvaluationResult {
  matrixResults: MatrixResult[];

  constructor(init?: Partial<EvaluationResult>) {
    Object.assign(this, init);
  }
}
