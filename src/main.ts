/* istanbul ignore file */
import { INestApplication, Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import chalk = require('chalk');
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { HttpExceptionFilter, InternalExceptionFilter, ValidateExceptionFilter } from './utils/filter/exception.filter';
import { RequestLoggingInterceptor } from './utils/interceptor/request-logging.interceptor';
import { getKafkaConfigs } from './utils/event';

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule);

    logAppEnv();
    configure(app);

    await startHttp(app);
    await startEvent(app);

    logAppPath(app);
  } catch (error) {
    Logger.error(`❌  Error starting server, ${error}`, '', 'Bootstrap');
    process.exit();
  }
}

function configure(app: INestApplication) {
  app.useGlobalInterceptors(new RequestLoggingInterceptor());

  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalFilters(new ValidateExceptionFilter());
  app.useGlobalFilters(new InternalExceptionFilter());

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );

  useSwagger(app);
}

function useSwagger(app: INestApplication) {
  const configService = app.get(ConfigService);
  const APP_NAME = configService.get('APP_NAME', 'Container service');
  const APP_DESCRIPTION = configService.get('APP_DESCRIPTION', 'Project for template purpose');
  const API_VERSION = configService.get('API_VERSION', 'v1');
  const options = new DocumentBuilder().setTitle(APP_NAME).setDescription(APP_DESCRIPTION).setVersion(API_VERSION).build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  SwaggerModule.setup('/', app, document);

  Logger.log('Mapped {/, GET} Swagger api route', 'RouterExplorer');
  Logger.log('Mapped {/api, GET} Swagger api route', 'RouterExplorer');
}

async function startHttp(app: INestApplication) {
  const configService = app.get(ConfigService);
  await app.listen(configService.get('PORT', '3000'));
}

async function startEvent(app: INestApplication) {
  const configService = app.get(ConfigService);
  app.connectMicroservice({
    ...getKafkaConfigs(configService),
  });

  await app.startAllMicroservices();
}

function logAppEnv() {
  Logger.log(`Environment: ${chalk.hex('#87e8de').bold(`${process.env['NODE_ENV']?.toUpperCase()}`)}`, 'Bootstrap');
}

function logAppPath(app: INestApplication) {
  const configService = app.get(ConfigService);
  const HOST = configService.get('HOST', 'localhost');
  const PORT = configService.get('PORT', '3000');

  process.env['NODE_ENV'] !== 'production'
    ? Logger.log(`🚀  Server ready at http://${HOST}:${chalk.hex('#87e8de').bold(`${PORT}`)}`, 'Bootstrap')
    : Logger.log(`🚀  Server is listening on port ${chalk.hex('#87e8de').bold(`${PORT}`)}`, 'Bootstrap');
}

bootstrap().catch((e) => {
  Logger.error(`❌  Error starting server, ${e}`, '', 'Bootstrap');
  throw e;
});
