import { Controller, Inject, Logger } from '@nestjs/common';
import { ClientKafka, Ctx, EventPattern, KafkaContext, Payload } from '@nestjs/microservices';
import { ValidateException } from 'src/shared/domain/model/exception';
import { Constants } from 'src/utils/constants';
import { ErrorCode } from 'src/utils/error.code';
import { KAFKA_CLIENT } from 'src/utils/event';
import { EventId } from 'src/utils/eventId';
import { DomainEventRequest } from './dto/domain-event-request.dto';
import { CancelTrackerScheduleService } from './service/cancel-tracker-scheduler.service';
import { RegisterTrackerScheduleService } from './service/register-tracker-scheduler.service';

@Controller('opus-IOT')
export class OpusDomainController {
  constructor(
    @Inject(KAFKA_CLIENT)
    private readonly client: ClientKafka,
    @Inject(RegisterTrackerScheduleService)
    private readonly registerTrackerScheduleService: RegisterTrackerScheduleService,
    @Inject(CancelTrackerScheduleService)
    private readonly cancelTrackerScheduleService: CancelTrackerScheduleService,
  ) {}

  /* istanbul ignore next */
  async onModuleInit() {
    await this.client.connect();
  }

  @EventPattern(Constants.CONTAINER_DOMAIN)
  async domainEventHandler(@Payload() payload: DomainEventRequest, @Ctx() ctx: KafkaContext): Promise<void> {
    if (!ctx) {
      throw new ValidateException(ErrorCode.CONTEXT_IS_NULL, 'Context is null');
    }
    if (!payload) {
      throw new ValidateException(ErrorCode.PAYLOAD_IS_NULL, 'Payload is null');
    }

    if (payload.code === EventId.E012) {
      await this.processEventE012(payload);
    }
    if (payload.code === EventId.E138) {
      await this.processEventE138(payload);
    }
  }

  private async processEventE012(request: DomainEventRequest): Promise<void> {
    try {
      await this.registerTrackerScheduleService.execute(request);
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Register tracker schedule failed : ${error}`);
    }
  }

  private async processEventE138(request: DomainEventRequest): Promise<void> {
    try {
      await this.cancelTrackerScheduleService.execute(request);
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Cancel tracker schedule failed : ${error}`);
    }
  }
}
