import { KafkaContext } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { Consumer, KafkaMessage, Producer } from 'kafkajs';
import { OpusDomainController } from './opus-domain.controller';
import { DomainEventRequest } from './dto/domain-event-request.dto';
import { RegisterTrackerScheduleService } from './service/register-tracker-scheduler.service';
import { CancelTrackerScheduleService } from './service/cancel-tracker-scheduler.service';
const args = [
  'test',
  { test: true },
  undefined,
  { test: 'consumer' },
  () => {}, // eslint-disable-line
  { test: 'producer' },
];
const context = new KafkaContext(args as [KafkaMessage, number, string, Consumer, () => Promise<void>, Producer]);

describe(`${OpusDomainController.name}`, () => {
  let controller: OpusDomainController;
  const type = OpusDomainController.prototype;
  const jestMockResolveData = { code: 1, message: 'Pass', data: {} };
  const eventE012Payload = () =>
    new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: Object, // eslint-disable-line
      data: Object, // eslint-disable-line
      matrix_id: '1',
    });
  const eventE138Payload = () =>
    new DomainEventRequest({
      code: 'E138',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: Object, // eslint-disable-line
      data: Object, // eslint-disable-line
      matrix_id: '1',
    });

  const mockRegisterTrackerScheduleServiceExecute = jest.fn();
  const mockCancelTrackerScheduleServiceExecute = jest.fn();

  beforeEach(async () => {
    mockRegisterTrackerScheduleServiceExecute.mockReset();
    mockRegisterTrackerScheduleServiceExecute.mockResolvedValue(jestMockResolveData);
    mockCancelTrackerScheduleServiceExecute.mockReset();
    mockCancelTrackerScheduleServiceExecute.mockResolvedValue(jestMockResolveData);

    const module: TestingModule = await Test.createTestingModule({
      controllers: [OpusDomainController],
      providers: [
        {
          provide: RegisterTrackerScheduleService,
          useFactory: () => ({
            execute: mockRegisterTrackerScheduleServiceExecute,
          }),
        },
        {
          provide: CancelTrackerScheduleService,
          useFactory: () => ({
            execute: mockCancelTrackerScheduleServiceExecute,
          }),
        },
        {
          provide: 'KAFKA_DEFAULT_CLIENT',
          useFactory: () => ({
            connect: jest.fn().mockReturnValue(true),
          }),
        },
      ],
    }).compile();

    controller = module.get<OpusDomainController>(OpusDomainController);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  it(`${type.domainEventHandler.name}_Should_ThrowValidateException_PayloadIsNull`, async () => {
    const result = controller.domainEventHandler(null, context);

    await expect(result).rejects.toThrow('Payload is null');
  });
  it(`${type.onModuleInit.name}_Should_Run_When_DataCorrect`, async () => {
    const result = controller.onModuleInit();

    await expect(result).resolves.toBeUndefined();
  });
  it(`${type.domainEventHandler.name}_Should_ThrowValidateException_ContextIsNull`, async () => {
    const result = controller.domainEventHandler(eventE012Payload(), null);

    await expect(result).rejects.toThrow('Context is null');
  });
  it(`${type.domainEventHandler.name}_Should_Send_Service_When_Event_Code_Is_E012`, async () => {
    await controller.domainEventHandler(eventE012Payload(), context);

    expect(mockRegisterTrackerScheduleServiceExecute.mock.calls).toHaveLength(1);
  });

  it(`${type.domainEventHandler.name}_Should_Send_Service_When_Event_Code_Is_E138`, async () => {
    await controller.domainEventHandler(eventE138Payload(), context);

    expect(mockCancelTrackerScheduleServiceExecute.mock.calls).toHaveLength(1);
  });
  it(`${type.domainEventHandler.name}_Should_Send_Service_When_Event_Code_Is_Not_E012_AND_E138`, async () => {
    const payloadFaild = () =>
      new DomainEventRequest({
        code: '1',
        id: '1',
        source: 'container',
        type: '1',
        data_type: '1',
        action: '1',
        create_time: '1',
        identity: Object, // eslint-disable-line
        data: Object, // eslint-disable-line
        matrix_id: '1',
      });
    await controller.domainEventHandler(payloadFaild(), context);

    expect(mockRegisterTrackerScheduleServiceExecute.mock.calls).toHaveLength(0);
  });
});
