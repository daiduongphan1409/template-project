import { IsNotEmpty } from 'class-validator';

export class RetrieveTrackerInfoRequest {
  @IsNotEmpty()
  scheduler_id: string;

  constructor(init?: Partial<RetrieveTrackerInfoRequest>) {
    Object.assign(this, init);
  }
}
