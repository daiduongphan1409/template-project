import { IsNotEmpty } from 'class-validator';

export class SchedulerCancelResponse {
  @IsNotEmpty()
  code: number;

  @IsNotEmpty()
  message: string;

  @IsNotEmpty()
  data: null;

  constructor(init?: Partial<SchedulerCancelResponse>) {
    Object.assign(this, init);
  }
}
