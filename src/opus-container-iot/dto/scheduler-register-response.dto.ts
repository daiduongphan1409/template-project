import { IsNotEmpty } from 'class-validator';

export class SchedulerRegisterResponse {
  @IsNotEmpty()
  code: number;

  @IsNotEmpty()
  message: string;

  @IsNotEmpty()
  data: {
    scheduler_id: string;
  };

  constructor(init?: Partial<SchedulerRegisterResponse>) {
    Object.assign(this, init);
  }
}
