import { IsNotEmpty } from 'class-validator';

export class SchedulerRepeat {
  @IsNotEmpty()
  every: string;

  @IsNotEmpty()
  until: string;

  constructor(init?: Partial<SchedulerRepeat>) {
    Object.assign(this, init);
  }
}
