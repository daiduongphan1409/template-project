import { IsNotEmpty } from 'class-validator';
import { SchedulerRegisterPayload } from './scheduler-register-payload.dto';

export class SchedulerRegisterData {
  @IsNotEmpty()
  topic: string;

  @IsNotEmpty()
  key: string;

  @IsNotEmpty()
  payload: SchedulerRegisterPayload;

  constructor(init?: Partial<SchedulerRegisterData>) {
    Object.assign(this, init);
  }
}
