import { IsNotEmpty } from 'class-validator';

export class SchedulerRegisterPayload {
  customer_no: string;

  @IsNotEmpty()
  booking_no: string;

  @IsNotEmpty()
  container_no: string;

  constructor(init?: Partial<SchedulerRegisterPayload>) {
    Object.assign(this, init);
  }
}
