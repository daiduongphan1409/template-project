import { IsNotEmpty } from 'class-validator';
import { SchedulerRegisterData } from './scheduler-register-data.dto';
import { SchedulerRepeat } from './scheduler-repeat.dto';

export class SchedulerRegisterRequest {
  @IsNotEmpty()
  start_time: string;

  @IsNotEmpty()
  delay: string;

  @IsNotEmpty()
  repeat: SchedulerRepeat;

  @IsNotEmpty()
  data: SchedulerRegisterData;

  constructor(init?: Partial<SchedulerRegisterRequest>) {
    Object.assign(this, init);
  }
}
