import { IsNotEmpty } from 'class-validator';

export class DomainEventRequest {
  @IsNotEmpty()
  code: string;

  @IsNotEmpty()
  id: string;

  source: string;
  type: string;
  data_type: string;
  action: string;
  create_time: string;
  identity: Object; // eslint-disable-line
  data: Object; // eslint-disable-line
  matrix_id: string;

  constructor(init?: Partial<DomainEventRequest>) {
    Object.assign(this, init);
  }
}
