import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientsModule } from '@nestjs/microservices';
import { KAFKA_CLIENT, configKafkaEventFactory } from 'src/utils/event';
import { OpusDomainController } from './opus-domain.controller';
import { OpusScheduleController } from './opus-scheduler.controller';
import { CancelTrackerScheduleService } from './service/cancel-tracker-scheduler.service';
import { RegisterTrackerScheduleService } from './service/register-tracker-scheduler.service';
import { RetrieveTrackerInfoService } from './service/retrieve-tracker-info.service';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: KAFKA_CLIENT,
        useFactory: configKafkaEventFactory,
        inject: [ConfigService],
      },
    ]),
  ],
  providers: [RetrieveTrackerInfoService, CancelTrackerScheduleService, RegisterTrackerScheduleService],
  controllers: [OpusScheduleController, OpusDomainController],
})
export class OpusContainerIotModule {}
