import { Test, TestingModule } from '@nestjs/testing';
import { OpusScheduleController } from './opus-scheduler.controller';
import { RetrieveTrackerInfoRequest } from './dto/retrieve-tracker-info-request.dto';
import { RetrieveTrackerInfoService } from './service/retrieve-tracker-info.service';

describe(`${OpusScheduleController.name}`, () => {
  let controller: OpusScheduleController;
  const type = OpusScheduleController.prototype;
  const jestMockResolveData = { code: 1, message: 'Pass', data: {} };
  const retrieveTrackerPayload = () =>
    new RetrieveTrackerInfoRequest({
      scheduler_id: 'string',
    });

  const mockRetrieveTrackerInfoServiceServiceExecute = jest.fn();

  beforeEach(async () => {
    mockRetrieveTrackerInfoServiceServiceExecute.mockReset();
    mockRetrieveTrackerInfoServiceServiceExecute.mockResolvedValue(jestMockResolveData);

    const module: TestingModule = await Test.createTestingModule({
      controllers: [OpusScheduleController],
      providers: [
        {
          provide: RetrieveTrackerInfoService,
          useFactory: () => ({
            execute: mockRetrieveTrackerInfoServiceServiceExecute,
          }),
        },
        {
          provide: 'KAFKA_DEFAULT_CLIENT',
          useFactory: () => ({
            connect: jest.fn().mockReturnValue(true),
          }),
        },
      ],
    }).compile();

    controller = module.get<OpusScheduleController>(OpusScheduleController);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  it(`${type.opusCdcEventHandler.name}_Should_ThrowValidateException_PayloadIsNull`, async () => {
    const result = controller.opusCdcEventHandler(null);

    await expect(result).rejects.toThrow('Payload is null');
  });
  it(`${type.onModuleInit.name}_Should_Run_When_DataCorrect`, async () => {
    const result = controller.onModuleInit();

    await expect(result).resolves.toBeUndefined();
  });
  it(`${type.opusCdcEventHandler.name}_Should_Send_Service_When_Data_Correct`, async () => {
    await controller.opusCdcEventHandler(retrieveTrackerPayload());

    expect(mockRetrieveTrackerInfoServiceServiceExecute.mock.calls).toHaveLength(1);
  });
});
