import { Inject, Injectable, Logger } from '@nestjs/common';
import { SchedulerApi, SchedulerApiName } from 'src/shared/domain/api/scheduler.api.interface';
import { SchedulerEntity } from 'src/shared/domain/entity';
import { ValidateException } from 'src/shared/domain/model/exception';
import {
  BookingContainerSchedulerRepository,
  BookingContainerSchedulerRepositoryName,
} from 'src/shared/domain/repository/booking-container-scheduler.repository.interface';
import { ErrorCode } from 'src/utils/error.code';
import { DomainEventRequest } from '../dto/domain-event-request.dto';

@Injectable()
export class CancelTrackerScheduleService {
  constructor(
    @Inject(SchedulerApiName)
    private readonly schedulerApi: SchedulerApi,
    @Inject(BookingContainerSchedulerRepositoryName)
    private readonly schedulerRepo: BookingContainerSchedulerRepository,
  ) {}

  public async execute(request: DomainEventRequest): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
    if (!request.identity || !request.identity['booking_no'] || !request.identity['container_no']) {
      throw new ValidateException(ErrorCode.PAYLOAD_INVALID, 'Payload identity invalid');
    }
    const schedulerId = (await this.getScheduler(request.identity['booking_no'], request.identity['container_no']))?.schedulerId;
    if (schedulerId) {
      await this.cancelScheduler(schedulerId);
    }
  }

  private async cancelScheduler(schedulerId: string): Promise<void> {
    try {
      await this.schedulerApi.cancel(schedulerId);
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Process cancel scheduler error , Message: ${error.message} `, error.stack);
    }
  }

  private async getScheduler(bookingNo: string, containerNo: string): Promise<SchedulerEntity> {
    try {
      return await this.schedulerRepo.getScheduler(bookingNo, containerNo);
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Process get scheduler id error , Message: ${error.message} `, error.stack);
      return null;
    }
  }
}
