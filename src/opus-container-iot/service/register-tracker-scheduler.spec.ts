import { Test, TestingModule } from '@nestjs/testing';
import { SchedulerApiName } from 'src/shared/domain/api/scheduler.api.interface';
// import { SchedulerEnity } from 'src/shared/domain/entity';
import { BookingContainerSchedulerRepositoryName } from 'src/shared/domain/repository/booking-container-scheduler.repository.interface';
import { DomainEventRequest } from '../dto/domain-event-request.dto';
import { RegisterTrackerScheduleService } from './register-tracker-scheduler.service';

describe(`${RegisterTrackerScheduleService.name}`, () => {
  let service: RegisterTrackerScheduleService;
  const type = RegisterTrackerScheduleService.prototype;
  const jestMockResolveData = { code: 1, message: 'Pass', data: {} };

  const mockRegisterTrackerScheduleService = jest.fn();
  const mockSaveScheduler = jest.fn();
  const mockRegister = jest.fn();

  beforeEach(async () => {
    mockRegisterTrackerScheduleService.mockReset();
    mockRegisterTrackerScheduleService.mockResolvedValue(jestMockResolveData);

    mockSaveScheduler.mockReset();
    mockSaveScheduler.mockReturnValue(jestMockResolveData);

    mockRegister.mockReset();
    mockRegister.mockReturnValue(jestMockResolveData);
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RegisterTrackerScheduleService],
      providers: [
        {
          provide: SchedulerApiName,
          useFactory: () => ({
            register: mockRegister,
          }),
        },
        {
          provide: BookingContainerSchedulerRepositoryName,
          useFactory: () => ({
            saveScheduler: mockSaveScheduler,
            save: mockSaveScheduler,
          }),
        },
        {
          provide: 'KAFKA_DEFAULT_CLIENT',
          useFactory: () => ({
            connect: jest.fn().mockReturnValue(true),
          }),
        },
      ],
    }).compile();

    service = module.get<RegisterTrackerScheduleService>(RegisterTrackerScheduleService);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  it(`${type.execute.name}_Should_ThrowValidateException_When_Missing_Booking_No_Identity`, async () => {
    const bookingNoIsNUll = new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: {
        booking_no: null,
        container_no: 'b123',
      },
      data: {
        booking_no: null,
        container_no: 'b123',
      },
      matrix_id: '1',
    });
    const result = service.execute(bookingNoIsNUll);

    await expect(result).rejects.toThrow('Payload identity invalid');
  });
  it(`${type.execute.name}_Should_ThrowValidateException_When_Missing_Container_No_Identity`, async () => {
    const bookingNoIsNUll = new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: {
        booking_no: 'acbd12345678',
        container_no: null,
      },
      data: {
        booking_no: 'acbd12345678',
        container_no: null,
      },
      matrix_id: '1',
    });
    const result = service.execute(bookingNoIsNUll);

    await expect(result).rejects.toThrow('Payload identity invalid');
  });
  it(`${type.execute.name}_Should_Run_Successfully_When_Booking_And_Container_Is_Correct`, async () => {
    const request = new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: {
        booking_no: 'bkg12345678',
        container_no: 'acbd12345678',
      },
      data: {
        booking_no: 'bkg12345678',
        container_no: 'acbd12345678',
      },
      matrix_id: '1',
    });
    await service.execute(request);
    expect(mockRegister.mock.calls).toHaveLength(1);
    expect(mockSaveScheduler.mock.calls).toHaveLength(1);
  });
});
