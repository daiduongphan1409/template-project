import { Inject, Injectable } from '@nestjs/common';
import { TailwindApi, TailwindApiName } from 'src/shared/domain/api/tailwind.api.interface';
import { TrackerSensorLogRepositoryName, TrackerSensorLogRepository } from 'src/shared/domain/repository/tracker-sensor-log.repository.interface.ts';
import {
  TrackerLocationLogRepository,
  TrackerLocationLogRepositoryName,
} from 'src/shared/domain/repository/tracker-location-log.repository.interface';
import { RetrieveTrackerInfoRequest } from '../dto/retrieve-tracker-info-request.dto';
import {
  BookingContainerSchedulerRepository,
  BookingContainerSchedulerRepositoryName,
} from 'src/shared/domain/repository/booking-container-scheduler.repository.interface';
import { ValidateException } from 'src/shared/domain/model/exception';
import { ErrorCode } from 'src/utils/error.code';
import { LocationInfo } from 'src/shared/domain/entity/api-response/location-info.entity';
import { TrackerLocationEntity, TrackerSensorEntity } from 'src/shared/domain/entity';
import { SensorInfo } from 'src/shared/domain/entity/api-response/sensor-info.entity';
@Injectable()
export class RetrieveTrackerInfoService {
  constructor(
    @Inject(TailwindApiName)
    private readonly tailwindApi: TailwindApi,
    @Inject(TrackerLocationLogRepositoryName)
    private readonly trackerLocationRepo: TrackerLocationLogRepository,
    @Inject(TrackerSensorLogRepositoryName)
    private readonly trackerSensorRepo: TrackerSensorLogRepository,
    @Inject(BookingContainerSchedulerRepositoryName)
    private readonly bookingContainerSchedulerRepository: BookingContainerSchedulerRepository,
  ) {}

  async execute(request: RetrieveTrackerInfoRequest) {
    if (!request?.scheduler_id) {
      throw new ValidateException(ErrorCode.SCHEDULER_ID_IS_NULL, 'Scheduler ID is null');
    }

    const bookingContainerItem = await this.bookingContainerSchedulerRepository.getSchedulerById(request.scheduler_id);
    if (!bookingContainerItem) {
      throw new ValidateException(ErrorCode.SCHEDULER_ID_IS_NULL, 'Scheduler ID is invalid');
    }

    const locationInfo = await this.tailwindApi.getLocationInfo(bookingContainerItem.containerNo);
    if (locationInfo && locationInfo.length > 0) {
      await this.saveLocation(locationInfo, bookingContainerItem.bookingNo);
    }

    const reeferInfo = await this.tailwindApi.getSensorInfo(bookingContainerItem.containerNo);
    if (reeferInfo && reeferInfo.length > 0) {
      await this.saveSensor(reeferInfo, bookingContainerItem.bookingNo);
    }
  }

  async saveLocation(locations: LocationInfo[], bookingNo: string) {
    const date = new Date();
    const etities = locations.map((location) => {
      const entity = new TrackerLocationEntity(location);
      entity.bookingNo = bookingNo;
      entity.receivedDate = date;
      return entity;
    });
    await this.trackerLocationRepo.saveRange(etities);
  }

  async saveSensor(sensors: SensorInfo[], bookingNo: string) {
    const date = new Date();
    const entities = sensors.map((sensor) => {
      const entity = new TrackerSensorEntity(sensor);
      entity.bookingNo = bookingNo;
      entity.receivedDate = date;
      return entity;
    });
    await this.trackerSensorRepo.saveRange(entities);
  }
}
