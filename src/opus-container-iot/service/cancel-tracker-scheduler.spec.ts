import { Test, TestingModule } from '@nestjs/testing';
import { SchedulerApiName } from 'src/shared/domain/api/scheduler.api.interface';
import { SchedulerEntity } from 'src/shared/domain/entity';
import { BookingContainerSchedulerRepositoryName } from 'src/shared/domain/repository/booking-container-scheduler.repository.interface';
import { DomainEventRequest } from '../dto/domain-event-request.dto';
import { CancelTrackerScheduleService } from './cancel-tracker-scheduler.service';

describe(`${CancelTrackerScheduleService.name}`, () => {
  let service: CancelTrackerScheduleService;
  const type = CancelTrackerScheduleService.prototype;
  const jestMockResolveData = { code: 1, message: 'Pass', data: {} };

  const mockCancelTrackerScheduleServiceExecute = jest.fn();
  const mockGetScheduler = jest.fn();
  const mockCancel = jest.fn();
  const SchedulerEntityResponse = new SchedulerEntity({
    id: 'string',
    type: 1,
    bookingNo: 'string',
    containerNo: 'string',
    schedulerId: 'string',
    isValid: true,
  });
  beforeEach(async () => {
    mockCancelTrackerScheduleServiceExecute.mockReset();
    mockCancelTrackerScheduleServiceExecute.mockResolvedValue(jestMockResolveData);

    mockGetScheduler.mockReset();
    mockGetScheduler.mockReturnValue(jestMockResolveData);

    mockCancel.mockReset();
    mockCancel.mockReturnValue(jestMockResolveData);
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CancelTrackerScheduleService],
      providers: [
        {
          provide: SchedulerApiName,
          useFactory: () => ({
            cancel: mockCancel,
          }),
        },
        {
          provide: BookingContainerSchedulerRepositoryName,
          useFactory: () => ({
            getScheduler: mockGetScheduler,
          }),
        },
      ],
    }).compile();

    service = module.get<CancelTrackerScheduleService>(CancelTrackerScheduleService);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  it(`${type.execute.name}_Should_ThrowValidateException_When_BookingNo_Is_Null`, async () => {
    const bookingNoIsNUll = new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: {
        booking_no: null,
        container_no: 'b123',
      },
      data: {
        booking_no: null,
        container_no: 'b123',
      },
      matrix_id: '1',
    });
    const result = service.execute(bookingNoIsNUll);

    await expect(result).rejects.toThrow('Payload identity invalid');
  });
  it(`${type.execute.name}_Should_ThrowValidateException_When_ContainerNo_Is_Null`, async () => {
    const containerNoIsNUll = new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: {
        booking_no: 'a123',
        container_no: null,
      },
      data: {
        booking_no: 'a123',
        container_no: null,
      },
      matrix_id: '1',
    });
    const result = service.execute(containerNoIsNUll);

    await expect(result).rejects.toThrow('Payload identity invalid');
  });

  it(`${type.execute.name}_should_throwValidateException_when_identity_Is_Null`, async () => {
    const containerNoIsNUll = new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: null,
      data: {
        booking_no: 'a123',
        container_no: null,
      },
      matrix_id: '1',
    });
    const result = service.execute(containerNoIsNUll);

    await expect(result).rejects.toThrow('Payload identity invalid');
  });

  it(`${type.execute.name}_should_beExecuted_successfully_when_allInformationIsCorrect`, async () => {
    mockGetScheduler.mockReturnValue(SchedulerEntityResponse);
    mockCancel.mockReturnValue(true);
    const domainEventRequest = new DomainEventRequest({
      code: 'E012',
      id: '1',
      source: 'container',
      type: '1',
      data_type: '1',
      action: '1',
      create_time: '1',
      identity: {
        booking_no: '123',
        container_no: 'b123',
      },
      data: {
        booking_no: '123',
        container_no: 'b123',
      },
      matrix_id: '1',
    });
    await service.execute(domainEventRequest);
    expect(mockGetScheduler.mock.calls).toHaveLength(1);
    expect(mockCancel.mock.calls).toHaveLength(1);
  });
});
