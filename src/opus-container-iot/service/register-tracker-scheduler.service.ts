import { ClientKafka } from '@nestjs/microservices';
import { KAFKA_CLIENT } from '../../utils/event';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { SchedulerApi, SchedulerApiName } from 'src/shared/domain/api/scheduler.api.interface';
import { SchedulerEntity } from 'src/shared/domain/entity';
import {
  BookingContainerSchedulerRepository,
  BookingContainerSchedulerRepositoryName,
} from 'src/shared/domain/repository/booking-container-scheduler.repository.interface';
import { Constants } from 'src/utils/constants';
import { DomainEventRequest } from '../dto/domain-event-request.dto';
import { SchedulerRegisterData } from '../dto/scheduler-register-data.dto';
import { SchedulerRegisterPayload } from '../dto/scheduler-register-payload.dto';
import { SchedulerRegisterRequest } from '../dto/scheduler-register.dto';
import { SchedulerRepeat } from '../dto/scheduler-repeat.dto';
import { EventType } from '../dto/event-type.dto';
import { ErrorCode } from 'src/utils/error.code';
import { ValidateException } from 'src/shared/domain/model/exception';

@Injectable()
export class RegisterTrackerScheduleService {
  constructor(
    @Inject(SchedulerApiName)
    private readonly schedulerApi: SchedulerApi,
    @Inject(BookingContainerSchedulerRepositoryName)
    private readonly schedulerRepo: BookingContainerSchedulerRepository,
    @Inject(KAFKA_CLIENT) private readonly client: ClientKafka,
  ) {}

  public async execute(request: DomainEventRequest): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
    if (!request.identity || !request.identity['booking_no'] || !request.identity['container_no']) {
      throw new ValidateException(ErrorCode.PAYLOAD_INVALID, 'Payload identity invalid');
    }
    const originData = request.identity;
    const schedulerId = await this.registerScheduler(request);
    await this.saveScheduler(originData['booking_no'], originData['container_no'], schedulerId);
  }

  private async registerScheduler(request: DomainEventRequest): Promise<string> {
    const originData = request.identity;
    const schedulerRegisterPayload = new SchedulerRegisterPayload({
      customer_no: 'undefined',
      booking_no: originData['booking_no'],
      container_no: originData['container_no'],
    });

    const schedulerRegisterData = new SchedulerRegisterData({
      topic: Constants.CONTAINER_IOT_TRIGGER,
      key: `${originData['booking_no']}_${originData['container_no']}}`,
      payload: schedulerRegisterPayload,
    });

    const schedulerRepeat = new SchedulerRepeat({
      every: process.env['SCHEDULE_REGISTER_EVERY'],
      until: process.env['SCHEDULE_REGISTER_UNTIL'],
    });

    const schedulerRegisterRequest = new SchedulerRegisterRequest({
      start_time: JSON.stringify(Date.now()),
      delay: process.env['SCHEDULE_REGISTER_DELAY'],
      repeat: schedulerRepeat,
      data: schedulerRegisterData,
    });

    const apiResponse = await this.schedulerApi.register(schedulerRegisterRequest);
    if (apiResponse.code === 200) {
      return apiResponse.data.scheduler_id;
    }
    this.emitEvent(schedulerRegisterRequest);
    return null;
  }

  private async saveScheduler(bookingNo: string, containerNo: string, schedulerId: string): Promise<void> {
    try {
      const schedule = new SchedulerEntity({
        bookingNo: bookingNo,
        containerNo: containerNo,
        schedulerId: schedulerId,
        isValid: true,
        type: EventType.REGULAR_MILESTONE,
      });
      await this.schedulerRepo.save(schedule);
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Save Schedule failed , Message: ${error.message} `, error.stack);
    }
  }

  private emitEvent(request: SchedulerRegisterRequest) {
    try {
      this.client.emit(request.data.topic, {
        key: request.data.key,
        value: new SchedulerRegisterRequest(request),
      });
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Process event error , Message: ${error.message} `, error.stack);
    }
  }
}
