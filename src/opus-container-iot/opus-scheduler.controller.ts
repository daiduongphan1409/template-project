import { ErrorCode } from 'src/utils/error.code';
import { ValidateException } from '../shared/domain/model/exception/validate.exception';
import { Controller, Inject, Logger } from '@nestjs/common';
import { ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import { ApiTags } from '@nestjs/swagger';
import { KAFKA_CLIENT } from 'src/utils/event';
import { Constants } from 'src/utils/constants';
import { BaseException } from 'src/shared/domain/model/exception';
import { RetrieveTrackerInfoService } from './service/retrieve-tracker-info.service';
import { RetrieveTrackerInfoRequest } from './dto/retrieve-tracker-info-request.dto';
@ApiTags('opus-iot-scheduler')
@Controller('opus-iot-scheduler')
export class OpusScheduleController {
  constructor(
    @Inject(RetrieveTrackerInfoService)
    private readonly retrieveTrackerInfoService: RetrieveTrackerInfoService,
    @Inject(KAFKA_CLIENT)
    private readonly client: ClientKafka,
  ) {}

  /* istanbul ignore next */
  async onModuleInit() {
    await this.client.connect();
  }

  @EventPattern(Constants.SCHEDULER)
  async opusCdcEventHandler(@Payload() request: RetrieveTrackerInfoRequest): Promise<void> {
    await this.callExecute(request);
  }

  private async callExecute(payloads: RetrieveTrackerInfoRequest) {
    if (!payloads) {
      throw new ValidateException(ErrorCode.PAYLOAD_IS_NULL, 'Payload is null');
    }
    try {
      Logger.log(`Receive event , Item: ${JSON.stringify(payloads.scheduler_id)}`);
      await this.retrieveTrackerInfoService.execute(payloads);
    } catch (ex) {
      if (ex instanceof BaseException) {
        Logger.warn(`Event invalid. scheduler_id: ${payloads.scheduler_id}, Code:${ex.code}, Message: ${ex.message}`);
      } else {
        const error = ex as Error;
        Logger.error(`Process event error. scheduler_id: ${payloads.scheduler_id}, Massage: ${error.message}`, error.stack);
      }
    }
  }
}
