import { ContainerDomainConfigRepository } from 'src/shared/domain/repository/container-domain-config.interface';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventMatrixFilter, EventMatrixLogic } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';

@Injectable()
export class ContainerConfigRepositoryImpl implements ContainerDomainConfigRepository {
  constructor(
    @InjectRepository(EventMatrixFilter, DBConnections.INTERNAL)
    private readonly repositoryMatrixFilter: Repository<EventMatrixFilter>,
    @InjectRepository(EventMatrixLogic, DBConnections.INTERNAL)
    private readonly repositoryMatrixLogic: Repository<EventMatrixLogic>,
  ) {}

  public async getAllMatrixFilter(): Promise<EventMatrixFilter[]> {
    return await this.repositoryMatrixFilter.find({
      where: {
        isValid: true,
      },
    });
  }

  public async getAllMatrixLogic(): Promise<EventMatrixLogic[]> {
    return await this.repositoryMatrixLogic.find({
      where: {
        isValid: true,
      },
    });
  }
}
