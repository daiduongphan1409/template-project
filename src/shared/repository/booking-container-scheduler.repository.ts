import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SchedulerEntity } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { BookingContainerSchedulerRepository } from '../domain/repository/booking-container-scheduler.repository.interface';

@Injectable()
export class BookingContainerSchedulerRepositoryImpl implements BookingContainerSchedulerRepository {
  constructor(
    @InjectRepository(SchedulerEntity, DBConnections.INTERNAL)
    private readonly repo: Repository<SchedulerEntity>,
  ) {}

  async save(scheduler: SchedulerEntity): Promise<SchedulerEntity> {
    await this.repo.save(scheduler);
    return scheduler;
  }

  async getScheduler(bookingId: string, containerId: string): Promise<SchedulerEntity> {
    return await this.repo.findOne({
      where: {
        bookingNo: bookingId,
        containerNo: containerId,
        isValid: true,
      },
    });
  }

  async getSchedulerById(schedulerId: string): Promise<SchedulerEntity> {
    return await this.repo.findOne({
      where: {
        schedulerId: schedulerId,
        isValid: true,
      },
    });
  }
}
