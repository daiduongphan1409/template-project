import { MockType } from './../../../test/repo-test-helper';
import { ConfigService } from '@nestjs/config';
import { TestingModule, Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SchedulerEntity } from 'src/shared/domain/entity';
import { BookingContainerSchedulerRepositoryImpl } from './booking-container-scheduler.repository';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { Repository } from 'typeorm';
import { RequestTimeoutException } from '@nestjs/common';

describe(`${BookingContainerSchedulerRepositoryImpl.name}`, () => {
  let repo: BookingContainerSchedulerRepositoryImpl;
  let repositoryMock: MockType<Repository<SchedulerEntity>>;
  const repoType = BookingContainerSchedulerRepositoryImpl.prototype;
  const jestMockResolveData = [];
  const SchedulerEntityResponse = new SchedulerEntity({
    id: 'string',
    type: 1,
    bookingNo: 'string',
    containerNo: 'string',
    schedulerId: 'string',
    isValid: true,
  });
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingContainerSchedulerRepositoryImpl,
        ConfigService,
        {
          provide: getRepositoryToken(SchedulerEntity, DBConnections.INTERNAL),
          useFactory: () => ({
            save: jest.fn().mockReturnValue(jestMockResolveData),
            findOne: jest.fn().mockReturnValue(SchedulerEntityResponse),
          }),
        },
      ],
    }).compile();
    repo = module.get<BookingContainerSchedulerRepositoryImpl>(BookingContainerSchedulerRepositoryImpl);
    repositoryMock = module.get(getRepositoryToken(SchedulerEntity, DBConnections.INTERNAL));
  });

  it(`${BookingContainerSchedulerRepositoryImpl.name}_should_success_when_toBeDeFined`, () => {
    expect(repo).toBeDefined();
  });

  it(`${repoType.save.name}_should_success_returnConfig_when_dataIsReady`, async () => {
    const payload = new SchedulerEntity({
      id: 'string',
      type: 1,
      bookingNo: 'string',
      containerNo: 'string',
      schedulerId: 'string',
      isValid: true,
    });
    repositoryMock.save.mockReturnValue(SchedulerEntityResponse);
    const response = await repo.save(payload);
    expect(response).toStrictEqual(SchedulerEntityResponse);
  });

  it(`${repoType.save.name}_should_throwException_when_repositorySaveTimeOut`, async () => {
    repositoryMock.save.mockImplementation(() => {
      throw new RequestTimeoutException('Execution time expired!');
    });
    return await expect(repo.save(null)).rejects.toThrow(RequestTimeoutException);
  });

  it(`${repoType.getScheduler.name}_should_success_returnConfig_when_dataIsReady`, async () => {
    repositoryMock.findOne.mockReturnValue(SchedulerEntityResponse);
    const response = await repo.getScheduler('string', 'string');
    expect(response).toStrictEqual(SchedulerEntityResponse);
  });

  it(`${repoType.getScheduler.name}_should_throwException_when_repositorySaveTimeOut`, async () => {
    repositoryMock.findOne.mockImplementation(() => {
      throw new RequestTimeoutException('Execution time expired!');
    });
    return await expect(repo.getScheduler('string', 'string')).rejects.toThrow(RequestTimeoutException);
  });

  it(`${repoType.getSchedulerById.name}_should_success_returnConfig_when_dataIsReady`, async () => {
    repositoryMock.findOne.mockReturnValue(SchedulerEntityResponse);
    const response = await repo.getSchedulerById('string');
    expect(response).toStrictEqual(SchedulerEntityResponse);
  });

  it(`${repoType.getSchedulerById.name}_should_throwException_when_repositorySaveTimeOut`, async () => {
    repositoryMock.findOne.mockImplementation(() => {
      throw new RequestTimeoutException('Execution time expired!');
    });
    return await expect(repo.getSchedulerById('string')).rejects.toThrow(RequestTimeoutException);
  });
});
