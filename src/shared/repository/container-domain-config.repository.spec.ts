import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ContainerConfigRepositoryImpl } from './container-domain-config.repository';
import { EventMatrixFilter } from '../domain/entity/event-matrix-filter.entity';
import { Trigger } from '../domain/model/enum/matrix.enum';
import { Repository } from 'typeorm';
import { EventMatrixLogic } from '../domain/entity';
import { ConfigService } from '@nestjs/config';
import { MockType, repositoryMockFactory } from '../../../test/repo-test-helper';
import { RequestTimeoutException } from '@nestjs/common';
import { DBConnections } from '../domain/model/enum/domain.enum';

const eventMatrixFilterResponse = [
  new EventMatrixFilter({
    id: 1,
    matrixId: '1234',
    triggerId: Trigger.ACTUAL,
    tableName: 'OPUSADM.BKG_BL_ISS',
    columnName: 'BKG_NO',
    isValid: true,
  }),
  new EventMatrixFilter({
    id: 2,
    matrixId: '3456',
    triggerId: Trigger.ESTIMATE,
    tableName: 'OPUSADM.BKG_BL_ISS',
    columnName: 'BKG_NO',
    isValid: true,
  }),
];

const eventMatrixLogicResponse = [
  new EventMatrixFilter({
    id: 1,
    matrixId: '1234',
    triggerId: Trigger.ACTUAL,
    tableName: 'OPUSADM.BKG_BL_ISS',
    columnName: 'BKG_NO',
    isValid: true,
  }),
  new EventMatrixFilter({
    id: 2,
    matrixId: '3456',
    triggerId: Trigger.ESTIMATE,
    tableName: 'OPUSADM.BKG_BL_ISS',
    columnName: 'BKG_NO',
    isValid: true,
  }),
];

describe(`${ContainerConfigRepositoryImpl.name}`, () => {
  let repo: ContainerConfigRepositoryImpl;
  let matrixFilterMockRepo: MockType<Repository<EventMatrixFilter>>;
  let matrixLogicMockRepo: MockType<Repository<EventMatrixLogic>>;
  const fileType = ContainerConfigRepositoryImpl.prototype;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ContainerConfigRepositoryImpl,
        ConfigService,
        {
          provide: getRepositoryToken(EventMatrixFilter, DBConnections.INTERNAL),
          useFactory: repositoryMockFactory,
        },
        {
          provide: getRepositoryToken(EventMatrixLogic, DBConnections.INTERNAL),
          useFactory: repositoryMockFactory,
        },
      ],
    }).compile();
    matrixFilterMockRepo = module.get(getRepositoryToken(EventMatrixFilter, DBConnections.INTERNAL));
    matrixLogicMockRepo = module.get(getRepositoryToken(EventMatrixLogic, DBConnections.INTERNAL));
    repo = module.get<ContainerConfigRepositoryImpl>(ContainerConfigRepositoryImpl);
  });

  it(`${ContainerConfigRepositoryImpl.name}_repository_should_be_defined`, () => {
    expect(repo).toBeDefined();
    expect(matrixFilterMockRepo).toBeDefined();
    expect(matrixLogicMockRepo).toBeDefined();
  });

  it(`${fileType.getAllMatrixFilter.name}_function_should_return_valid_data`, async () => {
    matrixFilterMockRepo.find.mockReturnValue(eventMatrixFilterResponse);
    const response = await repo.getAllMatrixFilter();
    expect(response).toBe(eventMatrixFilterResponse);
  });

  it(`${fileType.getAllMatrixFilter.name}_function_should_return_empty`, async () => {
    matrixFilterMockRepo.find.mockReturnValue([]);
    const response = await repo.getAllMatrixFilter();
    expect(response).toHaveLength(0);
  });

  it(`${fileType.getAllMatrixFilter.name}_function_should_return_undefined`, async () => {
    matrixFilterMockRepo.find.mockReturnValueOnce(undefined);
    const configReturn = await repo.getAllMatrixFilter();
    expect(configReturn).toBeUndefined();
  });

  it(`${fileType.getAllMatrixFilter.name}_function_should_have_timeout_exception`, async () => {
    matrixFilterMockRepo.find.mockImplementation(() => {
      throw new RequestTimeoutException('Execution time expired!');
    });
    return await expect(repo.getAllMatrixFilter()).rejects.toThrow(RequestTimeoutException);
  });

  it(`${fileType.getAllMatrixLogic.name}_function_should_return_valid_data`, async () => {
    matrixLogicMockRepo.find.mockReturnValue(eventMatrixLogicResponse);
    const response = await repo.getAllMatrixLogic();
    expect(response).toBe(eventMatrixLogicResponse);
  });

  it(`${fileType.getAllMatrixLogic.name}_function_should_return_empty`, async () => {
    matrixLogicMockRepo.find.mockReturnValue([]);
    const response = await repo.getAllMatrixLogic();
    expect(response).toHaveLength(0);
  });

  it(`${fileType.getAllMatrixLogic.name}_function_should_return_undefined`, async () => {
    matrixLogicMockRepo.find.mockReturnValueOnce(undefined);
    const configReturn = await repo.getAllMatrixLogic();
    expect(configReturn).toBeUndefined();
  });

  it(`${fileType.getAllMatrixLogic.name}_function_should_have_timeout_exception`, async () => {
    matrixLogicMockRepo.find.mockImplementation(() => {
      throw new RequestTimeoutException('Execution time expired!');
    });
    return await expect(repo.getAllMatrixLogic()).rejects.toThrow(RequestTimeoutException);
  });
});
