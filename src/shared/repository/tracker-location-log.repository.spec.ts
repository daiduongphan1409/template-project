import { TrackerLocationEntity } from './../domain/entity/tracker-location.entity';
import { TrackerLocationLogRepositoryImpl } from './tracker-location-log.repository';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ConfigService } from '@nestjs/config';

describe(`${TrackerLocationLogRepositoryImpl.name}`, () => {
  let repo: TrackerLocationLogRepositoryImpl;
  let repositoryMock;
  const repoType = TrackerLocationLogRepositoryImpl.prototype;
  const jestMockResolveData = [];
  const trackerLocationEntityResponse = new TrackerLocationEntity({
    id: 'string',
    bookingNo: 'string',
    containerNo: 'string',
    assetId: 'string',
    longitude: 1,
    latitude: 1,
    receivedDate: new Date('2022-11-18T09:06:50.850Z'),
    zoneId: 'string',
    heading: 1,
    altitude: 1,
  });
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TrackerLocationLogRepositoryImpl,
        ConfigService,
        {
          provide: getRepositoryToken(TrackerLocationEntity, DBConnections.INTERNAL),
          useFactory: () => ({
            save: jest.fn().mockReturnValue(jestMockResolveData),
            saveRange: jest.fn().mockReturnValue([jestMockResolveData]),
          }),
        },
      ],
    }).compile();
    repo = module.get<TrackerLocationLogRepositoryImpl>(TrackerLocationLogRepositoryImpl);
    repositoryMock = module.get(getRepositoryToken(TrackerLocationEntity, DBConnections.INTERNAL));
  });
  it(`${TrackerLocationLogRepositoryImpl.name}_Should_Success_ToBeDeFined`, () => {
    expect(repo).toBeDefined();
  });
  it(`${repoType.save.name}_Should_ReturnConfig_WhenDataIsReady`, async () => {
    const payload = new TrackerLocationEntity({
      id: 'string',
      bookingNo: 'string',
      containerNo: 'string',
      assetId: 'string',
      longitude: 1,
      latitude: 1,
      receivedDate: new Date('2022-11-18T09:06:50.850Z'),
      zoneId: 'string',
      heading: 1,
      altitude: 1,
    });
    repositoryMock.save.mockReturnValue(trackerLocationEntityResponse);
    const response = await repo.save(payload);
    expect(response).toStrictEqual(trackerLocationEntityResponse);
  });
});
