import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainerActivityDeadline } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ValidateException } from '../domain/model/exception';
import { BookingContainerActivityDeadlineRepositoryImpl } from './booking-container-activity-deadline.repository';

const activityData = new BookingContainerActivityDeadline({
  activityId: '6d517979-b234-4aab-9653-615f684bacce',
  deadlineDate: new Date('2022-11-01 11:00:00.000'),
});

const dataSave = new BookingContainerActivityDeadline({
  activityId: '6d517979-b234-4aab-9653-615f684bacce',
  deadlineDate: new Date('2022-11-01 11:00:00.000'),
});

let mockDeadlineRepository: DeepMocked<Repository<BookingContainerActivityDeadline>>;

describe(`${BookingContainerActivityDeadlineRepositoryImpl.name}`, () => {
  let target: BookingContainerActivityDeadlineRepositoryImpl;
  const fileType = BookingContainerActivityDeadlineRepositoryImpl.prototype;
  mockDeadlineRepository = createMock<Repository<BookingContainerActivityDeadline>>();

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingContainerActivityDeadlineRepositoryImpl,
        {
          provide: getRepositoryToken(BookingContainerActivityDeadline, DBConnections.INTERNAL),
          useValue: mockDeadlineRepository,
        },
      ],
    }).compile();

    target = module.get<BookingContainerActivityDeadlineRepositoryImpl>(BookingContainerActivityDeadlineRepositoryImpl);
  });

  it(`${fileType.findValidBy.name}_function_should_return_valid_data`, async () => {
    //Arrange
    mockDeadlineRepository.findOne.mockResolvedValue(activityData);
    //Act
    const temp = await target.findValidBy('');
    //Assert
    expect(temp).toMatchObject(activityData);
  });

  it(`${fileType.findValidBy.name}_function_should_return_null_when_exception`, async () => {
    //Arrange
    mockDeadlineRepository.findOne.mockImplementation(() => {
      throw new ValidateException(1, 'error');
    });
    //Act
    const temp = await target.findValidBy('');
    //Assert
    expect(temp).toBeNull();
  });

  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save(dataSave);
    //Assert
    expect(temp).toBe(true);
  });

  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(true);
  });

  it(`${fileType.save.name}_function_should_return_false_when_exception`, async () => {
    //Arrange
    mockDeadlineRepository.save.mockImplementation(() => {
      throw new ValidateException(1, 'error');
    });
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(false);
  });
});
