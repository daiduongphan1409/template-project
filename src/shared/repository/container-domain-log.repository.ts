import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ContainerDomainLog } from '../domain/entity/container-domain-log.entity';
import { ContainerDomainLogRepository } from '../domain/repository/container-domain-log.interface';
import { ContainerLogDocument } from '../mongo/schema/container-domain-log.schema';

@Injectable()
export class ContainerRepositoryImpl implements ContainerDomainLogRepository {
  constructor(
    @InjectModel(ContainerDomainLog.name)
    private readonly filterLogModel: Model<ContainerLogDocument>,
  ) {}

  async save(log: ContainerDomainLog): Promise<ContainerDomainLog> {
    log.updatedTime = new Date();
    const logModel = new this.filterLogModel(log);
    await logModel.save();
    return logModel;
  }
}
