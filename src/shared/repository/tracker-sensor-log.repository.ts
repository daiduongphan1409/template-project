import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TrackerSensorEntity } from '../domain/entity/tracker-sensor.entity';
import { DBConnections } from '../domain/model/enum/domain.enum';

import { TrackerSensorLogRepository } from '../domain/repository/tracker-sensor-log.repository.interface.ts';

@Injectable()
export class TrackerSensorLogRepositoryImpl implements TrackerSensorLogRepository {
  constructor(
    @InjectRepository(TrackerSensorEntity, DBConnections.INTERNAL)
    private readonly repo: Repository<TrackerSensorEntity>,
  ) {}

  async saveRange(data: TrackerSensorEntity[]): Promise<TrackerSensorEntity[]> {
    await this.repo.save(data);
    return data;
  }

  async save(data: TrackerSensorEntity): Promise<TrackerSensorEntity> {
    await this.repo.save(data);
    return data;
  }
}
