import { TrackerSensorEntity } from 'src/shared/domain/entity/tracker-sensor.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TrackerSensorLogRepositoryImpl } from './tracker-sensor-log.repository';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ConfigService } from '@nestjs/config';

describe(`${TrackerSensorLogRepositoryImpl.name}`, () => {
  let repo: TrackerSensorLogRepositoryImpl;
  let repositoryMock;
  const repoType = TrackerSensorLogRepositoryImpl.prototype;
  const jestMockResolveData = [];
  const TrackerSensorEntityResponse = new TrackerSensorEntity({
    id: 'string',
    bookingNo: 'string',
    containerNo: 'string',
    assetId: 'string',
    ambientTemperature: 1,
    o2: 1,
    returnAirTemperature: 1,
    supplyAirTemperature: 1,
    receivedDate: new Date('2022-11-18T09:06:50.850Z'),
    co2: 1,
    ethylene: 1,
    humidity: 1,
    nitrogen: 1,
    returnAir2Temperature: 1,
    supplyAir2Temperature: 1,
    airExchange: 1,
    co: 1,
  });
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TrackerSensorLogRepositoryImpl,
        ConfigService,
        {
          provide: getRepositoryToken(TrackerSensorEntity, DBConnections.INTERNAL),
          useFactory: () => ({
            save: jest.fn().mockReturnValue(jestMockResolveData),
            saveRange: jest.fn().mockReturnValue([jestMockResolveData]),
          }),
        },
      ],
    }).compile();
    repo = module.get<TrackerSensorLogRepositoryImpl>(TrackerSensorLogRepositoryImpl);
    repositoryMock = module.get(getRepositoryToken(TrackerSensorEntity, DBConnections.INTERNAL));
  });
  it(`${TrackerSensorLogRepositoryImpl.name}_Should_Success_ToBeDeFined`, () => {
    expect(repo).toBeDefined();
  });
  it(`${repoType.save.name}_Should_ReturnConfig_WhenDataIsReady`, async () => {
    const payload = new TrackerSensorEntity({
      id: 'string',
      bookingNo: 'string',
      containerNo: 'string',
      assetId: 'string',
      ambientTemperature: 1,
      o2: 1,
      returnAirTemperature: 1,
      supplyAirTemperature: 1,
      receivedDate: new Date('2022-11-18T09:06:50.850Z'),
      co2: 1,
      ethylene: 1,
      humidity: 1,
      nitrogen: 1,
      returnAir2Temperature: 1,
      supplyAir2Temperature: 1,
      airExchange: 1,
      co: 1,
    });
    repositoryMock.save.mockReturnValue(TrackerSensorEntityResponse);
    const response = await repo.save(payload);
    expect(response).toStrictEqual(TrackerSensorEntityResponse);
  });

  it(`${repoType.saveRange.name}_should_success_returnConfig_when_dataIsReady`, async () => {
    const trackerSensorEntity = new TrackerSensorEntity({
      id: 'string',
      bookingNo: 'string',
      containerNo: 'string',
      assetId: 'string',
      ambientTemperature: 1,
      o2: 1,
      returnAirTemperature: 1,
      supplyAirTemperature: 1,
      receivedDate: new Date('2022-11-18T09:06:50.850Z'),
      co2: 1,
      ethylene: 1,
      humidity: 1,
      nitrogen: 1,
      returnAir2Temperature: 1,
      supplyAir2Temperature: 1,
      airExchange: 1,
      co: 1,
    });
    repositoryMock.saveRange.mockReturnValue(TrackerSensorEntityResponse);
    const response = await repo.saveRange([trackerSensorEntity]);
    expect(response[0]).toStrictEqual(TrackerSensorEntityResponse);
  });
});
