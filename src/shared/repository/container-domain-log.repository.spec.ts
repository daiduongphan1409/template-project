import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { ContainerRepositoryImpl } from './container-domain-log.repository';
import { ContainerDomainLog } from '../domain/entity/container-domain-log.entity';
import { ContainerDomainLogState } from '../domain/model/enum/filter-log-state.enum';
import { EvaluationResult, EventMatrixDto } from 'src/opus-container-domain/dto';

const logModel = new ContainerDomainLog({
  id: 'container_log_1',
  eventId: 'log_event_1',
  matrixMatch: [new EvaluationResult()],
  matrixPotential: [new EventMatrixDto()],
  state: ContainerDomainLogState.NEW,
});
const fakeContainerDomainLog = jest.fn(() => ({
  id: 'container_log_1',
  save: jest.fn().mockReturnValue(true),
}));
const fakeContainerLogDocument = jest.fn();

describe(`${ContainerRepositoryImpl.name}`, () => {
  let repo: ContainerRepositoryImpl;
  const fileType = ContainerRepositoryImpl.prototype;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        ContainerRepositoryImpl,
        {
          provide: getModelToken('ContainerDomainLog'),
          useValue: fakeContainerDomainLog,
        },
        {
          provide: getModelToken('ContainerLogDocument'),
          useValue: fakeContainerLogDocument,
        },
      ],
    }).compile();
    repo = module.get<ContainerRepositoryImpl>(ContainerRepositoryImpl);
  });

  it(`${ContainerRepositoryImpl.name}_repository_should_be_defined`, () => {
    expect(repo).toBeDefined();
  });

  it(`${fileType.save.name}_function_should_be_successfully`, async () => {
    const returnSave = await repo.save(logModel);
    expect(returnSave.id).toBe(logModel.id);
  });
});
