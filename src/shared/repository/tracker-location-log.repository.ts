import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TrackerLocationEntity } from '../domain/entity/tracker-location.entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { TrackerLocationLogRepository } from '../domain/repository/tracker-location-log.repository.interface';

@Injectable()
export class TrackerLocationLogRepositoryImpl implements TrackerLocationLogRepository {
  constructor(
    @InjectRepository(TrackerLocationEntity, DBConnections.INTERNAL)
    private readonly repo: Repository<TrackerLocationEntity>,
  ) {}

  async saveRange(data: TrackerLocationEntity[]): Promise<TrackerLocationEntity[]> {
    return await this.repo.save(data);
  }

  async save(data: TrackerLocationEntity): Promise<TrackerLocationEntity> {
    return await this.repo.save(data);
  }
}
