import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainerActivity } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ValidateException } from '../domain/model/exception';
import { BookingContainerActivityRepositoryImpl } from './booking-container-activity.repository';
const activityData = new BookingContainerActivity({
  bookingNo: 'PKGU23502600',
  containerNo: 'TCLU8730600',
  copNo: 'CPKG8813684919',
  copSeq: 4052,
  opusCode: 'FUVMBD',
});
const dataSave = new BookingContainerActivity({
  bookingNo: 'PKGU23502600',
  containerNo: 'TCLU8730600',
  copNo: 'CPKG8813684919',
});
let mockActivityRepository: DeepMocked<Repository<BookingContainerActivity>>;
describe(`${BookingContainerActivityRepositoryImpl.name}`, () => {
  let target: BookingContainerActivityRepositoryImpl;
  const fileType = BookingContainerActivityRepositoryImpl.prototype;
  mockActivityRepository = createMock<Repository<BookingContainerActivity>>();
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingContainerActivityRepositoryImpl,
        {
          provide: getRepositoryToken(BookingContainerActivity, DBConnections.INTERNAL),
          useValue: mockActivityRepository,
        },
      ],
    }).compile();

    target = module.get<BookingContainerActivityRepositoryImpl>(BookingContainerActivityRepositoryImpl);
  });
  it(`${fileType.findValidBy.name}_function_should_return_valid_data`, async () => {
    //Arrange
    mockActivityRepository.findOne.mockResolvedValue(activityData);
    //Act
    const temp = await target.findValidBy('', '', '');
    //Assert
    expect(temp).toMatchObject(activityData);
  });
  it(`${fileType.findValidBy.name}_function_should_return_null_when_exception`, async () => {
    //Arrange
    mockActivityRepository.findOne.mockImplementation(() => {
      throw new ValidateException(1, 'error');
    });
    //Act
    const temp = await target.findValidBy('', '', '');
    //Assert
    expect(temp).toBeNull();
  });
  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save(dataSave);
    //Assert
    expect(temp).toBe(true);
  });
  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(true);
  });
  it(`${fileType.save.name}_function_should_return_false_when_exception`, async () => {
    //Arrange
    mockActivityRepository.save.mockImplementation(() => {
      throw new ValidateException(1, 'error');
    });
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(false);
  });
});
