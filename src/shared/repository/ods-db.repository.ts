import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { BookingClzTime, SupplyChainManagementCopDetail, SupplyChainManagementCopHeader } from '../domain/entity-ods';
import { BkgContainer } from '../domain/entity-ods/bkg-container.entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ODSRepository } from '../domain/repository/ods-db-repository.interface';

@Injectable()
export class ODSRepositoryImpl implements ODSRepository {
  constructor(
    @InjectRepository(BkgContainer, DBConnections.ODS)
    private readonly repositoryBkgContainer: Repository<BkgContainer>,

    @InjectRepository(BookingClzTime, DBConnections.ODS)
    private readonly repositoryBkgClzTm: Repository<BookingClzTime>,

    @InjectRepository(SupplyChainManagementCopDetail, DBConnections.ODS)
    private readonly repositorySceCopDtl: Repository<SupplyChainManagementCopDetail>,

    @InjectRepository(SupplyChainManagementCopHeader, DBConnections.ODS)
    private readonly repositorySceCopHdr: Repository<SupplyChainManagementCopHeader>,
  ) {}

  async getListBookingNumber(bookingNumber: string): Promise<BkgContainer[]> {
    return await this.repositoryBkgContainer.find({
      where: {
        BKG_NO: bookingNumber,
      },
      select: {
        CNTR_NO: true,
      },
    });
  }

  async getSceCopHdrByBooking(bookingNumber: string): Promise<SupplyChainManagementCopHeader[]> {
    return await this.repositorySceCopHdr.find({
      where: {
        BKG_NO: bookingNumber,
      },
    });
  }

  async getBookingContainer(bookingNumber: string, containerNumber: string): Promise<BkgContainer> {
    return await this.repositoryBkgContainer.findOne({
      where: {
        BKG_NO: bookingNumber,
        CNTR_NO: containerNumber,
      },
    });
  }

  async getSceCopHdrByBookingAndContainer(bookingNumber: string, containerNumber: string): Promise<SupplyChainManagementCopHeader> {
    return await this.repositorySceCopHdr.findOne({
      where: {
        BKG_NO: bookingNumber,
        CNTR_NO: containerNumber,
      },
    });
  }

  async getSceCopDtlsByBookingAndContainer(bookingNumber: string, containerNumber: string): Promise<SupplyChainManagementCopDetail[]> {
    return await this.repositorySceCopDtl.find({
      where: {
        header: {
          BKG_NO: bookingNumber,
          CNTR_NO: containerNumber,
        },
      },
    });
  }

  async getSceCopDtlsByBooking(bookingNumber: string): Promise<SupplyChainManagementCopDetail[]> {
    return await this.repositorySceCopDtl.find({
      where: {
        header: {
          BKG_NO: bookingNumber,
        },
      },
    });
  }

  async getBookingCutoffTimeList(bkgNo: string): Promise<BookingClzTime[]> {
    return await this.repositoryBkgClzTm.find({
      where: {
        BKG_NO: bkgNo,
      },
    });
  }

  async getSceCopDtls(copNo: string): Promise<SupplyChainManagementCopDetail[]> {
    return await this.repositorySceCopDtl.find({
      where: {
        COP_NO: copNo,
      },
    });
  }

  public async getCopDltByBkgClzTm(bkgNo: string, clzYdCd: string, actCd: string[]): Promise<SupplyChainManagementCopDetail[]> {
    return await this.repositorySceCopDtl.find({
      relations: {
        header: true,
      },
      relationLoadStrategy: 'join',
      where: {
        header: {
          BKG_NO: bkgNo,
          COP_STS_CD: Not(In(['F', 'X'])),
        },
        NOD_CD: clzYdCd,
        ACT_CD: In(actCd),
      },
    });
  }

  async getSceCopDtl(copNo: string, copDtlSeq: string): Promise<SupplyChainManagementCopDetail> {
    return await this.repositorySceCopDtl.findOne({
      where: {
        COP_NO: copNo,
        COP_DTL_SEQ: copDtlSeq,
      },
    });
  }

  async getSceCopHdr(copNo: string): Promise<SupplyChainManagementCopHeader> {
    return await this.repositorySceCopHdr.findOne({
      where: {
        COP_NO: copNo,
      },
    });
  }

  async getBkgClzTm(bkgNo: string, clzTypeCode: string): Promise<BookingClzTime> {
    return await this.repositoryBkgClzTm.findOne({
      where: {
        BKG_NO: bkgNo,
        CLZ_TP_CD: clzTypeCode,
      },
    });
  }
}
