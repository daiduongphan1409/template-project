/* eslint-disable no-await-in-loop */
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { BookingContainer } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { BookingContainerRepository } from '../domain/repository/booking-container.interface';

@Injectable()
export class BookingContainerRepositoryImpl implements BookingContainerRepository {
  private readonly logger = new Logger(BookingContainerRepositoryImpl.name);
  constructor(
    @InjectRepository(BookingContainer, DBConnections.INTERNAL)
    private readonly bookingContainerRepository: Repository<BookingContainer>,
  ) {}

  async isExist(bookingNumber: string, containerNumber: string): Promise<boolean> {
    return (
      (await this.bookingContainerRepository.countBy({
        bookingNo: bookingNumber,
        containerNo: containerNumber,
      })) > 0
    );
  }

  public async save(bookingContainer: BookingContainer | BookingContainer[]): Promise<boolean> {
    try {
      if (!Array.isArray(bookingContainer)) {
        bookingContainer = [bookingContainer];
      }
      await this.bookingContainerRepository.save(bookingContainer);
      return true;
    } catch (error) {
      const err = error as Error;
      this.logger.error(`BookingContainer save data error. Message: ${err.message}, Input: ${JSON.stringify(bookingContainer)}`, err.stack);
    }
    return false;
  }

  public async getEvent(bkgNo: string, cntrNo: string, copNo: string, matrixIds: string[] = []): Promise<BookingContainer> {
    try {
      return await this.bookingContainerRepository.findOne({
        relations: {
          activities: {
            activitydeadLines: true,
            activityDeadLineHistories: false,
          },
        },
        relationLoadStrategy: 'join',
        where: {
          activities: {
            eventMatrixId: In(matrixIds),
          },
          bookingNo: bkgNo,
          copNo: copNo,
        },
      });
    } catch (error) {
      const err = error as Error;
      this.logger.error(`BookingContainer getEvent data error. Message: ${err.message}, Input: ${bkgNo}`, err.stack);
    }
    return null;
  }
}
