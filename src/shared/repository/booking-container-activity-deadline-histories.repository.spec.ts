import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainerActivityDeadlineHistories } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ValidateException } from '../domain/model/exception';
import { BookingContainerActivityDeadlineHistoriesRepositoryImpl } from './booking-container-activity-deadline-histories.repository';

const dataSave = new BookingContainerActivityDeadlineHistories({
  activityId: '6d517979-b234-4aab-9653-615f684bacce',
  deadlineDate: new Date('2022-11-01 11:00:00.000'),
});

let mockDeadlineRepository: DeepMocked<Repository<BookingContainerActivityDeadlineHistories>>;

describe(`${BookingContainerActivityDeadlineHistoriesRepositoryImpl.name}`, () => {
  let target: BookingContainerActivityDeadlineHistoriesRepositoryImpl;
  const fileType = BookingContainerActivityDeadlineHistoriesRepositoryImpl.prototype;
  mockDeadlineRepository = createMock<Repository<BookingContainerActivityDeadlineHistories>>();

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingContainerActivityDeadlineHistoriesRepositoryImpl,
        {
          provide: getRepositoryToken(BookingContainerActivityDeadlineHistories, DBConnections.INTERNAL),
          useValue: mockDeadlineRepository,
        },
      ],
    }).compile();

    target = module.get<BookingContainerActivityDeadlineHistoriesRepositoryImpl>(BookingContainerActivityDeadlineHistoriesRepositoryImpl);
  });

  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save(dataSave);
    //Assert
    expect(temp).toBe(true);
  });

  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(true);
  });

  it(`${fileType.save.name}_function_should_return_false_when_exception`, async () => {
    //Arrange
    mockDeadlineRepository.save.mockImplementation(() => {
      throw new ValidateException(1, 'error');
    });
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(false);
  });
});
