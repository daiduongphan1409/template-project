import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainerActivityDeadline } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { BookingContainerActivityDeadlineRepository } from '../domain/repository/booking-container-activity-deadline.interface';

@Injectable()
export class BookingContainerActivityDeadlineRepositoryImpl implements BookingContainerActivityDeadlineRepository {
  private readonly logger = new Logger(BookingContainerActivityDeadlineRepositoryImpl.name);

  constructor(
    @InjectRepository(BookingContainerActivityDeadline, DBConnections.INTERNAL)
    private readonly repository: Repository<BookingContainerActivityDeadline>,
  ) {}

  public async save(deadline: BookingContainerActivityDeadline | BookingContainerActivityDeadline[]): Promise<boolean> {
    try {
      if (!Array.isArray(deadline)) {
        deadline = [deadline];
      }
      await this.repository.save(deadline);
      return true;
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(
        `Save booking container activity deadline error. Activity: ${JSON.stringify(deadline)}, Message: ${error.message}`,
        error.stack,
      );
    }

    return false;
  }

  public async findValidBy(activityId: string): Promise<BookingContainerActivityDeadline> {
    try {
      return this.repository.findOne({
        where: {
          activityId: activityId,
        },
      });
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(`Find booking container activity deadline error. ActivtyId: ${activityId}, Message: ${error.message}`, error.stack);
    }

    return null;
  }
}
