import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainerActivityDeadlineHistories } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { BookingContainerActivityDeadlineHistoriesRepository } from '../domain/repository/booking-container-activity-deadline-histories.interface';

@Injectable()
export class BookingContainerActivityDeadlineHistoriesRepositoryImpl implements BookingContainerActivityDeadlineHistoriesRepository {
  private readonly logger = new Logger(BookingContainerActivityDeadlineHistoriesRepositoryImpl.name);

  constructor(
    @InjectRepository(BookingContainerActivityDeadlineHistories, DBConnections.INTERNAL)
    private readonly repository: Repository<BookingContainerActivityDeadlineHistories>,
  ) {}

  public async save(history: BookingContainerActivityDeadlineHistories | BookingContainerActivityDeadlineHistories[]): Promise<boolean> {
    try {
      if (!Array.isArray(history)) {
        history = [history];
      }
      await this.repository.save(history);
      return true;
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(
        `Save booking container activity deadline error. Activity: ${JSON.stringify(history)}, Message: ${error.message}`,
        error.stack,
      );
    }

    return false;
  }
}
