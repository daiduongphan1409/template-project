import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainerActivity } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { BookingContainerActivityRepository } from '../domain/repository/booking-container-activity.interface';

@Injectable()
export class BookingContainerActivityRepositoryImpl implements BookingContainerActivityRepository {
  private readonly logger = new Logger(BookingContainerActivityRepositoryImpl.name);
  constructor(
    @InjectRepository(BookingContainerActivity, DBConnections.INTERNAL)
    private readonly repository: Repository<BookingContainerActivity>,
  ) {}

  public async save(activity: BookingContainerActivity | BookingContainerActivity[]): Promise<boolean> {
    try {
      if (!Array.isArray(activity)) {
        activity = [activity];
      }
      await this.repository.save(activity);
      return true;
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(`Save booking container activity error. Activity: ${JSON.stringify(activity)}, Message: ${error.message}`, error.stack);
    }

    return false;
  }

  public async findValidBy(bookingNumber: string, containerNumber: string, matrixId: string): Promise<BookingContainerActivity> {
    try {
      return this.repository.findOne({
        where: {
          bookingNo: bookingNumber,
          containerNo: containerNumber,
          eventMatrixId: matrixId,
          isValid: 1,
        },
      });
    } catch (ex) {
      const error = ex as Error;
      this.logger.error(
        `Find booking container activity error. BookingNumber: ${bookingNumber}, ContainerNumber: ${containerNumber}, MatrixId: ${matrixId}, Message: ${error.message}`,
        error.stack,
      );
    }

    return null;
  }
}
