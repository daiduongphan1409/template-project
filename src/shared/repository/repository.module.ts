import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventMatrixEntity } from '../postgres/schema/event-matrix.schema';
import { PostgresModule } from '../postgres/postgres.module';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoDBModule } from '../mongo/mongo.module';
import { ContainerDomainLogRepositoryName } from '../domain/repository/container-domain-log.interface';
import { ContainerConfigRepositoryImpl } from './container-domain-config.repository';
import { ContainerDomainConfigRepositoryName } from '../domain/repository/container-domain-config.interface';
import { ContainerRepositoryImpl } from './container-domain-log.repository';
import {
  BookingContainerActivityDeadline,
  BookingContainerActivityEstimate,
  ContainerDomainLog,
  EventMatrixFilter,
  EventMatrixLogic,
  BookingContainerActivity,
  BookingContainer,
  TrackerLocationEntity,
  TrackerSensorEntity,
  SchedulerEntity,
} from '../domain/entity';
import { ContainerDomainLogSchema } from '../mongo/schema/container-domain-log.schema';
import { EventMatrixFilterEntity } from '../postgres/schema/event-matrix-filter.schema';
import { EventMatrixLogicEntity } from '../postgres/schema/event-matrix-logic.schema';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { BookingClzTimerEntity } from '../postgres/ods-schema/bkg-clz-tm.schema';
import { odsRepositoryName } from '../domain/repository/ods-db-repository.interface';
import { ODSRepositoryImpl } from './ods-db.repository';
import { BookingContainerActivityDeadlineEntity } from '../postgres/schema/booking-container-activity-deadline.schema';
import { ContainerDomainEventEstimateRepositoryName } from '../domain/repository/container-domain-event-estimate.interface';
import { SupplyChainManagementCopDtlEntity } from '../postgres/ods-schema/sce-cop-dtl.schema';
import { BookingContainerActivityEntity } from '../postgres/schema/booking-container-activity.schema';
import { BookingContainerEntity } from '../postgres/schema/booking-container.schema';
import { BookingContainerActivityDeadlineHistories } from '../domain/entity/booking-container-deadline-history.entity';
import { BookingContainerRepositoryName } from '../domain/repository/booking-container.interface';
import { BookingContainerActivityEstimateHistories } from '../domain/entity/booking-container-estimate-history.entity';
import { BookingContainerActivityDeadlineHistoriesEntity } from '../postgres/schema/booking-container-activity-deadline-histories.schema';
import { BookingContainerActivityEstimateEntity } from '../postgres/schema/booking-container-activity-estimate.schema';
import { BookingContainerActivityEstimateHistoriesEntity } from '../postgres/schema/booking-container-activity-estimate-histories.schema';
import { SupplyChainManagementCopHdrEntity } from '../postgres/ods-schema/sce-cop-hdr.schema';
import { BookingContainerRepositoryImpl } from './booking-container.repository';
import { ContainerEventRepositoryEstimateImpl } from './container-domain-event-estimate.repository';
import { BookingContainerSensorLogEntity } from '../postgres/schema/booking-container-sensor-log.schema';
import { BookingContainerLocationLogEntity } from '../postgres/schema/booking-container-location-log.schema';
import { BookingContainerSchedulerEntity } from '../postgres/schema/booking-container-scheduler.schema';
import { TrackerSensorLogRepositoryName } from '../domain/repository/tracker-sensor-log.repository.interface.ts';
import { TrackerSensorLogRepositoryImpl } from './tracker-sensor-log.repository';
import { TrackerLocationLogRepositoryName } from '../domain/repository/tracker-location-log.repository.interface';
import { TrackerLocationLogRepositoryImpl } from './tracker-location-log.repository';
import { BookingContainerSchedulerRepositoryName } from '../domain/repository/booking-container-scheduler.repository.interface';
import { BookingContainerSchedulerRepositoryImpl } from './booking-container-scheduler.repository';
import { BookingContainerActivityRepositoryImpl } from './booking-container-activity.repository';
import { BookingContainerActivityRepositoryName } from '../domain/repository/booking-container-activity.interface';
import { BookingContainerActivityDeadlineRepositoryName } from '../domain/repository/booking-container-activity-deadline.interface';
import { BookingContainerActivityDeadlineRepositoryImpl } from './booking-container-activity-deadline.repository';
import { BkgContainerEntity } from '../postgres/ods-schema/bkg-container.schema';
import { BookingContainerActivityDeadlineHistoriesRepositoryName } from '../domain/repository/booking-container-activity-deadline-histories.interface';
import { BookingContainerActivityDeadlineHistoriesRepositoryImpl } from './booking-container-activity-deadline-histories.repository';

@Global()
@Module({
  imports: [
    PostgresModule,
    MongoDBModule,
    TypeOrmModule.forFeature(
      [
        BookingContainer,
        BookingContainerActivity,
        BookingContainerActivityDeadline,
        BookingContainerActivityDeadlineHistories,
        EventMatrixFilter,
        EventMatrixLogic,
        BookingContainerEntity,
        BookingContainerActivityEntity,
        BookingContainerActivityDeadlineEntity,
        BookingContainerActivityDeadlineHistoriesEntity,
        EventMatrixEntity,
        EventMatrixFilterEntity,
        EventMatrixLogicEntity,
        BookingContainerActivityEstimate,
        BookingContainerActivityEstimateEntity,
        BookingContainerActivity,
        BookingContainerActivityEntity,
        BookingContainer,
        BookingContainerEntity,
        BookingContainerActivityEstimateHistoriesEntity,
        BookingContainerActivityEstimateHistories,
        TrackerLocationEntity,
        TrackerSensorEntity,
        SchedulerEntity,
        BookingContainerSensorLogEntity,
        BookingContainerLocationLogEntity,
        BookingContainerSchedulerEntity,
      ],
      DBConnections.INTERNAL,
    ),
    TypeOrmModule.forFeature(
      [BkgContainerEntity, BookingClzTimerEntity, SupplyChainManagementCopDtlEntity, SupplyChainManagementCopHdrEntity],
      DBConnections.ODS,
    ),
    MongooseModule.forFeature([{ name: ContainerDomainLog.name, schema: ContainerDomainLogSchema }]),
  ],
  providers: [
    {
      provide: BookingContainerRepositoryName,
      useClass: BookingContainerRepositoryImpl,
    },
    {
      provide: ContainerDomainConfigRepositoryName,
      useClass: ContainerConfigRepositoryImpl,
    },
    {
      provide: ContainerDomainLogRepositoryName,
      useClass: ContainerRepositoryImpl,
    },
    {
      provide: odsRepositoryName,
      useClass: ODSRepositoryImpl,
    },
    {
      provide: ContainerDomainEventEstimateRepositoryName,
      useClass: ContainerEventRepositoryEstimateImpl,
    },
    {
      provide: BookingContainerActivityRepositoryName,
      useClass: BookingContainerActivityRepositoryImpl,
    },
    {
      provide: BookingContainerActivityDeadlineRepositoryName,
      useClass: BookingContainerActivityDeadlineRepositoryImpl,
    },
    {
      provide: BookingContainerActivityDeadlineHistoriesRepositoryName,
      useClass: BookingContainerActivityDeadlineHistoriesRepositoryImpl,
    },
    {
      provide: TrackerSensorLogRepositoryName,
      useClass: TrackerSensorLogRepositoryImpl,
    },
    {
      provide: TrackerLocationLogRepositoryName,
      useClass: TrackerLocationLogRepositoryImpl,
    },
    {
      provide: BookingContainerSchedulerRepositoryName,
      useClass: BookingContainerSchedulerRepositoryImpl,
    },
  ],
  exports: [
    ContainerDomainLogRepositoryName,
    ContainerDomainConfigRepositoryName,
    ContainerDomainEventEstimateRepositoryName,
    BookingContainerActivityRepositoryName,
    BookingContainerActivityDeadlineRepositoryName,
    BookingContainerRepositoryName,
    odsRepositoryName,
    TrackerLocationLogRepositoryName,
    TrackerSensorLogRepositoryName,
    BookingContainerSchedulerRepositoryName,
    BookingContainerActivityDeadlineHistoriesRepositoryName,
  ],
})
export class RepositoryModule {}
