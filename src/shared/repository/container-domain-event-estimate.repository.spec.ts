import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { BookingContainer, BookingContainerActivity, BookingContainerActivityEstimate } from '../domain/entity';
import { ConfigService } from '@nestjs/config';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ContainerEventRepositoryEstimateImpl } from './container-domain-event-estimate.repository';
import { BookingContainerRepositoryImpl } from './booking-container.repository';
import { BookingContainerActivityEstimateHistories } from '../domain/entity/booking-container-estimate-history.entity';
import { randomUUID } from 'crypto';

describe(`${BookingContainerRepositoryImpl.name}`, () => {
  let repo: ContainerEventRepositoryEstimateImpl;
  const type = ContainerEventRepositoryEstimateImpl.prototype;
  const bookingContainerMock = jest.fn();
  const bookingContainerActivityMock = jest.fn();
  const bookingContainerActivityEstimateMock = jest.fn();
  const bookingContainerActivityEstimateHistories = jest.fn();
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ContainerEventRepositoryEstimateImpl,
        ConfigService,
        {
          provide: getRepositoryToken(BookingContainer, DBConnections.INTERNAL),
          useFactory: () => ({
            findOne: bookingContainerMock,
            save: jest.fn().mockReturnValue(true),
          }),
        },
        {
          provide: getRepositoryToken(BookingContainerActivity, DBConnections.INTERNAL),
          useFactory: () => ({
            findOne: bookingContainerActivityMock,
            save: jest.fn().mockReturnValue(true),
          }),
        },
        {
          provide: getRepositoryToken(BookingContainerActivityEstimate, DBConnections.INTERNAL),
          useFactory: () => ({
            findOne: bookingContainerActivityEstimateMock,
            save: jest.fn().mockReturnValue(true),
          }),
        },
        {
          provide: getRepositoryToken(BookingContainerActivityEstimateHistories, DBConnections.INTERNAL),
          useFactory: () => ({
            findOne: bookingContainerActivityEstimateHistories,
            save: jest.fn().mockReturnValue(true),
          }),
        },
      ],
    }).compile();

    repo = module.get<ContainerEventRepositoryEstimateImpl>(ContainerEventRepositoryEstimateImpl);
  });

  it(`${type.saveBookingContainer.name}_should_return_true_when_data_insrert_is_correct`, async () => {
    //Arrange
    const data = new BookingContainer({
      bookingNo: 'fkdhj',
      containerNo: 'kfjdkfd',
      copNo: 'kfjkdf',
      activities: [
        new BookingContainerActivity({
          id: randomUUID(),
          bookingNo: 'kfjdk',
          containerNo: 'fkjdfkd',
          copSeq: 5,
          opusCode: 'fjdffd',
          activityEstimates: [
            new BookingContainerActivityEstimate({
              id: randomUUID(),
              estimateDate: new Date(),
            }),
          ],
          activityEstimateHistories: [
            new bookingContainerActivityEstimateHistories({
              id: randomUUID(),
              estimateDate: new Date(),
            }),
          ],
        }),
      ],
    });
    //Act
    const temp = await repo.saveBookingContainer(data);
    //Assert
    expect(temp).toBe(true);
  });

  it(`${type.saveBookingContainer.name}_should_return_true_when_data_update_is_correct`, async () => {
    //Arrange
    const data = new BookingContainer({
      bookingNo: 'fkdhj',
      containerNo: 'kfjdkfd',
      copNo: 'kfjkdf',
      activities: [
        new BookingContainerActivity({
          id: randomUUID(),
          bookingNo: 'kfjdk',
          containerNo: 'fkjdfkd',
          copSeq: 5,
          opusCode: 'fjdffd',
          activityEstimates: [
            new BookingContainerActivityEstimate({
              id: randomUUID(),
              estimateDate: new Date(),
            }),
          ],
          activityEstimateHistories: [
            new bookingContainerActivityEstimateHistories({
              id: randomUUID(),
              estimateDate: new Date(),
            }),
          ],
        }),
      ],
    });
    const bookingContainer = new BookingContainer({
      bookingNo: 'fkdhj',
      containerNo: 'kfjdkfd',
      copNo: 'kfjkdf',
    });
    const bookingContainerActivity = new BookingContainerActivity({
      bookingNo: 'kfjdk',
      containerNo: 'fkjdfkd',
      copSeq: 5,
      opusCode: 'fjdffd',
    });
    const bookingContainerActivityEstimate = new BookingContainerActivityEstimate({
      estimateDate: new Date(),
    });
    bookingContainerMock.mockReturnValue(bookingContainer);
    bookingContainerActivityMock.mockReturnValue(bookingContainerActivity);
    bookingContainerActivityEstimateMock.mockReturnValue(bookingContainerActivityEstimate);

    //Act
    const temp = await repo.saveBookingContainer(data);

    //Assert
    expect(temp).toBe(true);
  });

  it(`${type.saveBookingContainer.name}_should_return_true_when_data_insrert_is_not_correct`, async () => {
    //Arrange
    const data = new BookingContainer({
      bookingNo: 'fkdhj',
      containerNo: 'kfjdkfd',
      copNo: 'kfjkdf',
      activities: [
        new BookingContainerActivity({
          bookingNo: 'kfjdk',
          id: randomUUID(),
          containerNo: 'fkjdfkd',
          copSeq: 5,
          opusCode: 'fjdffd',
          activityEstimateHistories: [
            new bookingContainerActivityEstimateHistories({
              id: randomUUID(),
              estimateDate: new Date(),
            }),
          ],
        }),
      ],
    });

    //Act
    const temp = await repo.saveBookingContainer(data);

    //Assert
    expect(temp).toBe(false);
  });
  it(`${type.saveBookingContainer.name}_should_return_true_when_data_update_is_not_correct`, async () => {
    //Arrange
    const data = new BookingContainer({
      bookingNo: 'fkdhj',
      containerNo: 'kfjdkfd',
      copNo: 'kfjkdf',
      activities: [
        new BookingContainerActivity({
          bookingNo: 'kfjdk',
          containerNo: 'fkjdfkd',
          copSeq: 5,
          opusCode: 'fjdffd',
          activityEstimateHistories: [
            new bookingContainerActivityEstimateHistories({
              id: randomUUID(),
              estimateDate: new Date(),
            }),
          ],
        }),
      ],
    });
    const bookingContainer = new BookingContainer({
      bookingNo: 'fkdhj',
      containerNo: 'kfjdkfd',
      copNo: 'kfjkdf',
    });
    const bookingContainerActivity = new BookingContainerActivity({
      bookingNo: 'kfjdk',
      containerNo: 'fkjdfkd',
      copSeq: 5,
      opusCode: 'fjdffd',
    });
    const bookingContainerActivityEstimate = new BookingContainerActivityEstimate({
      estimateDate: new Date(),
    });
    bookingContainerMock.mockReturnValue(bookingContainer);
    bookingContainerActivityMock.mockReturnValue(bookingContainerActivity);
    bookingContainerActivityEstimateMock.mockReturnValue(bookingContainerActivityEstimate);

    //Act
    const temp = await repo.saveBookingContainer(data);

    //Assert
    expect(temp).toBe(false);
  });

  it(`${type.saveBookingContainer.name}_Should_ThrowException_When_DatabaseError`, async () => {
    //Arrange
    bookingContainerMock.mockImplementation(() => {
      throw new TypeError();
    });
    return await expect(repo.saveBookingContainer).rejects.toThrow(TypeError);
  });
});
