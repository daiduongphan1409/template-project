import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { BookingContainer } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ODSRepositoryImpl } from './ods-db.repository';
import { BookingClzTime, SupplyChainManagementCopDetail, SupplyChainManagementCopHeader } from '../domain/entity-ods';
import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { BkgContainer } from '../domain/entity-ods/bkg-container.entity';
import { Repository } from 'typeorm';

const eventData = new BookingContainer({
  bookingNo: '',
  containerNo: '',
  activities: [],
});

describe(`${ODSRepositoryImpl.name}`, () => {
  let target: ODSRepositoryImpl;
  let mockBookingContainerRepository: DeepMocked<Repository<BkgContainer>>;

  const fileType = ODSRepositoryImpl.prototype;

  beforeEach(async () => {
    mockBookingContainerRepository = createMock<Repository<BkgContainer>>();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ODSRepositoryImpl,
        {
          provide: getRepositoryToken(BookingClzTime, DBConnections.ODS),
          useFactory: () => ({
            findOne: jest.fn().mockReturnValue(eventData),
          }),
        },
        {
          provide: getRepositoryToken(SupplyChainManagementCopDetail, DBConnections.ODS),
          useFactory: () => ({
            findOne: jest.fn().mockReturnValue(eventData),
            find: jest.fn().mockReturnValue(eventData),
          }),
        },
        {
          provide: getRepositoryToken(SupplyChainManagementCopHeader, DBConnections.ODS),
          useFactory: () => ({
            findOne: jest.fn().mockReturnValue(eventData),
          }),
        },
        {
          provide: getRepositoryToken(BkgContainer, DBConnections.ODS),
          useValue: mockBookingContainerRepository,
        },
      ],
    }).compile();

    target = module.get<ODSRepositoryImpl>(ODSRepositoryImpl);
  });

  it(`${fileType.getSceCopDtls.name}_function_should_return_valid_data`, async () => {
    //Arrange

    //Act
    const temp = await target.getSceCopDtls('');

    //Assert
    expect(temp).toMatchObject(eventData);
  });

  it(`${fileType.getSceCopDtl.name}_function_should_return_valid_data`, async () => {
    //Arrange

    //Act
    const temp = await target.getSceCopDtl('', '');

    //Assert
    expect(temp).toMatchObject(eventData);
  });

  it(`${fileType.getSceCopHdr.name}_function_should_return_valid_data`, async () => {
    //Arrange

    //Act
    const temp = await target.getSceCopHdr('');

    //Assert
    expect(temp).toMatchObject(eventData);
  });

  it(`${fileType.getCopDltByBkgClzTm.name}_function_should_return_valid_data`, async () => {
    //Arrange

    //Act
    const temp = await target.getCopDltByBkgClzTm('', '', []);

    //Assert
    expect(temp).toMatchObject(eventData);
  });
});
