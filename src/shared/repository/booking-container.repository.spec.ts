import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainer } from '../domain/entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ValidateException } from '../domain/model/exception';
import { BookingContainerRepositoryImpl } from './booking-container.repository';
const eventData = new BookingContainer({
  bookingNo: '',
  containerNo: '',
  activities: [],
});
const dataSave = new BookingContainer({
  bookingNo: 'fkdhj',
  containerNo: 'kfjdkfd',
  copNo: 'kfjkdf',
});
let mockBookingContainerRepository: DeepMocked<Repository<BookingContainer>>;
describe(`${BookingContainerRepositoryImpl.name}`, () => {
  let target: BookingContainerRepositoryImpl;
  const fileType = BookingContainerRepositoryImpl.prototype;
  mockBookingContainerRepository = createMock<Repository<BookingContainer>>();
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingContainerRepositoryImpl,
        {
          provide: getRepositoryToken(BookingContainer, DBConnections.INTERNAL),
          useValue: mockBookingContainerRepository,
        },
      ],
    }).compile();

    target = module.get<BookingContainerRepositoryImpl>(BookingContainerRepositoryImpl);
  });
  it(`${fileType.getEvent.name}_function_should_return_valid_data`, async () => {
    //Arrange
    mockBookingContainerRepository.findOne.mockResolvedValue(eventData);
    //Act
    const temp = await target.getEvent('', '', '');
    //Assert
    expect(temp).toMatchObject(eventData);
  });
  it(`${fileType.getEvent.name}_function_should_return_null_when_exception`, async () => {
    //Arrange
    mockBookingContainerRepository.findOne.mockImplementation(() => {
      throw new ValidateException(1, 'error');
    });
    //Act
    const temp = await target.getEvent('', '', '');
    //Assert
    expect(temp).toBeNull();
  });
  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save(dataSave);
    //Assert
    expect(temp).toBe(true);
  });
  it(`${fileType.save.name}_function_should_return_true`, async () => {
    //Arrange
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(true);
  });
  it(`${fileType.save.name}_function_should_return_false_when_exception`, async () => {
    //Arrange
    mockBookingContainerRepository.save.mockImplementation(() => {
      throw new ValidateException(1, 'error');
    });
    //Act
    const temp = await target.save([dataSave]);
    //Assert
    expect(temp).toBe(false);
  });
});
