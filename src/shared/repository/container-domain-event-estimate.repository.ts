/* eslint-disable no-await-in-loop */
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BookingContainer, BookingContainerActivity, BookingContainerActivityEstimate } from '../domain/entity';
import { BookingContainerActivityEstimateHistories } from '../domain/entity/booking-container-estimate-history.entity';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { ContainerDomainEventEstimateRepository } from '../domain/repository/container-domain-event-estimate.interface';
import { randomUUID } from 'crypto';
@Injectable()
export class ContainerEventRepositoryEstimateImpl implements ContainerDomainEventEstimateRepository {
  private readonly logger = new Logger(ContainerEventRepositoryEstimateImpl.name);

  constructor(
    @InjectRepository(BookingContainerActivityEstimate, DBConnections.INTERNAL)
    private readonly repositoryBookingContainerActivityEstimateData: Repository<BookingContainerActivityEstimate>,

    @InjectRepository(BookingContainerActivity, DBConnections.INTERNAL)
    private readonly repositoryBookingContainerActivityData: Repository<BookingContainerActivity>,

    @InjectRepository(BookingContainer, DBConnections.INTERNAL)
    private readonly repositoryBookingContainerData: Repository<BookingContainer>,

    @InjectRepository(BookingContainerActivityEstimateHistories, DBConnections.INTERNAL)
    private readonly estimateHistoryRepository: Repository<BookingContainerActivityEstimateHistories>,
  ) {}

  public async saveBookingContainer(data: BookingContainer) {
    try {
      const bookingContainer = await this.repositoryBookingContainerData.findOne({
        where: { bookingNo: data.bookingNo, copNo: data.copNo },
      });
      if (bookingContainer) {
        bookingContainer.containerNo = data.containerNo;
        await this.repositoryBookingContainerData.save(bookingContainer);
      } else {
        await this.repositoryBookingContainerData.save(data);
      }

      for (const activity of data.activities) {
        const bookingContainerActivity = await this.saveBookingContainerActivity(activity);

        for (const estimate of activity.activityEstimates) {
          this.saveBookingContainerActivityEstimate(estimate, bookingContainerActivity);
        }
        for (const estimateHistory of activity.activityEstimateHistories) {
          if (!estimateHistory.activityId) {
            estimateHistory.activityId = bookingContainerActivity.id;
            estimateHistory.id = randomUUID();
            this.estimateHistoryRepository.save(estimateHistory);
          }
        }
      }

      return true;
    } catch (error) {
      const err = error as Error;
      this.logger.error('BookingContainer save data error', err.message);
      return false;
    }
  }

  private async saveBookingContainerActivity(activity: BookingContainerActivity): Promise<BookingContainerActivity> {
    const bookingContainerActivity = await this.repositoryBookingContainerActivityData.findOne({
      where: { bookingNo: activity.bookingNo, copNo: activity.copNo, eventMatrixId: activity.eventMatrixId },
    });
    if (bookingContainerActivity) {
      bookingContainerActivity.containerNo = activity.containerNo;
      bookingContainerActivity.opusCode = activity.opusCode;
      bookingContainerActivity.copSeq = activity.copSeq;
      bookingContainerActivity.clptIndSeq = activity.clptIndSeq;
      bookingContainerActivity.countryId = activity.countryId;
      bookingContainerActivity.locationId = activity.locationId;
      bookingContainerActivity.vpsPortCd = activity.vpsPortCd;
      bookingContainerActivity.vesselCode = activity.vesselCode;
      bookingContainerActivity.skdVoyNo = activity.skdVoyNo;
      bookingContainerActivity.skdDirCd = activity.skdDirCd;
      bookingContainerActivity.clsCd = activity.clsCd;

      return await this.repositoryBookingContainerActivityData.save(bookingContainerActivity);
    } else {
      // eslint-disable-next-line require-atomic-updates
      activity.id = randomUUID();
      return await this.repositoryBookingContainerActivityData.save(activity);
    }
  }

  private async saveBookingContainerActivityEstimate(estimate: BookingContainerActivityEstimate, bookingContainerActivity: BookingContainerActivity) {
    const bookingContainerActivityEstimate = await this.repositoryBookingContainerActivityEstimateData.findOne({
      where: { activityId: bookingContainerActivity.id },
    });
    if (bookingContainerActivityEstimate) {
      bookingContainerActivityEstimate.estimateDate = estimate.estimateDate;
      bookingContainerActivityEstimate.planDate = estimate.planDate;
      this.repositoryBookingContainerActivityEstimateData.save(bookingContainerActivityEstimate);
    } else {
      estimate.activityId = bookingContainerActivity.id;
      estimate.id = randomUUID();
      this.repositoryBookingContainerActivityEstimateData.save(estimate);
    }
  }
}
