import { HttpModule } from '@nestjs/axios';
import { Global, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientsModule } from '@nestjs/microservices';
import { KAFKA_CLIENT, configKafkaEventFactory } from 'src/utils/event';
import { SchedulerApiName } from '../domain/api/scheduler.api.interface';
import { TailwindApiName } from '../domain/api/tailwind.api.interface';
import { SchedulerApiImpl } from './scheduler.api';
import { TailwindApiImpl } from './tailwind.api';

@Global()
@Module({
  imports: [
    HttpModule,
    ClientsModule.registerAsync([
      {
        name: KAFKA_CLIENT,
        useFactory: configKafkaEventFactory,
        inject: [ConfigService],
      },
    ]),
  ],
  providers: [
    {
      provide: TailwindApiName,
      useClass: TailwindApiImpl,
    },
    {
      provide: SchedulerApiName,
      useClass: SchedulerApiImpl,
    },
  ],
  exports: [TailwindApiName, SchedulerApiName],
})
export class ApiModule {}
