import { HttpService } from '@nestjs/axios';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { catchError, lastValueFrom, map } from 'rxjs';
import { TailwindApi } from '../domain/api/tailwind.api.interface';
import { LocationInfo } from '../domain/entity/api-response/location-info.entity';
import { SensorInfo } from '../domain/entity/api-response/sensor-info.entity';
import { ConfigService } from '@nestjs/config';
import { ConfigConstants } from '../domain/model/enum/domain.enum';
import { formatDate } from 'src/utils/date';
import { createBasicAuthHeader, createBearerAuthHeader } from '../../utils/api';
@Injectable()
export class TailwindApiImpl implements TailwindApi {
  private static getTokenDate: Date;
  private static token: string;

  constructor(private readonly httpService: HttpService, private readonly configService: ConfigService) {}
  //Here define static property

  public async getSensorInfo(containerNo: string): Promise<SensorInfo[]> {
    const accessToken = await this.getAccessToken();
    const userName = this.configService.get(ConfigConstants.ONE_APIS_USERNAME);
    const baseUrl = this.configService.get(ConfigConstants.ONE_IOT_API_URL);
    const requestDate = this.getRequestDate();
    const url = `${baseUrl}reefer?containerNumber=${containerNo}&fromDate=${requestDate}`;

    const request = this.httpService
      .get(url, {
        headers: createBearerAuthHeader(userName, accessToken),
      })
      .pipe(
        map((res) => {
          const data = res?.data.map((sensor) => this.createSensorInfoFromResponseObject(sensor));
          return data;
        }),
      )
      .pipe(
        catchError((err) => {
          throw new ForbiddenException(err, 'API calling has throwed new exception');
        }),
      );

    const data = await lastValueFrom(request);
    return data;
  }

  public async getLocationInfo(containerNo: string): Promise<LocationInfo[]> {
    const accessToken = await this.getAccessToken();
    const username = this.configService.get(ConfigConstants.ONE_APIS_USERNAME);
    const baseUrl = this.configService.get(ConfigConstants.ONE_IOT_API_URL);
    const requestDate = this.getRequestDate();
    const url = `${baseUrl}location?containerNumber=${containerNo}&fromDate=${requestDate}`;

    const request = this.httpService
      .get(url, {
        headers: createBearerAuthHeader(username, accessToken),
      })
      .pipe(
        map((res) => {
          const data = res?.data.map((location) => this.createLocationInfoFromResponseObject(location));
          return data;
        }),
      )
      .pipe(
        catchError((err) => {
          throw new ForbiddenException(err, 'API calling has throwed new exception');
        }),
      );

    const data = await lastValueFrom(request);
    return data;
  }

  private async getAccessToken(): Promise<string> {
    if (TailwindApiImpl.getTokenDate && TailwindApiImpl.token && !this.isExpired(TailwindApiImpl.getTokenDate)) {
      return TailwindApiImpl.token;
    }

    const url = this.configService.get(ConfigConstants.ONE_APIS_CREDENTIAL_URL);
    const userName = this.configService.get(ConfigConstants.ONE_APIS_USERNAME);
    const passWord = this.configService.get(ConfigConstants.ONE_APIS_PASSWORD);
    const data = {};

    // 👇️ const data: CreateUserResponse
    const request = this.httpService
      .post(url, data, {
        headers: createBasicAuthHeader(userName, passWord),
      })
      .pipe(
        map((res) => res.data),
        map((data) => {
          const accessToken = data?.access_token;
          if (!accessToken) return;
          TailwindApiImpl.token = accessToken;
          TailwindApiImpl.getTokenDate = new Date();
          return accessToken;
        }),
      )
      .pipe(
        catchError((err) => {
          throw new ForbiddenException(err, 'API not available');
        }),
      );
    const token = await lastValueFrom(request);
    return token;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private createSensorInfoFromResponseObject(sensor: any) {
    return new SensorInfo({
      containerNo: sensor?.containerNumber,
      whenCreated: sensor?.whenCreated ? new Date(sensor?.whenCreated) : null,
      assetId: sensor?.assetId,
      temperatureSetpoint: sensor?.settings?.temperatureSetpoint,
      ambientTemperature: sensor?.sensors?.ambientTemperature,
      o2: sensor?.sensors?.o2,
      co2: sensor?.sensors?.co2,
      returnAirTemperature: sensor?.sensors?.returnAirTemperature,
      supplyAirTemperature: sensor?.sensors?.supplyAirTemperature,
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private createLocationInfoFromResponseObject(location: any) {
    return new LocationInfo({
      containerNo: location?.containerNumber,
      whenCreated: location?.whenCreated ? new Date(location?.whenCreated) : null,
      assetId: location?.assetId,
      longitude: location?.longitude,
      latitude: location?.latitude,
    });
  }

  private isExpired(getTokenDate: Date) {
    const expireMin = Number(this.configService.get(ConfigConstants.ONE_APIS_TOKEN_EXPIRED_MIN));
    return (new Date().getTime() - getTokenDate.getTime()) / 60000 > expireMin;
  }

  private getRequestDate() {
    const frequenceByMin = this.configService.get(ConfigConstants.ONE_APIS_FREQUENCE_BY_MINUNTES);
    const durationPerMinute = 60000;
    const currentTime = new Date().getTime();
    const requestTime = new Date(currentTime - durationPerMinute * frequenceByMin);
    const result = formatDate(requestTime);
    return result;
  }
}
