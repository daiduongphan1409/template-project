import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { TestingModule, Test } from '@nestjs/testing';
import { of, pipe } from 'rxjs';
import { SensorInfo } from '../domain/entity/api-response/sensor-info.entity';
import { TailwindApiImpl } from './tailwind.api';

describe(`${TailwindApiImpl.name}`, () => {
  let api: TailwindApiImpl;
  const type = TailwindApiImpl.prototype;
  const sensorResponse = new SensorInfo({
    containerNo: 'containerNo',
    whenCreated: new Date('2022-10-22T00:07:55Z'),
    assetId: 'assetId',
    temperatureSetpoint: 2,
    ambientTemperature: 2,
    o2: 32,
    co2: 23,
  });
  const httpService = {
    get: jest
      .fn()
      .mockImplementation(pipe())
      .mockImplementation(() => of({ data: [sensorResponse] })),
    post: jest.fn().mockImplementation(() => of({ data: {} })),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        TailwindApiImpl,
        {
          provide: ConfigService,
          useValue: {
            get: jest.fn((key: string) => {
              // this is being super extra, in the case that you need multiple keys with the `get` method
              if (key === 'ONE_APIS_CREDENTIAL_URL') {
                return 'abc';
              }
              if (key === 'ONE_APIS_USERNAME') {
                return 'das';
              }
              if (key === 'ONE_APIS_PASSWORD') {
                return '23';
              }

              if (key === 'ONE_IOT_API_URL') {
                return 'abc';
              }
              if (key === 'ONE_APIS_FREQUENCE_BY_MINUNTES') {
                return 60;
              }
              if (key === 'ONE_APIS_TOKEN_EXPIRED_MIN') {
                return 59;
              }
              return null;
            }),
          },
        },
        {
          provide: HttpService,
          useValue: httpService,
        },
      ],
    }).compile();
    api = module.get<TailwindApiImpl>(TailwindApiImpl);
  });

  it(`${type.getLocationInfo.name}_Should_Run_When_DataCorrect`, async () => {
    const data = await api.getLocationInfo('FSCU5758219');
    expect(data).not.toBeNull();
  });

  it(`${type.getSensorInfo.name}_Should_Run_When_DataCorrect`, async () => {
    const data = await api.getSensorInfo('FSCU5758219');
    expect(data).not.toBeNull();
  });
});
