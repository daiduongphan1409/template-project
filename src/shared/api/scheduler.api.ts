import { Injectable, Logger } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { SchedulerCancelResponse } from 'src/opus-container-iot/dto/scheduler-cancel-respone.dto';
import { SchedulerRegisterResponse } from 'src/opus-container-iot/dto/scheduler-register-response.dto';
import { SchedulerRegisterRequest } from 'src/opus-container-iot/dto/scheduler-register.dto';
import { SchedulerApi } from '../domain/api/scheduler.api.interface';

@Injectable()
export class SchedulerApiImpl implements SchedulerApi {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async register(input: SchedulerRegisterRequest): Promise<SchedulerRegisterResponse> {
    try {
      //TODO: will be updated after Scheduler Services update
      //Call to Scheduler Service, will be update after implement on Scheduler Service
      return new SchedulerRegisterResponse({
        code: 200,
        message: 'success',
        data: {
          scheduler_id: randomUUID(),
        },
      });
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Register failed while calling to Scheduler Service , Message: ${error.message} `, error.stack);
      return null;
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async cancel(schedulerId: string): Promise<SchedulerCancelResponse> {
    try {
      //Call to Scheduler Service, will be update after implement on Scheduler Service
      return new SchedulerCancelResponse({
        code: 200,
        message: 'success',
      });
    } catch (ex) {
      const error = ex as Error;
      Logger.error(`Register failed while calling to Scheduler Service , Message: ${error.message} `, error.stack);
      return null;
    }
  }
}
