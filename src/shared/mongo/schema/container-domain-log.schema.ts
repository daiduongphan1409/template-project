import * as mongoose from 'mongoose';
import { ContainerDomainLog } from 'src/shared/domain/entity/container-domain-log.entity';
import { ContainerDomainLogState } from 'src/shared/domain/model/enum/filter-log-state.enum';

export type ContainerLogDocument = ContainerDomainLog & mongoose.Document;

export const ContainerDomainLogSchema = new mongoose.Schema(
  {
    event_id: {
      type: String,
      required: true,
      alias: 'eventId',
    },

    matrix_match: {
      type: Array<string>,
      required: true,
      alias: 'matrixMatch',
    },

    matrix_potential: {
      type: Array<string>,
      required: true,
      alias: 'matrixPotential',
    },

    created_time: {
      type: Date,
      required: true,
      alias: 'createdTime',
    },

    updated_time: {
      type: Date,
      required: true,
      alias: 'updatedTime',
    },

    state: {
      type: String,
      enum: ContainerDomainLogState,
      required: true,
    },
  },
  { collection: 'container_domain_logs' },
);
