import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

// For application
export function optionsFactory(configService: ConfigService): TypeOrmModuleOptions {
  return {
    type: 'postgres',
    host: configService.get('DATABASE_HOST'),
    port: configService.get('DATABASE_PORT'),
    username: configService.get('DATABASE_USER'),
    password: configService.get('DATABASE_PASSWORD'),
    database: configService.get('DATABASE_NAME'),
    schema: configService.get('DATABASE_SCHEMA'),
    entities: [__dirname + '/../schemas/*{.ts,.js}'],
    synchronize: configService.get('DATABASE_SYNCHRONIZE'),
    autoLoadEntities: true,
    migrationsTableName: 'migrations',
    migrations: [__dirname + '/migration/*{.ts,.js}'],
    migrationsRun: true,
  };
}
// For application
export function optionsODSFactory(configService: ConfigService): TypeOrmModuleOptions {
  return {
    type: 'postgres',
    host: configService.get('ODS_DATABASE_HOST'),
    port: configService.get('ODS_DATABASE_PORT'),
    username: configService.get('ODS_DATABASE_USER'),
    password: configService.get('ODS_DATABASE_PASSWORD'),
    database: configService.get('ODS_DATABASE_NAME'),
    schema: configService.get('ODS_DATABASE_SCHEMA'),
    entities: [__dirname + '/../ods-schema/*{.ts,.js}'],
    autoLoadEntities: true,
  };
}
// Manual load config when run from the cli
if (!process.env['DATABASE_HOST'] && (!process.env['NODE_ENV'] || process.env['NODE_ENV'] === 'local' || process.env['NODE_ENV'] === 'test')) {
  // eslint-disable-next-line
  require('dotenv').config();
}

// For cli migration
export const AppDataSource = new DataSource({
  type: 'postgres',
  host: process.env['DATABASE_HOST'],
  port: parseInt(process.env['DATABASE_PORT']),
  username: process.env['DATABASE_USER'],
  password: process.env['DATABASE_PASSWORD'],
  database: process.env['DATABASE_NAME'],
  schema: process.env['DATABASE_SCHEMA'],
  entities: [__dirname + '/schema/*.ts'],
  synchronize: JSON.parse(process.env['DATABASE_SYNCHRONIZE']),
  logging: true,
  migrationsRun: true,
  migrationsTableName: 'migrations',
  migrations: [__dirname + '/migration/*{.ts,.js}'],
});
