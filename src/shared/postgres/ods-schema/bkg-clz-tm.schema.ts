import { BookingClzTime } from 'src/shared/domain/entity-ods/bkg-clz-tm.entity';
import { EntitySchema } from 'typeorm';

export const BookingClzTimerEntity = new EntitySchema<BookingClzTime>({
  name: 'BookingClzTime',
  tableName: 'bkg_clz_tm',
  target: BookingClzTime,
  columns: {
    BKG_NO: {
      type: String,
      length: 13,
      primary: true,
      name: 'bkg_no',
    },
    CLZ_TP_CD: {
      type: String,
      length: 1,
      primary: true,
      name: 'clz_tp_cd',
    },
    CLZ_YD_CD: {
      type: String,
      length: 7,
      nullable: true,
      name: 'clz_yd_cd',
    },
    MNL_SET_DT: {
      type: Date,
      nullable: true,
      name: 'mnl_set_dt',
    },
    MNL_SET_USR_ID: {
      type: String,
      length: 100,
      nullable: true,
      name: 'mnl_set_usr_id',
    },
    NTC_FLG: {
      type: String,
      length: 1,
      nullable: true,
      name: 'ntc_flg',
    },
    SYS_SET_DT: {
      type: Date,
      nullable: true,
      name: 'sys_set_dt',
    },
  },
  orderBy: {
    BKG_NO: 'ASC',
  },
});
