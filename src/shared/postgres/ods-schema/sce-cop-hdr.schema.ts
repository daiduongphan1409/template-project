import { SupplyChainManagementCopHeader } from 'src/shared/domain/entity-ods';
import { EntitySchema } from 'typeorm';

export const SupplyChainManagementCopHdrEntity = new EntitySchema<SupplyChainManagementCopHeader>({
  name: 'SupplyChainManagementCopHeader',
  tableName: 'sce_cop_hdr',
  target: SupplyChainManagementCopHeader,
  columns: {
    COP_NO: {
      type: String,
      length: 14,
      primary: true,
      name: 'cop_no',
    },
    BKG_NO: {
      type: String,
      length: 13,
      primary: false,
      name: 'bkg_no',
    },
    CNTR_NO: {
      type: String,
      length: 14,
      primary: false,
      name: 'cntr_no',
    },
    COP_STS_CD: {
      type: String,
      length: 1,
      primary: false,
      name: 'cop_sts_cd',
    },
  },
  relations: {
    details: {
      type: 'one-to-many',
      target: 'SupplyChainManagementCopDetail',
    },
  },
  orderBy: {
    BKG_NO: 'ASC',
  },
});
