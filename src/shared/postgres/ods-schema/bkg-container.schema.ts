import { BkgContainer } from 'src/shared/domain/entity-ods/bkg-container.entity';
import { EntitySchema } from 'typeorm';
import { ColumnNumericTransformer } from '../utils/column-numberic-transformer';

export const BkgContainerEntity = new EntitySchema<BkgContainer>({
  name: 'BkgContainer',
  tableName: 'bkg_container',
  target: BkgContainer,
  columns: {
    BKG_NO: {
      type: String,
      length: 13,
      primary: true,
      name: 'bkg_no',
    },
    CNTR_NO: {
      type: String,
      length: 14,
      primary: true,
      name: 'cntr_no',
    },
    CNTR_TPSZ_CD: {
      type: String,
      length: 4,
      nullable: true,
      name: 'cntr_tpsz_cd',
    },
    CNMV_YR: {
      type: String,
      nullable: true,
      name: 'cnmv_yr',
    },
    CNMV_ID_NO: {
      type: Number,
      nullable: true,
      name: 'cnmv_id_no',
      transformer: new ColumnNumericTransformer(),
    },
    CNMV_CYC_NO: {
      type: Number,
      nullable: true,
      name: 'cnmv_cyc_no',
      transformer: new ColumnNumericTransformer(),
    },
    CNMV_STS_CD: {
      type: String,
      length: 2,
      nullable: true,
      name: 'cnmv_sts_cd',
    },
    PCK_TP_CD: {
      type: String,
      length: 2,
      nullable: true,
      name: 'pck_tp_cd',
    },
    PCK_QTY: {
      type: 'decimal',
      nullable: true,
      name: 'pck_qty',
      precision: 12,
      scale: 3,
      transformer: new ColumnNumericTransformer(),
    },
    WGT_UT_CD: {
      type: String,
      length: 3,
      nullable: true,
      name: 'wgt_ut_cd',
    },
    CNTR_WGT: {
      type: 'decimal',
      nullable: true,
      name: 'cntr_wgt',
      precision: 18,
      scale: 3,
      transformer: new ColumnNumericTransformer(),
    },
    MEAS_UT_CD: {
      type: String,
      length: 3,
      nullable: true,
      name: 'meas_ut_cd',
    },
    MEAS_QTY: {
      type: 'decimal',
      nullable: true,
      name: 'meas_qty',
      precision: 12,
      scale: 3,
      transformer: new ColumnNumericTransformer(),
    },
    CNTR_VOL_QTY: {
      type: 'decimal',
      nullable: true,
      name: 'cntr_vol_qty',
      precision: 8,
      scale: 2,
      transformer: new ColumnNumericTransformer(),
    },
    UPD_USR_ID: {
      type: String,
      length: 100,
      nullable: true,
      name: 'upd_usr_id',
    },
    CRE_USR_ID: {
      type: String,
      length: 100,
      nullable: true,
      name: 'cre_usr_id',
    },
    CRE_DT: {
      type: Date,
      nullable: true,
      name: 'cre_dt',
    },
    UPD_DT: {
      type: Date,
      nullable: true,
      name: 'upd_dt',
    },
  },
  orderBy: {
    CRE_DT: 'ASC',
  },
});
