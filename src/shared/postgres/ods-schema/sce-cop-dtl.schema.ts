import { SupplyChainManagementCopDetail } from 'src/shared/domain/entity-ods';
import { EntitySchema } from 'typeorm';

export const SupplyChainManagementCopDtlEntity = new EntitySchema<SupplyChainManagementCopDetail>({
  name: 'SupplyChainManagementCopDetail',
  tableName: 'sce_cop_dtl',
  target: SupplyChainManagementCopDetail,
  columns: {
    COP_NO: {
      type: String,
      length: 14,
      primary: true,
      name: 'cop_no',
    },
    COP_DTL_SEQ: {
      type: String,
      length: 4,
      primary: true,
      name: 'cop_dtl_seq',
    },
    PLN_DT: {
      type: Date,
      nullable: true,
      name: 'pln_dt',
    },
    ACT_CD: {
      type: String,
      length: 6,
      nullable: true,
      name: 'act_cd',
    },
    ACT_DT: {
      type: Date,
      nullable: true,
      name: 'act_dt',
    },
    ESTM_DT: {
      type: Date,
      nullable: true,
      name: 'estm_dt',
    },
    SKD_VOY_NO: {
      type: String,
      length: 4,
      nullable: true,
      name: 'skd_voy_no',
    },
    SKD_DIR_CD: {
      type: String,
      length: 1,
      nullable: true,
      name: 'skd_dir_cd',
    },
    CLPT_IND_SEQ: {
      type: String,
      length: 2,
      nullable: true,
      name: 'clpt_ind_seq',
    },
    VPS_PORT_CD: {
      type: String,
      length: 5,
      nullable: true,
      name: 'vps_port_cd',
    },
    NOD_CD: {
      type: String,
      length: 7,
      name: 'nod_cd',
    },
    VSL_CD: {
      type: String,
      length: 4,
      name: 'vsl_cd',
      primary: true,
    },
  },
  relations: {
    header: {
      target: 'SupplyChainManagementCopHeader',
      joinColumn: {
        referencedColumnName: 'COP_NO',
        name: 'cop_no',
      },
      type: 'many-to-one',
    },
  },
  orderBy: {
    COP_NO: 'ASC',
  },
});
