import { TrackerLocationEntity } from 'src/shared/domain/entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerLocationLogEntity = new EntitySchema<TrackerLocationEntity>({
  name: 'TrackerLocationEntity',
  tableName: 'BOOKING_CONTAINER_LOCATION_LOG',
  target: TrackerLocationEntity,
  columns: {
    ...BaseEntity,
    id: {
      type: Number,
      primary: true,
      name: 'id',
      generated: 'increment',
    },
    bookingNo: {
      name: 'BOOKING_NO',
      type: String,
      nullable: false,
      length: 12,
    },
    containerNo: {
      name: 'CONTAINER_NO',
      type: String,
      nullable: false,
      length: 14,
    },
    assetId: {
      name: 'asset_id',
      type: String,
      length: 45,
      nullable: false,
    },
    longitude: {
      name: 'LONGITUDE',
      type: 'decimal',
      nullable: true,
    },
    latitude: {
      name: 'LATITUDE',
      type: 'decimal',
      nullable: true,
    },
    whenCreated: {
      name: 'WHEN_CREATED',
      type: Date,
      nullable: false,
    },
    receivedDate: {
      name: 'RECEIVED_DATE',
      type: Date,
      nullable: false,
    },
    zoneId: {
      name: 'ZONE_ID',
      type: String,
      length: 10,
      nullable: true,
    },
    heading: {
      name: 'HEADING',
      type: Number,
      nullable: true,
    },
    altitude: {
      name: 'altitude',
      type: Number,
      nullable: true,
    },
  },
  orderBy: {
    id: 'ASC',
  },
});
