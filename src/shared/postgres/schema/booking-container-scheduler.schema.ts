import { SchedulerEntity } from 'src/shared/domain/entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerSchedulerEntity = new EntitySchema<SchedulerEntity>({
  name: 'SchedulerEntity',
  tableName: 'BOOKING_CONTAINER_SCHEDULER',
  target: SchedulerEntity,
  columns: {
    ...BaseEntity,
    id: {
      type: Number,
      primary: true,
      name: 'id',
      generated: 'increment',
    },
    type: {
      name: 'TYPE',
      type: Number,
      nullable: false,
    },
    bookingNo: {
      name: 'BOOKING_NO',
      type: String,
      length: 12,
      nullable: false,
    },
    containerNo: {
      name: 'CONTAINER_NO',
      type: String,
      length: 14,
      nullable: false,
    },
    schedulerId: {
      name: 'SCHEDULER_ID',
      type: String,
      length: 36,
      nullable: false,
    },
    isValid: {
      name: 'IS_VALID',
      type: Boolean,
      nullable: false,
    },
  },
  orderBy: {
    id: 'ASC',
  },
});
