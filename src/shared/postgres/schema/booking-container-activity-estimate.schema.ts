import { BookingContainerActivityEstimate } from 'src/shared/domain/entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerActivityEstimateEntity = new EntitySchema<BookingContainerActivityEstimate>({
  name: 'BookingContainerActivityEstimate',
  tableName: 'Booking_Container_Activity_Estimate',
  target: BookingContainerActivityEstimate,
  columns: {
    ...BaseEntity,
    id: {
      type: String,
      primary: true,
      length: 64,
      name: 'id',
    },
    activityId: {
      type: String,
      primary: false,
      length: 64,
      name: 'activity_id',
    },
    estimateDate: {
      type: Date,
      primary: false,
      name: 'estimate_date',
    },
    templateId: {
      type: String,
      length: 36,
      nullable: true,
      name: 'template_td',
    },
    planDate: {
      type: Date,
      primary: false,
      name: 'pln_dt',
    },
  },
  relations: {
    activity: {
      target: 'BookingContainerActivity',
      joinColumn: {
        referencedColumnName: 'id',
        name: 'activity_id',
      },
      nullable: true,
      primary: true,
      type: 'many-to-one',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
