import { BookingContainer } from 'src/shared/domain/entity/booking-container.entity';
import { EntitySchema } from 'typeorm';
import { ColumnNumericTransformer } from '../utils/column-numberic-transformer';
import { BaseEntity } from './base.schema';

export const BookingContainerEntity = new EntitySchema<BookingContainer>({
  name: 'BookingContainer',
  tableName: 'Booking_Container',
  target: BookingContainer,
  columns: {
    ...BaseEntity,
    bookingNo: {
      type: String,
      length: 13,
      primary: true,
      name: 'booking_no',
    },
    containerNo: {
      type: String,
      length: 14,
      primary: true,
      name: 'container_no',
    },
    copNo: {
      type: String,
      length: 14,
      primary: true,
      name: 'cop_no',
    },
    typeSizeCode: {
      type: String,
      length: 4,
      nullable: false,
      name: 'type_size_code',
    },
    commodityCode: {
      type: String,
      length: 6,
      nullable: true,
      name: 'commodity_code',
    },
    movementYear: {
      type: String,
      length: 4,
      nullable: false,
      name: 'movement_year',
    },
    movementId: {
      type: Number,
      nullable: false,
      name: 'movement_id',
      transformer: new ColumnNumericTransformer(),
    },
    movementCycleNo: {
      type: Number,
      nullable: false,
      name: 'movement_cycle_no',
      transformer: new ColumnNumericTransformer(),
    },
    movementStatus: {
      type: String,
      length: 2,
      nullable: false,
      name: 'movement_status',
    },
    measurementUnitCode: {
      type: String,
      length: 3,
      nullable: true,
      name: 'measurement_unit_code',
    },
    measurementQuantity: {
      type: 'decimal',
      nullable: true,
      name: 'measurement_quantity',
      precision: 12,
      scale: 3,
      transformer: new ColumnNumericTransformer(),
    },
    packageUnitCode: {
      type: String,
      length: 2,
      nullable: true,
      name: 'package_unit_code',
    },
    packageQuantity: {
      type: 'decimal',
      nullable: true,
      name: 'package_quantity',
      precision: 12,
      scale: 3,
      transformer: new ColumnNumericTransformer(),
    },
    packageWeightUnitCode: {
      type: String,
      length: 3,
      nullable: true,
      name: 'package_weight_unit_code',
    },
    packageWeight: {
      type: 'decimal',
      nullable: true,
      name: 'package_weight',
      precision: 18,
      scale: 3,
      transformer: new ColumnNumericTransformer(),
    },
    volumeQuantity: {
      type: 'decimal',
      nullable: true,
      name: 'volume_quantity',
      precision: 8,
      scale: 2,
      transformer: new ColumnNumericTransformer(),
    },
    isValid: {
      type: Boolean,
      nullable: false,
      default: true,
      name: 'is_valid',
    },
    createdBy: {
      type: String,
      nullable: true,
      name: 'created_by',
    },
    updatedBy: {
      type: String,
      nullable: true,
      name: 'updated_by',
    },
  },
  relations: {
    activities: {
      target: 'BookingContainerActivity',
      inverseSide: 'bookingContainer',
      primary: false,
      type: 'one-to-many',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
