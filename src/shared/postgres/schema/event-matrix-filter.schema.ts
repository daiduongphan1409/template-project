import { EventMatrixFilter } from 'src/shared/domain/entity/event-matrix-filter.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const EventMatrixFilterEntity = new EntitySchema<EventMatrixFilter>({
  name: 'EventMatrixFilter',
  tableName: 'Event_Matrix_Filter',
  target: EventMatrixFilter,
  columns: {
    id: {
      type: String,
      length: 36,
      primary: true,
      name: 'id',
    },
    matrixId: {
      type: String,
      length: 4,
      nullable: false,
      name: 'matrix_id',
    },
    triggerId: {
      type: Number,
      nullable: false,
      name: 'trigger_id',
    },
    tableName: {
      type: String,
      length: 128,
      nullable: true,
      name: 'table_name',
    },
    columnName: {
      type: String,
      length: 128,
      nullable: false,
      name: 'column_name',
    },
    isValid: {
      type: Boolean,
      name: 'is_valid',
    },
    ...BaseEntity,
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
