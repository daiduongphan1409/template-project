import { EventMatrixChannel } from 'src/shared/domain/entity/event-matrix-channel.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const EventMatrixChannelEntity = new EntitySchema<EventMatrixChannel>({
  name: 'EventMatrixChannel',
  tableName: 'Event_Matrix_Channel',
  columns: {
    ...BaseEntity,
    channelId: {
      name: 'chanel_id',
      type: Number,
      primary: true,
    },
    matrixId: {
      name: 'matrix_id',
      type: String,
      length: 4,
      nullable: false,
    },
    templateId: {
      name: 'template_id',
      type: String,
      length: 36,
      nullable: true,
    },
    isValid: {
      type: Number,
      nullable: false,
      name: 'is_valid',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
