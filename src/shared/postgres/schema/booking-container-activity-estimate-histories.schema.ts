import { BookingContainerActivityEstimateHistories } from 'src/shared/domain/entity/booking-container-estimate-history.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerActivityEstimateHistoriesEntity = new EntitySchema<BookingContainerActivityEstimateHistories>({
  name: 'BookingContainerActivityEstimateHistories',
  tableName: 'Booking_Container_Activity_Estimate_Histories',
  target: BookingContainerActivityEstimateHistories,
  columns: {
    ...BaseEntity,
    id: {
      type: String,
      primary: true,
      length: 64,
      name: 'id',
    },
    activityId: {
      type: String,
      primary: false,
      nullable: false,
      length: 64,
      name: 'activity_id',
    },
    estimate: {
      type: Date,
      primary: false,
      name: 'estimate_date',
    },
  },
  relations: {
    activity: {
      target: 'BookingContainerActivity',
      joinColumn: {
        referencedColumnName: 'id',
        name: 'activity_id',
      },
      nullable: true,
      primary: true,
      type: 'many-to-one',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
