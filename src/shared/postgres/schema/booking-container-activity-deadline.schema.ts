import { BookingContainerActivityDeadline } from 'src/shared/domain/entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerActivityDeadlineEntity = new EntitySchema<BookingContainerActivityDeadline>({
  name: 'BookingContainerActivityDeadline',
  tableName: 'Booking_Container_Activity_Deadline',
  target: BookingContainerActivityDeadline,
  columns: {
    ...BaseEntity,
    id: {
      type: String,
      primary: true,
      length: 64,
      name: 'id',
    },
    activityId: {
      type: String,
      primary: false,
      nullable: false,
      length: 64,
      name: 'activity_id',
    },
    deadlineDate: {
      type: Date,
      primary: false,
      name: 'deadline_date',
    },
    templateId: {
      type: String,
      length: 36,
      nullable: true,
      name: 'template_td',
    },
  },
  relations: {
    activity: {
      target: 'BookingContainerActivity',
      joinColumn: {
        referencedColumnName: 'id',
        name: 'activity_id',
      },
      nullable: true,
      primary: true,
      type: 'many-to-one',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
