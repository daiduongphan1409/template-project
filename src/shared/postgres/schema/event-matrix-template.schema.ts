import { EventMatrixTemplate } from 'src/shared/domain/entity/event-matrix-template.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const EventMatrixTemplateEntity = new EntitySchema<EventMatrixTemplate>({
  name: 'EventMatrixTemplate',
  tableName: 'Event_Matrix_Template',
  columns: {
    ...BaseEntity,
    id: {
      type: Number,
      primary: true,
    },
    template: {
      type: String,
      length: 2048,
      nullable: false,
    },
    isValid: {
      type: Number,
      nullable: false,
      name: 'is_valid',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
