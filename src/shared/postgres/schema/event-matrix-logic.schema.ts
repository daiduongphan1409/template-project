import { EventMatrixLogic } from 'src/shared/domain/entity/event-matrix-logic.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const EventMatrixLogicEntity = new EntitySchema<EventMatrixLogic>({
  name: 'EventMatrixLogic',
  tableName: 'Event_Matrix_Logic',
  columns: {
    matrixId: {
      type: String,
      length: 4,
      primary: true,
      name: 'matrix_id',
    },
    triggerId: {
      type: Number,
      primary: true,
      name: 'trigger_id',
    },
    logicType: {
      type: Number,
      name: 'logic_type',
    },
    description: {
      type: String,
      length: 1024,
      nullable: true,
      name: 'description',
    },
    script: {
      type: String,
      length: 2048,
      nullable: false,
      name: 'script',
    },
    isValid: {
      type: Boolean,
      name: 'is_valid',
    },
    ...BaseEntity,
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
