import { BookingContainerActivityDeadlineHistories } from 'src/shared/domain/entity/booking-container-deadline-history.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerActivityDeadlineHistoriesEntity = new EntitySchema<BookingContainerActivityDeadlineHistories>({
  name: 'BookingContainerActivityDeadlineHistories',
  tableName: 'Booking_Container_Activity_Deadline_Histories',
  target: BookingContainerActivityDeadlineHistories,
  columns: {
    ...BaseEntity,
    id: {
      type: String,
      primary: true,
      name: 'id',
      length: 64,
    },
    activityId: {
      type: String,
      primary: false,
      nullable: false,
      length: 64,
      name: 'activity_id',
    },
    deadlineDate: {
      type: Date,
      primary: false,
      name: 'deadline_date',
    },
  },
  relations: {
    activity: {
      target: 'BookingContainerActivity',
      joinColumn: {
        referencedColumnName: 'id',
        name: 'activity_id',
      },
      nullable: true,
      primary: true,
      type: 'many-to-one',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
