import { TrackerSensorEntity } from 'src/shared/domain/entity/tracker-sensor.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerSensorLogEntity = new EntitySchema<TrackerSensorEntity>({
  name: 'TrackerSensorEntity',
  tableName: 'BOOKING_CONTAINER_SENSOR_LOG',
  target: TrackerSensorEntity,
  columns: {
    ...BaseEntity,
    id: {
      type: Number,
      primary: true,
      name: 'id',
      generated: 'increment',
    },
    bookingNo: {
      name: 'BOOKING_NO',
      type: String,
      nullable: false,
      length: 12,
    },
    containerNo: {
      name: 'CONTAINER_NO',
      type: String,
      nullable: false,
      length: 14,
    },
    assetId: {
      name: 'ASSET_ID',
      type: String,
      length: 45,
      nullable: false,
    },
    ambientTemperature: {
      name: 'AMBIENT_TEMPERATURE',
      type: 'decimal',
      nullable: true,
    },
    o2: {
      name: 'O2',
      type: 'decimal',
      nullable: true,
    },
    returnAirTemperature: {
      name: 'RETURN_AIR_TEMPERATURE',
      type: 'decimal',
      nullable: true,
    },
    supplyAirTemperature: {
      name: 'SUPPLY_AIR_TEMPERATURE',
      type: 'decimal',
      nullable: true,
    },
    receivedDate: {
      name: 'RECEIVED_DATE',
      type: Date,
      nullable: false,
    },
    whenCreated: {
      name: 'WHEN_CREATED',
      type: Date,
      nullable: false,
    },
    co2: {
      name: 'CO2',
      type: 'decimal',
      nullable: true,
    },
    ethylene: {
      name: 'ETHYLENE',
      type: 'decimal',
      nullable: true,
    },
    humidity: {
      name: 'HUMIDITY',
      type: 'decimal',
      nullable: true,
    },
    nitrogen: {
      name: 'NITROGEN',
      type: 'decimal',
      nullable: true,
    },
    returnAir2Temperature: {
      name: 'RETURN_AIR_2_TEMPERATURE',
      type: 'decimal',
      nullable: true,
    },
    supplyAir2Temperature: {
      name: 'SUPPLY_AIR_2_TEMPERATURE',
      type: 'decimal',
      nullable: true,
    },
    airExchange: {
      name: 'AIR_EXCHANGE',
      type: 'decimal',
      nullable: true,
    },
    co: {
      name: 'CO',
      type: 'decimal',
      nullable: true,
    },
  },
  orderBy: {
    id: 'ASC',
  },
});
