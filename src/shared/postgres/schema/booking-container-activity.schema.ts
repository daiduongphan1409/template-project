import { BookingContainerActivity } from 'src/shared/domain/entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const BookingContainerActivityEntity = new EntitySchema<BookingContainerActivity>({
  name: 'BookingContainerActivity',
  tableName: 'Booking_Container_Activity',
  target: BookingContainerActivity,
  columns: {
    ...BaseEntity,
    id: {
      type: String,
      primary: true,
      length: 64,
      name: 'id',
    },
    bookingNo: {
      type: String,
      length: 13,
      primary: false,
      name: 'booking_no',
    },
    containerNo: {
      type: String,
      length: 14,
      primary: false,
      name: 'container_no',
    },
    copNo: {
      type: String,
      length: 14,
      nullable: true,
      name: 'cop_no',
    },
    copSeq: {
      type: Number,
      primary: false,
      name: 'cop_seq',
    },
    countryId: {
      type: String,
      length: 2,
      nullable: true,
      name: 'country_id',
    },
    ediCode: {
      type: String,
      length: 2,
      nullable: true,
      name: 'edi_code',
    },
    vesselCode: {
      type: String,
      length: 4,
      nullable: true,
      name: 'vessel_code',
    },
    opusCode: {
      type: String,
      length: 6,
      nullable: true,
      name: 'opus_code',
    },
    eventMatrixId: {
      type: String,
      length: 4,
      nullable: true,
      name: 'event_matrix_id',
    },
    locationId: {
      type: String,
      length: 7,
      nullable: true,
      name: 'location_id',
    },
    skdVoyNo: {
      type: String,
      length: 4,
      nullable: true,
      name: 'skd_voy_no',
    },
    skdDirCd: {
      type: String,
      length: 1,
      nullable: true,
      name: 'skd_dir_cd',
    },
    clptIndSeq: {
      type: String,
      length: 2,
      nullable: true,
      name: 'clpt_ind_seq',
    },
    vpsPortCd: {
      type: String,
      length: 5,
      nullable: true,
      name: 'vps_port_cd',
    },
    isValid: {
      type: Number,
      nullable: false,
      default: 1,
      name: 'is_valid',
    },
  },
  relations: {
    bookingContainer: {
      target: 'BookingContainer',
      joinColumn: [
        {
          referencedColumnName: 'bookingNo',
          name: 'booking_no',
        },
        {
          referencedColumnName: 'containerNo',
          name: 'container_no',
        },
        {
          referencedColumnName: 'copNo',
          name: 'cop_no',
        },
      ],
      nullable: true,
      primary: true,
      type: 'many-to-one',
    },
    activitydeadLines: {
      target: 'BookingContainerActivityDeadline',
      joinColumn: {
        referencedColumnName: 'activityId',
        name: 'activity_id',
      },
      inverseSide: 'activity',
      primary: false,
      type: 'one-to-many',
    },
    activityDeadLineHistories: {
      target: 'BookingContainerActivityDeadlineHistories',
      joinColumn: {
        referencedColumnName: 'activityId',
        name: 'activity_id',
      },
      inverseSide: 'activity',
      primary: false,
      type: 'one-to-many',
    },
    activityEstimateHistories: {
      target: 'BookingContainerActivityEstimateHistories',
      joinColumn: {
        referencedColumnName: 'activityId',
        name: 'activity_id',
      },
      inverseSide: 'activity',
      primary: false,
      type: 'one-to-many',
    },
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
