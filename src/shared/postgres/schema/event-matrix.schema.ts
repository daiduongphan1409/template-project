import { EventMatrix } from 'src/shared/domain/entity/event-matrix.entity';
import { EntitySchema } from 'typeorm';
import { BaseEntity } from './base.schema';

export const EventMatrixEntity = new EntitySchema<EventMatrix>({
  name: 'EventMatrix',
  tableName: 'Event_Matrix',
  target: EventMatrix,
  columns: {
    id: {
      type: String,
      length: 4,
      primary: true,
      name: 'id',
    },
    name: {
      type: String,
      length: 100,
      nullable: false,
      name: 'name',
    },
    description: {
      type: String,
      length: 1024,
      nullable: true,
      name: 'description',
    },
    eventType: {
      type: Number,
      nullable: true,
      name: 'event_type',
    },
    category: {
      type: Number,
      nullable: true,
      name: 'category',
    },
    level: {
      type: Number,
      nullable: true,
      name: 'level',
    },
    bound: {
      type: Number,
      nullable: true,
      name: 'bound',
    },
    domain: {
      type: Number,
      nullable: true,
      name: 'domain',
    },
    edi315Status: {
      type: String,
      length: 10,
      nullable: true,
      name: 'edi_315_status',
    },
    opusCode: {
      type: String,
      length: 100,
      nullable: true,
      name: 'opus_code',
    },
    isValid: {
      type: Boolean,
      name: 'is_valid',
    },
    ...BaseEntity,
  },
  orderBy: {
    createdAt: 'ASC',
  },
});
