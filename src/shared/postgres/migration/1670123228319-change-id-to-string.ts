import { MigrationInterface, QueryRunner } from "typeorm";

export class changeIdToString1670123228319 implements MigrationInterface {
    name = 'changeIdToString1670123228319'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP CONSTRAINT "FK_4ebae9e9c463656265bc5648ff1"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP CONSTRAINT "PK_2bfb8caa5b57b2f7d21073298a2"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD "id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD CONSTRAINT "PK_2bfb8caa5b57b2f7d21073298a2" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD "activity_id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP CONSTRAINT "FK_0c443344ba3bd54286883d8627f"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP CONSTRAINT "PK_ec7b3dc94cddf82347d4bbdca96"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD "id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD CONSTRAINT "PK_ec7b3dc94cddf82347d4bbdca96" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD "activity_id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP CONSTRAINT "FK_b20962cef7cc5b48a9cc09327ff"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP CONSTRAINT "PK_a078779b72a0aadc29419d67789"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD "id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD CONSTRAINT "PK_a078779b72a0aadc29419d67789" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD "activity_id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP CONSTRAINT "FK_29ce3d20b98f694880bce3bfd61"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP CONSTRAINT "PK_0acb9125b593b18eec9c081f382"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD "id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD CONSTRAINT "PK_0acb9125b593b18eec9c081f382" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD "activity_id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP CONSTRAINT "PK_fce4fbd01f35c7bfa8ebb239042"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD "id" character varying(64) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD CONSTRAINT "PK_fce4fbd01f35c7bfa8ebb239042" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD CONSTRAINT "FK_4ebae9e9c463656265bc5648ff1" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD CONSTRAINT "FK_0c443344ba3bd54286883d8627f" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD CONSTRAINT "FK_b20962cef7cc5b48a9cc09327ff" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD CONSTRAINT "FK_29ce3d20b98f694880bce3bfd61" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP CONSTRAINT "FK_29ce3d20b98f694880bce3bfd61"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP CONSTRAINT "FK_b20962cef7cc5b48a9cc09327ff"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP CONSTRAINT "FK_0c443344ba3bd54286883d8627f"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP CONSTRAINT "FK_4ebae9e9c463656265bc5648ff1"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP CONSTRAINT "PK_fce4fbd01f35c7bfa8ebb239042"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD CONSTRAINT "PK_fce4fbd01f35c7bfa8ebb239042" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD "activity_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP CONSTRAINT "PK_0acb9125b593b18eec9c081f382"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD CONSTRAINT "PK_0acb9125b593b18eec9c081f382" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD CONSTRAINT "FK_29ce3d20b98f694880bce3bfd61" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD "activity_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP CONSTRAINT "PK_a078779b72a0aadc29419d67789"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD CONSTRAINT "PK_a078779b72a0aadc29419d67789" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD CONSTRAINT "FK_b20962cef7cc5b48a9cc09327ff" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD "activity_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP CONSTRAINT "PK_ec7b3dc94cddf82347d4bbdca96"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD CONSTRAINT "PK_ec7b3dc94cddf82347d4bbdca96" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD CONSTRAINT "FK_0c443344ba3bd54286883d8627f" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP COLUMN "activity_id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD "activity_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP CONSTRAINT "PK_2bfb8caa5b57b2f7d21073298a2"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD CONSTRAINT "PK_2bfb8caa5b57b2f7d21073298a2" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD CONSTRAINT "FK_4ebae9e9c463656265bc5648ff1" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
