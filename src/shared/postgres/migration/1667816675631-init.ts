import { MigrationInterface, QueryRunner } from "typeorm";

export class init1667816675631 implements MigrationInterface {
    name = 'init1667816675631'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "Booking_Container" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "booking_no" character varying(12) NOT NULL, "container_no" character varying(12) NOT NULL, "type_size_code" character varying(2), "commodity_code" character varying(6), "movement_year" character varying(5), "movement_id" character varying(45), "movement_cycle_no" character varying(45), "movement_status" character varying(45), "measurement_code" character varying(45), "measurement_quantity" integer, "is_valid" integer NOT NULL, CONSTRAINT "PK_cd7ba4f6052d451baf0d2852315" PRIMARY KEY ("booking_no", "container_no"))`);
        await queryRunner.query(`CREATE TABLE "Event_Matrix_Channel" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "chanel_id" integer NOT NULL, "matrix_id" character varying(4) NOT NULL, "template_id" character varying(36), "is_valid" integer NOT NULL, CONSTRAINT "PK_dd1aab00e646941578deefb0838" PRIMARY KEY ("chanel_id"))`);
        await queryRunner.query(`CREATE TABLE "Event_Matrix_Filter" ("id" character varying(36) NOT NULL, "matrix_id" character varying(4) NOT NULL, "trigger_id" integer NOT NULL, "table_name" character varying(128), "column_name" character varying(128) NOT NULL, "is_valid" boolean NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_f4a23d1a6a0d37b921063bd8661" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Event_Matrix_Logic" ("matrix_id" character varying(4) NOT NULL, "trigger_id" integer NOT NULL, "logic_type" integer NOT NULL, "description" character varying(1024), "script" character varying(2048) NOT NULL, "is_valid" boolean NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_da7d3f591585c4c4c8f0f4c8161" PRIMARY KEY ("matrix_id", "trigger_id"))`);
        await queryRunner.query(`CREATE TABLE "Event_Matrix_Template" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" integer NOT NULL, "template" character varying(2048) NOT NULL, "is_valid" integer NOT NULL, CONSTRAINT "PK_c71df979b43a74a66423cf7cdc6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Event_Matrix" ("id" character varying(4) NOT NULL, "name" character varying(100) NOT NULL, "description" character varying(1024), "event_type" integer, "category" integer, "level" integer, "bound" integer, "domain" integer, "edi_315_status" character varying(10), "opus_code" character varying(100), "is_valid" boolean NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_4dcb384b17978da47a904b7697a" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "Event_Matrix"`);
        await queryRunner.query(`DROP TABLE "Event_Matrix_Template"`);
        await queryRunner.query(`DROP TABLE "Event_Matrix_Logic"`);
        await queryRunner.query(`DROP TABLE "Event_Matrix_Filter"`);
        await queryRunner.query(`DROP TABLE "Event_Matrix_Channel"`);
        await queryRunner.query(`DROP TABLE "Booking_Container"`);
    }

}
