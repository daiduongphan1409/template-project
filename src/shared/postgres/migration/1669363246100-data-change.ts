import { MigrationInterface, QueryRunner } from "typeorm";

export class datachange1669363246100 implements MigrationInterface {
    name = 'datachange1669363246100'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "Booking_Container_Activity_Deadline_Histories" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "activity_id" integer NOT NULL, "deadline_date" TIMESTAMP NOT NULL, CONSTRAINT "PK_2bfb8caa5b57b2f7d21073298a2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Booking_Container_Activity_Deadline" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "activity_id" integer NOT NULL, "deadline_date" TIMESTAMP NOT NULL, "template_td" character varying(36), CONSTRAINT "PK_ec7b3dc94cddf82347d4bbdca96" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Booking_Container_Activity_Estimate_Histories" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "activity_id" integer NOT NULL, "estimate_date" TIMESTAMP NOT NULL, CONSTRAINT "PK_a078779b72a0aadc29419d67789" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Booking_Container_Activity_Estimate" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "activity_id" integer NOT NULL, "estimate_date" TIMESTAMP NOT NULL, "template_td" character varying(36), "pln_dt" TIMESTAMP NOT NULL, CONSTRAINT "PK_0acb9125b593b18eec9c081f382" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "Booking_Container_Activity" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "booking_no" character varying(13) NOT NULL, "container_no" character varying(14) NOT NULL, "cop_seq" integer NOT NULL, "country_id" character varying(2), "edi_code" character varying(2), "vessel_code" character varying(4), "opus_code" character varying(6), "event_matrix_id" character varying(4), "location_id" character varying(7), "skd_voy_no" character varying(4), "skd_dir_cd" character varying(1), "clpt_ind_seq" character varying(2), "vps_port_cd" character varying(5), "is_valid" integer NOT NULL DEFAULT '1', CONSTRAINT "PK_fce4fbd01f35c7bfa8ebb239042" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" ADD "cop_no" character varying(14)`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" ALTER COLUMN "is_valid" SET DEFAULT '1'`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" ADD CONSTRAINT "FK_4ebae9e9c463656265bc5648ff1" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" ADD CONSTRAINT "FK_0c443344ba3bd54286883d8627f" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" ADD CONSTRAINT "FK_b20962cef7cc5b48a9cc09327ff" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" ADD CONSTRAINT "FK_29ce3d20b98f694880bce3bfd61" FOREIGN KEY ("activity_id") REFERENCES "Booking_Container_Activity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD CONSTRAINT "FK_746440caae6af0077c81ce2a836" FOREIGN KEY ("booking_no", "container_no") REFERENCES "Booking_Container"("booking_no","container_no") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP CONSTRAINT "FK_746440caae6af0077c81ce2a836"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate" DROP CONSTRAINT "FK_29ce3d20b98f694880bce3bfd61"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Estimate_Histories" DROP CONSTRAINT "FK_b20962cef7cc5b48a9cc09327ff"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline" DROP CONSTRAINT "FK_0c443344ba3bd54286883d8627f"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity_Deadline_Histories" DROP CONSTRAINT "FK_4ebae9e9c463656265bc5648ff1"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" ALTER COLUMN "is_valid" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" DROP COLUMN "cop_no"`);
        await queryRunner.query(`DROP TABLE "Booking_Container_Activity"`);
        await queryRunner.query(`DROP TABLE "Booking_Container_Activity_Estimate"`);
        await queryRunner.query(`DROP TABLE "Booking_Container_Activity_Estimate_Histories"`);
        await queryRunner.query(`DROP TABLE "Booking_Container_Activity_Deadline"`);
        await queryRunner.query(`DROP TABLE "Booking_Container_Activity_Deadline_Histories"`);
    }

}
