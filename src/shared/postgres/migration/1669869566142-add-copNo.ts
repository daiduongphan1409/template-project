import { MigrationInterface, QueryRunner } from "typeorm";

export class addCopNo1669869566142 implements MigrationInterface {
    name = 'addCopNo1669869566142'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP CONSTRAINT "FK_746440caae6af0077c81ce2a836"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD "cop_no" character varying(14)`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" DROP CONSTRAINT "PK_cd7ba4f6052d451baf0d2852315"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" ADD CONSTRAINT "PK_dfc8efa9a761ae041550d359d52" PRIMARY KEY ("booking_no", "container_no", "cop_no")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" ALTER COLUMN "cop_no" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD CONSTRAINT "FK_3dc0ceb5c7175e28b0f780c18b3" FOREIGN KEY ("booking_no", "container_no", "cop_no") REFERENCES "Booking_Container"("booking_no","container_no","cop_no") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP CONSTRAINT "FK_3dc0ceb5c7175e28b0f780c18b3"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" ALTER COLUMN "cop_no" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" DROP CONSTRAINT "PK_dfc8efa9a761ae041550d359d52"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container" ADD CONSTRAINT "PK_cd7ba4f6052d451baf0d2852315" PRIMARY KEY ("booking_no", "container_no")`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" DROP COLUMN "cop_no"`);
        await queryRunner.query(`ALTER TABLE "Booking_Container_Activity" ADD CONSTRAINT "FK_746440caae6af0077c81ce2a836" FOREIGN KEY ("booking_no", "container_no") REFERENCES "Booking_Container"("booking_no","container_no") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
