import { MigrationInterface, QueryRunner } from "typeorm";

export class init1669112256223 implements MigrationInterface {
    name = 'init1669112256223'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "BOOKING_CONTAINER_LOCATION_LOG" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "BOOKING_NO" character varying(12) NOT NULL, "CONTAINER_NO" character varying(14) NOT NULL, "asset_id" character varying(45) NOT NULL, "LONGITUDE" numeric, "LATITUDE" numeric, "WHEN_CREATED" TIMESTAMP NOT NULL, "RECEIVED_DATE" TIMESTAMP NOT NULL, "ZONE_ID" character varying(10), "HEADING" integer, "altitude" integer, CONSTRAINT "PK_65732d8f5331afffd5e27fcd803" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "BOOKING_CONTAINER_SCHEDULER" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "TYPE" integer NOT NULL, "BOOKING_NO" character varying(12) NOT NULL, "CONTAINER_NO" character varying(14) NOT NULL, "SCHEDULER_ID" character varying(36) NOT NULL, "IS_VALID" boolean NOT NULL, CONSTRAINT "PK_82334f04e33beeac33f2ac457ab" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "BOOKING_CONTAINER_SENSOR_LOG" ("created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "id" SERIAL NOT NULL, "BOOKING_NO" character varying(12) NOT NULL, "CONTAINER_NO" character varying(14) NOT NULL, "ASSET_ID" character varying(45) NOT NULL, "AMBIENT_TEMPERATURE" numeric, "O2" numeric, "RETURN_AIR_TEMPERATURE" numeric, "SUPPLY_AIR_TEMPERATURE" numeric, "RECEIVED_DATE" TIMESTAMP NOT NULL, "WHEN_CREATED" TIMESTAMP NOT NULL, "CO2" numeric, "ETHYLENE" numeric, "HUMIDITY" numeric, "NITROGEN" numeric, "RETURN_AIR_2_TEMPERATURE" numeric, "SUPPLY_AIR_2_TEMPERATURE" numeric, "AIR_EXCHANGE" numeric, "CO" numeric, CONSTRAINT "PK_f5907c28c0ae1c4220995110760" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "BOOKING_CONTAINER_SENSOR_LOG"`);
        await queryRunner.query(`DROP TABLE "BOOKING_CONTAINER_SCHEDULER"`);
        await queryRunner.query(`DROP TABLE "BOOKING_CONTAINER_LOCATION_LOG"`);
    }

}
