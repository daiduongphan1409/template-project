import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DBConnections } from '../domain/model/enum/domain.enum';
import { optionsFactory, optionsODSFactory } from './configs';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      name: DBConnections.INTERNAL,
      useFactory: optionsFactory,
      inject: [ConfigService],
    }),
    TypeOrmModule.forRootAsync({
      name: DBConnections.ODS,
      useFactory: optionsODSFactory,
      inject: [ConfigService],
    }),
  ],
})
export class PostgresModule {}
