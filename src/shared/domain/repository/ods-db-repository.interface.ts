import { BookingClzTime, SupplyChainManagementCopDetail, SupplyChainManagementCopHeader } from '../entity-ods';
import { BkgContainer } from '../entity-ods/bkg-container.entity';

export const odsRepositoryName = 'ODSRepository.Interface';

export interface ODSRepository {
  getListBookingNumber(bookingNumber: string): Promise<BkgContainer[]>;
  getBookingContainer(bookingNumber: string, containerNumber: string): Promise<BkgContainer>;
  getBookingCutoffTimeList(bkgNo: string): Promise<BookingClzTime[]>;
  getBkgClzTm(bkgNo: string, clzTypeCode: string): Promise<BookingClzTime>;
  getSceCopDtl(copNo: string, copDtlSeq: string): Promise<SupplyChainManagementCopDetail>;
  getSceCopHdr(copNo: string): Promise<SupplyChainManagementCopHeader>;
  getSceCopHdrByBooking(bookingNumber: string): Promise<SupplyChainManagementCopHeader[]>;
  getSceCopHdrByBookingAndContainer(bookingNumber: string, containerNumber: string): Promise<SupplyChainManagementCopHeader>;
  getSceCopDtls(copNo: string): Promise<SupplyChainManagementCopDetail[]>;
  getSceCopDtlsByBooking(bookingNumber: string): Promise<SupplyChainManagementCopDetail[]>;
  getSceCopDtlsByBookingAndContainer(bookingNumber: string, containerNumber: string): Promise<SupplyChainManagementCopDetail[]>;
  getCopDltByBkgClzTm(bkgNo: string, clzYdCd: string, actCd: string[]): Promise<SupplyChainManagementCopDetail[]>;
}
