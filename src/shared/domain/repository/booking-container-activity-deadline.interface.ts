import { BookingContainerActivityDeadline } from '../entity';

export const BookingContainerActivityDeadlineRepositoryName = 'BookingContainerActivityDeadlineRepository.Interface';
export interface BookingContainerActivityDeadlineRepository {
  findValidBy(activityId: string): Promise<BookingContainerActivityDeadline>;
  save(deadline: BookingContainerActivityDeadline | BookingContainerActivityDeadline[]): Promise<boolean>;
}
