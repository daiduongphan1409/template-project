import { BookingContainer } from '../entity';

export const ContainerDomainEventEstimateRepositoryName = 'ContainerDomainEventEstimateRepository.Interface';

export interface ContainerDomainEventEstimateRepository {
  saveBookingContainer(data: BookingContainer);
}
