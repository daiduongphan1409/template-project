import { BookingContainerActivityDeadlineHistories } from '../entity';

export const BookingContainerActivityDeadlineHistoriesRepositoryName = 'BookingContainerActivityDeadlineHistoriesRepository.Interface';
export interface BookingContainerActivityDeadlineHistoriesRepository {
  save(history: BookingContainerActivityDeadlineHistories | BookingContainerActivityDeadlineHistories[]): Promise<boolean>;
}
