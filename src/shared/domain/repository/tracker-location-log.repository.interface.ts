import { TrackerLocationEntity } from '../entity/tracker-location.entity';

export const TrackerLocationLogRepositoryName = 'TrackerLocationLogRepository.Interface';

export interface TrackerLocationLogRepository {
  saveRange(data: TrackerLocationEntity[]): Promise<TrackerLocationEntity[]>;
}
