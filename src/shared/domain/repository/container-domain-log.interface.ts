import { ContainerDomainLog } from '../entity/container-domain-log.entity';

export const ContainerDomainLogRepositoryName = 'ContainerDomainLogRepositoryName.Interface';

export interface ContainerDomainLogRepository {
  save(log: ContainerDomainLog): Promise<ContainerDomainLog>;
}
