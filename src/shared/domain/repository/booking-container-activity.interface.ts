import { BookingContainerActivity } from '../entity';

export const BookingContainerActivityRepositoryName = 'BookingContainerActivityRepository.Interface';
export interface BookingContainerActivityRepository {
  findValidBy(bookingNumber: string, containerNumber: string, matrixId: string): Promise<BookingContainerActivity>;
  save(activity: BookingContainerActivity | BookingContainerActivity[]): Promise<boolean>;
}
