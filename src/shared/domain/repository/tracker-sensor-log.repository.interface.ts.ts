import { TrackerSensorEntity } from '../entity/tracker-sensor.entity';

export const TrackerSensorLogRepositoryName = 'TrackerSensorLogRepositoryName.Interface';

export interface TrackerSensorLogRepository {
  save(data: TrackerSensorEntity): Promise<TrackerSensorEntity>;
  saveRange(data: TrackerSensorEntity[]): Promise<TrackerSensorEntity[]>;
}
