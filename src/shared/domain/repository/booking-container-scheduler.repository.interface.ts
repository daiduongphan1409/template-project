import { SchedulerEntity } from '../entity/scheduler.entity';

export const BookingContainerSchedulerRepositoryName = 'BookingContainerSchedulerRepositoryName.Interface';

export interface BookingContainerSchedulerRepository {
  save(scheduler: SchedulerEntity): Promise<SchedulerEntity>;
  getScheduler(bookingId: string, containerId: string): Promise<SchedulerEntity>;
  getSchedulerById(schedulerId: string): Promise<SchedulerEntity>;
}
