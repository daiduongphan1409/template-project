import { EventMatrixFilter, EventMatrixLogic } from '../entity';

export const ContainerDomainConfigRepositoryName = 'ContainerDomainConfigRepository.Interface';

export interface ContainerDomainConfigRepository {
  getAllMatrixFilter(): Promise<EventMatrixFilter[]>;
  getAllMatrixLogic(): Promise<EventMatrixLogic[]>;
}
