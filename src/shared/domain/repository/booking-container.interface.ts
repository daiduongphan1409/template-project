import { BookingContainer } from '../entity';

export const BookingContainerRepositoryName = 'BookingContainerRepository.Interface';
export interface BookingContainerRepository {
  getEvent(bkgNo: string, cntrNo: string, copNo: string, matrixIds: string[]): Promise<BookingContainer>;
  isExist(bookingNumber: string, containerNumber: string): Promise<boolean>;
  save(data: BookingContainer | BookingContainer[]): Promise<boolean>;
}
