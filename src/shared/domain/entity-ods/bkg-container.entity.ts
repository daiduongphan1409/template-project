import { IEntity } from '../model/seedwork/entity.interface';

export class BkgContainer implements IEntity {
  BKG_NO: string;
  CNTR_NO: string;
  CNTR_TPSZ_CD: string;
  CNMV_YR: string;
  CNMV_ID_NO: number;
  CNMV_CYC_NO: number;
  CNMV_STS_CD: string;
  PCK_TP_CD: string;
  PCK_QTY: number;
  WGT_UT_CD: string;
  CNTR_WGT: number;
  MEAS_UT_CD: string;
  MEAS_QTY: number;
  CNTR_VOL_QTY: number;
  UPD_USR_ID: string;
  UPD_DT: Date;
  CRE_USR_ID: string;
  CRE_DT: Date;

  constructor(init?: Partial<BkgContainer>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BkgContainer)) return false;

    return this.BKG_NO === entity.BKG_NO && this.CNTR_NO === entity.CNTR_NO;
  }
}
