import { IEntity } from '../model/seedwork/entity.interface';

export class BookingClzTime implements IEntity {
  BKG_NO: string;
  CLZ_TP_CD: string;
  CLZ_YD_CD: string;
  MNL_SET_DT: Date;
  NTC_FLG: string;
  SYS_SET_DT: Date;
  MNL_SET_USR_ID: string;
  CRE_USR_ID: string;
  CRE_DT: Date;

  constructor(init?: Partial<BookingClzTime>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingClzTime)) return false;

    return this.BKG_NO === entity.BKG_NO && this.BKG_NO === entity.BKG_NO;
  }
}
