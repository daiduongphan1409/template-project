import { IEntity } from '../model/seedwork/entity.interface';
import { SupplyChainManagementCopHeader } from './sce-cop-hdr.entity';

export class SupplyChainManagementCopDetail implements IEntity {
  COP_NO: string;
  COP_DTL_SEQ: string;
  PLN_DT: string;
  ACT_DT: Date;
  ACT_CD: string;
  ESTM_DT: string;
  SKD_VOY_NO: string;
  SKD_DIR_CD: string;
  CLPT_IND_SEQ: string;
  VPS_PORT_CD: string;
  NOD_CD: string;
  VSL_CD: string;
  CRE_USR_ID: string;
  CRE_DT: Date;

  header: SupplyChainManagementCopHeader;

  constructor(init?: Partial<SupplyChainManagementCopDetail>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof SupplyChainManagementCopDetail)) return false;

    return this.COP_NO === entity.COP_NO && this.COP_NO === entity.COP_NO;
  }
}
