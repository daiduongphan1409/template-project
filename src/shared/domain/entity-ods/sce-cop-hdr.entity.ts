import { IEntity } from '../model/seedwork/entity.interface';
import { SupplyChainManagementCopDetail } from './sce-cop-dtl.entity';

export class SupplyChainManagementCopHeader implements IEntity {
  COP_NO: string;
  BKG_NO: string;
  CNTR_NO: string;
  COP_STS_CD: string;

  details: SupplyChainManagementCopDetail[];

  constructor(init?: Partial<SupplyChainManagementCopHeader>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof SupplyChainManagementCopHeader)) return false;

    return this.COP_NO === entity.COP_NO;
  }
}
