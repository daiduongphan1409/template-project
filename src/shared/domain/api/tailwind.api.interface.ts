import { LocationInfo } from '../entity/api-response/location-info.entity';
import { SensorInfo } from '../entity/api-response/sensor-info.entity';

export const TailwindApiName = 'TailwindApi.Interface';

export interface TailwindApi {
  getSensorInfo(containerNo: string): Promise<SensorInfo[]>;
  getLocationInfo(containerNo: string): Promise<LocationInfo[]>;
}
