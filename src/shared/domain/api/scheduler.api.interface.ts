import { SchedulerCancelResponse } from 'src/opus-container-iot/dto/scheduler-cancel-respone.dto';
import { SchedulerRegisterResponse } from 'src/opus-container-iot/dto/scheduler-register-response.dto';
import { SchedulerRegisterRequest } from 'src/opus-container-iot/dto/scheduler-register.dto';

export const SchedulerApiName = 'SchedulerApi.Interface';

export interface SchedulerApi {
  register(input: SchedulerRegisterRequest): Promise<SchedulerRegisterResponse>;
  cancel(schedulerId: string): Promise<SchedulerCancelResponse>;
}
