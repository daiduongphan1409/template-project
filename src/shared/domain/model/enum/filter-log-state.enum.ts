export enum ContainerDomainLogState {
  NEW = 'NEW',
  INVALID = 'INVALID',
  REJECT = 'REJECT',
  ACCEPT = 'ACCEPT',
}
