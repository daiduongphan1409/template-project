export enum EventType {
  REGULAR_MILESTONE = 1,
  EXCEPTION = 2,
  PRE_ALERT = 3,
  EXCEPTION_SUMMARY = 4,
  PRE_ALERT_SUMMARY = 5,
}

export enum Category {
  TIMELINE = 1,
  UPCOMING_DEADLINES = 2,
  REEFER_IOT = 3,
  ROUTE_CHANGE = 4,
  OTHER_EXCEPTION = 5,
  LONG_STAYING = 6,
  SCHEDULE_CHANGE = 7,
  PREDICTIVE_ETA = 8,
}

export enum Level {
  BOOKING = 1,
  CONTAINER = 2,
}
export enum Bound {
  ORIGIN = 1,
  DESTINATION = 2,
  TRANSHIPMENT = 3,
  EMPTY_VALUE = 4,
}

export enum Domain {
  BOOKING = 1,
  CONTAINER = 2,
}

export enum LogicType {
  DEDICATED = 0,
  DYNAMIC = 1,
}

export enum Trigger {
  ACTUAL = 1,
  ESTIMATE = 2,
}

export enum TriggerType {
  ACTUAL = 1,
  ESTIMATED = 2,
  DEADLINE = 3,
  NOTIFICATION = 4,
  PRE_ALERT_ACTIVATION = 5,
  PRE_ALERT_DEACTIVATION = 6,
  EXCEPTION = 7,
  SUMMARY = 8,
}
