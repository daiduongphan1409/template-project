import { IEntity } from '../model/seedwork/entity.interface';

export class SchedulerEntity implements IEntity {
  id: string;
  type: number;
  bookingNo: string;
  containerNo: string;
  schedulerId: string;
  isValid: boolean;

  constructor(init?: Partial<SchedulerEntity>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof SchedulerEntity)) return false;

    return this.id === entity.id;
  }
}
