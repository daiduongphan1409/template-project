import { Trigger } from 'src/shared/domain/model/enum/matrix.enum';
import { IEntity } from '../model/seedwork/entity.interface';

export class EventMatrixFilter implements IEntity {
  id: number;
  matrixId: string;
  triggerId: Trigger;
  tableName: string;
  columnName: string;
  isValid: boolean;

  constructor(init?: Partial<EventMatrixFilter>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof EventMatrixFilter)) return false;

    return this.id === entity.id;
  }
}
