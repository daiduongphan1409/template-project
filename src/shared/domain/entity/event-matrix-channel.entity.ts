import { IEntity } from '../model/seedwork/entity.interface';

export class EventMatrixChannel implements IEntity {
  channelId: number;
  matrixId: string;
  templateId: string;
  isValid: number;

  constructor(init?: Partial<EventMatrixChannel>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof EventMatrixChannel)) return false;

    return this.channelId === entity.channelId;
  }
}
