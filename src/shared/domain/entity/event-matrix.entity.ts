import { Bound, Category, Domain, Level, EventType } from 'src/shared/domain/model/enum/matrix.enum';
import { IEntity } from '../model/seedwork/entity.interface';
import { EventMatrixChannel } from './event-matrix-channel.entity';
import { EventMatrixLogic } from './event-matrix-logic.entity';

export class EventMatrix implements IEntity {
  id?: string;
  name: string;
  description: string;
  eventType: EventType;
  category: Category;
  level: Level;
  bound: Bound;
  domain: Domain;
  edi315Status: string;
  opusCode: string;
  isValid: boolean;
  channels: EventMatrixChannel[];
  logic: EventMatrixLogic[];

  constructor(init?: Partial<EventMatrix>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof EventMatrix)) return false;

    return this.id === entity.id;
  }
}
