import { IEntity } from '../model/seedwork/entity.interface';
import { BookingContainerActivity } from './booking-container-activity.entity';

export class BookingContainerActivityDeadline implements IEntity {
  id: string;
  activityId: string;
  deadlineDate: Date;
  templateId: string;

  activity: BookingContainerActivity;

  constructor(init?: Partial<BookingContainerActivityDeadline>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivityDeadline)) return false;

    return this.id === entity.id;
  }
}
