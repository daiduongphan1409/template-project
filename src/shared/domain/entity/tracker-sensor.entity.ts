import { IEntity } from '../model/seedwork/entity.interface';
export class TrackerSensorEntity implements IEntity {
  id: string;
  bookingNo: string;
  containerNo: string;
  assetId: string;
  ambientTemperature?: number;
  o2?: number;
  returnAirTemperature?: number;
  supplyAirTemperature?: number;
  receivedDate: Date;
  whenCreated: Date;
  co2?: number;
  ethylene?: number;
  humidity?: number;
  nitrogen?: number;
  returnAir2Temperature?: number;
  supplyAir2Temperature?: number;
  airExchange?: number;
  co?: number;

  constructor(init?: Partial<TrackerSensorEntity>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof TrackerSensorEntity)) return false;

    return this.id === entity.id;
  }
}
