import { IEntity } from '../model/seedwork/entity.interface';
import { BookingContainerActivity } from './booking-container-activity.entity';

export class BookingContainer implements IEntity {
  bookingNo: string;
  containerNo: string;
  copNo: string;
  typeSizeCode: string;
  commodityCode: string;
  movementYear: string;
  movementId: number;
  movementCycleNo: number;
  movementStatus: string;
  packageUnitCode: string;
  packageQuantity: number;
  packageWeightUnitCode: string;
  packageWeight: number;
  measurementUnitCode: string;
  measurementQuantity: number;
  volumeQuantity: number;
  isValid: boolean;
  createdBy: string;
  createdAt: Date;
  updatedBy: string;
  updatedAt: Date;

  activities: BookingContainerActivity[];

  constructor(init?: Partial<BookingContainer>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainer)) return false;

    return this.bookingNo === entity.bookingNo && this.containerNo === entity.containerNo;
  }
}
