import { IEntity } from '../model/seedwork/entity.interface';
import { BookingContainerActivity } from './booking-container-activity.entity';

export class BookingContainerActivityEstimateHistories implements IEntity {
  id: string;
  activityId: string;
  estimate: Date;

  activity: BookingContainerActivity;

  constructor(init?: Partial<BookingContainerActivityEstimateHistories>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivityEstimateHistories)) return false;

    return this.id === entity.id;
  }
}
