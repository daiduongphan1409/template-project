import { IEntity } from '../../model/seedwork/entity.interface';

export class SensorInfo implements IEntity {
  assetId: string;
  whenCreated: Date;
  containerNo: string;
  bookingNo: string;
  temperatureSetpoint: number;
  ambientTemperature?: number;
  o2?: number;
  returnAirTemperature?: number;
  supplyAirTemperature?: number;
  co2?: number;
  ethylene?: number;
  humidity?: number;
  nitrogen?: number;
  returnAir2Temperature?: number;
  supplyAir2Temperature?: number;
  airExchange?: number;
  co?: number;
  receivedDate: Date;
  createdDate: Date;

  constructor(init?: Partial<SensorInfo>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof SensorInfo)) return false;

    return this.assetId === entity.assetId && this.whenCreated === this.whenCreated && this.containerNo === this.containerNo;
  }
}
