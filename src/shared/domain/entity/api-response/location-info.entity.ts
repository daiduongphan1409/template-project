import { IEntity } from '../../model/seedwork/entity.interface';

export class LocationInfo implements IEntity {
  assetId: string;
  whenCreated: Date;
  containerNo: string;
  bookingNo: string;
  longitude?: number;
  latitude?: number;
  receivedDate: Date;
  createdDate: Date;
  zoneId?: string;
  heading?: number;
  altitude?: number;

  constructor(init?: Partial<LocationInfo>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof LocationInfo)) return false;

    return this.assetId === entity.assetId && this.whenCreated === this.whenCreated && this.containerNo === this.containerNo;
  }
}
