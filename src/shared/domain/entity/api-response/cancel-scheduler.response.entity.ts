import { randomUUID } from 'crypto';
import { IEntity } from '../../model/seedwork/entity.interface';

export class CancelSchedulerResponse implements IEntity {
  id?: string;

  constructor(title: string, id?: string) {
    this.id = id ?? randomUUID();
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof CancelSchedulerResponse)) return false;

    return this.id === entity.id;
  }
}
