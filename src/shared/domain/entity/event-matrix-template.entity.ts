import { IEntity } from '../model/seedwork/entity.interface';

export class EventMatrixTemplate implements IEntity {
  id: string;
  template: string;
  isValid: number;

  constructor(init?: Partial<EventMatrixTemplate>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof EventMatrixTemplate)) return false;

    return this.id === entity.id;
  }
}
