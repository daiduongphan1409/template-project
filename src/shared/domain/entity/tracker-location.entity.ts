import { IEntity } from '../model/seedwork/entity.interface';

export class TrackerLocationEntity implements IEntity {
  id: string;
  bookingNo: string;
  containerNo: string;
  assetId: string;
  longitude?: number;
  whenCreated: Date;
  latitude?: number;
  receivedDate: Date;
  zoneId?: string;
  heading?: number;
  altitude?: number;

  constructor(init?: Partial<TrackerLocationEntity>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof TrackerLocationEntity)) return false;

    return this.id === entity.id;
  }
}
