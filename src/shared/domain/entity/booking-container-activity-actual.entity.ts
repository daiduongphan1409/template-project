import { IEntity } from '../model/seedwork/entity.interface';

export class BookingContainerActivityActual implements IEntity {
  id: string;
  activityId: string;
  actualDate: Date;
  templateId: string;

  constructor(init?: Partial<BookingContainerActivityActual>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivityActual)) return false;

    return this.id === entity.id;
  }
}
