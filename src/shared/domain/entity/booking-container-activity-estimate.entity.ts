import { IEntity } from '../model/seedwork/entity.interface';
import { BookingContainerActivity } from './booking-container-activity.entity';

export class BookingContainerActivityEstimate implements IEntity {
  id: string;
  activityId: string;
  estimateDate: Date;
  templateId: string;
  planDate: Date;

  activity: BookingContainerActivity;

  constructor(init?: Partial<BookingContainerActivityEstimate>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivityEstimate)) return false;

    return this.id === entity.id;
  }
}
