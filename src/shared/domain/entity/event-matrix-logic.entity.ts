import { LogicType, TriggerType } from '../model/enum/matrix.enum';
import { IEntity } from '../model/seedwork/entity.interface';
import { EventMatrixFilter } from './event-matrix-filter.entity';

export class EventMatrixLogic implements IEntity {
  logicType: LogicType;
  triggerId: TriggerType;
  matrixId: string;
  description: string;
  script: string;
  isValid: boolean;
  filters: EventMatrixFilter[];

  constructor(init?: Partial<EventMatrixLogic>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof EventMatrixLogic)) return false;

    return this.logicType === entity.logicType;
  }
}
