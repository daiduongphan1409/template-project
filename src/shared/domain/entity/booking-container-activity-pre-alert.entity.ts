import { IEntity } from '../model/seedwork/entity.interface';

export class BookingContainerActivityPreAlert implements IEntity {
  id: string;
  activityId: string;
  templateId: string;
  status: number;

  constructor(init?: Partial<BookingContainerActivityPreAlert>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivityPreAlert)) return false;

    return this.id === entity.id;
  }
}
