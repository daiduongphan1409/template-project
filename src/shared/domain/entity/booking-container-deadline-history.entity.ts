import { IEntity } from '../model/seedwork/entity.interface';
import { BookingContainerActivity } from './booking-container-activity.entity';

export class BookingContainerActivityDeadlineHistories implements IEntity {
  id: string;
  activityId: string;
  deadlineDate: Date;

  activity: BookingContainerActivity;

  constructor(init?: Partial<BookingContainerActivityDeadlineHistories>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivityDeadlineHistories)) return false;

    return this.id === entity.id;
  }
}
