import { IEntity } from '../model/seedwork/entity.interface';

export class BookingContainerActivityException implements IEntity {
  id: string;
  activityId: string;
  templateId: string;

  constructor(init?: Partial<BookingContainerActivityException>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivityException)) return false;

    return this.id === entity.id;
  }
}
