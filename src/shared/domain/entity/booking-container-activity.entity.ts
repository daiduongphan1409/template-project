import { IEntity } from '../model/seedwork/entity.interface';
import { BookingContainerActivityDeadline } from './booking-container-activity-deadline.entity';
import { BookingContainerActivityEstimate } from './booking-container-activity-estimate.entity';
import { BookingContainerActivityDeadlineHistories } from './booking-container-deadline-history.entity';
import { BookingContainerActivityEstimateHistories } from './booking-container-estimate-history.entity';
import { BookingContainer } from './booking-container.entity';

export class BookingContainerActivity implements IEntity {
  id: string;
  bookingNo: string;
  containerNo: string;
  copNo: string;
  copSeq: number;
  eventMatrixId: string;
  ediCode: string;
  opusCode: string;
  vesselCode: string;
  countryId: string;
  locationId: string;
  isValid: number;
  skdVoyNo: string;
  skdDirCd: string;
  clptIndSeq: string;
  vpsPortCd: string;
  clsCd: string;

  bookingContainer: BookingContainer;
  activitydeadLines: BookingContainerActivityDeadline[];
  activityEstimates: BookingContainerActivityEstimate[];

  activityDeadLineHistories: BookingContainerActivityDeadlineHistories[];
  activityEstimateHistories: BookingContainerActivityEstimateHistories[];

  constructor(init?: Partial<BookingContainerActivity>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof BookingContainerActivity)) return false;

    return this.id === entity.id;
  }
}
