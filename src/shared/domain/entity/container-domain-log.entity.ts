import { EvaluationResult, EventMatrixDto } from 'src/opus-container-domain/dto';
import { ContainerDomainLogState } from '../model/enum/filter-log-state.enum';
import { IEntity } from '../model/seedwork/entity.interface';

export class ContainerDomainLog implements IEntity {
  id: string;

  eventId: string;

  matrixMatch: EvaluationResult[];

  matrixPotential: EventMatrixDto[];

  createdTime: Date;

  updatedTime: Date;

  state: ContainerDomainLogState;
  containerLog: Promise<EvaluationResult[]>;

  public constructor(init?: Partial<ContainerDomainLog>) {
    Object.assign(this, init);
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof ContainerDomainLog)) return false;

    return this.id === entity.id;
  }
}
