export const enum TableNames {
  BKG_CLZ_TM = 'BKG_CLZ_TM',
  SCE_COP_DTL = 'SCE_COP_DTL',
  SCE_COP_DTLS = 'SCE_COP_DTLS',
  SCE_COP_HDR = 'SCE_COP_HDR',
}

export const enum ColumnNames {
  SYS_SET_DT = 'SYS_SET_DT',
  MNL_SET_DT = 'MNL_SET_DT',
  BKG_NO = 'BKG_NO',
  CLZ_TP_CD = 'CLZ_TP_CD',
  NTC_FLG = 'NTC_FLG',
  CLZ_YD_CD = 'CLZ_YD_CD',
  COP_NO = 'COP_NO',
  ACT_CD = 'ACT_CD',
  ESTM_DT = 'ESTM_DT',
  PLN_DT = 'PLN_DT',
  NOD_CD = 'NOD_CD',
  SKD_VOY_NO = 'SKD_VOY_NO',
  SKD_DIR_CD = 'SKD_DIR_CD',
  CLPT_IND_SEQ = 'CLPT_IND_SEQ',
  VPS_PORT_CD = 'VPS_PORT_CD',
  COP_DTL_SEQ = 'COP_DTL_SEQ',
  CNTR_NO = 'CNTR_NO',
  EVENT_ID = 'EVENT_ID',
  VSL_CD = 'VSL_CD',
}

export const enum Common {
  Vars = 'vars',
}

export const enum DataEventEstimate {
  eventID = 'eventID',
  bookingNumber = 'bookingNumber',
  containerNumber = 'containerNumber',
  copNumber = 'copNumber',
  estimatedDate = 'estimatedDate',
  plannedDate = 'plannedDate',
  scheduleVoyageNumber = 'scheduleVoyageNumber',
  actualFilingScheduleDirectionCode = 'actualFilingScheduleDirectionCode',
  callingPortIndicatorSequence = 'callingPortIndicatorSequence',
  vpsPortCode = 'vpsPortCode',
  copDetailSequence = 'copDetailSequence',
  actualCode = 'actualCode',
  vesselCode = 'vesselCode',
  nodeCode = 'nodeCode',
}
