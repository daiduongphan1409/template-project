// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getObjectProperty = (key: string, data: any) => {
  if (key in data && data[key]) {
    return data[key];
  }
  return null;
};

export { getObjectProperty };
