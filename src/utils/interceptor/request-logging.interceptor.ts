import { CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import chalk = require('chalk');

@Injectable()
export class RequestLoggingInterceptor implements NestInterceptor {
  // eslint-disable-next-line
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const parentType = chalk.hex('#87e8de').bold(`${context.getArgs()[0].route.path}`);
    const fieldName = chalk.hex('#87e8de').bold(`${context.getArgs()[0].route.stack[0].method}`);
    return next.handle().pipe(
      tap(() => {
        Logger.debug(`⛩  ${parentType} » ${fieldName}`, 'RESTful');
      }),
    );
  }
}
