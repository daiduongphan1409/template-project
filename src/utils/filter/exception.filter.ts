import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InternalException, ValidateException } from 'src/shared/domain/model/exception';
import { ApiResponse } from '../api.response';
import { ErrorCode } from '../error.code';

/**
 * Http Error Filter.
 * Gets an HttpException in code and creates an error response
 */
@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter<HttpException> {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const statusCode = exception.getStatus();

    if (statusCode !== HttpStatus.UNPROCESSABLE_ENTITY)
      response.status(statusCode).json({
        code: ErrorCode.UNKNOWN,
        message: exception.message,
        data: null,
      });

    // eslint-disable-next-line
    const exceptionResponse: any = exception.getResponse();
    Logger.error(exceptionResponse.message, exception.stack, 'Exception Handler');

    response.status(statusCode).json({
      code: ErrorCode.UNKNOWN,
      message: exceptionResponse.message,
      data: null,
    } as ApiResponse<void>);
  }
}

/**
 * Validate exception filter.
 * Gets an ValidateException in code and creates an error response
 */
@Catch(ValidateException)
export class ValidateExceptionFilter implements ExceptionFilter<ValidateException> {
  catch(exception: ValidateException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    response.status(400).json({
      code: exception.code,
      message: exception.message,
      data: null,
    } as ApiResponse<void>);
  }
}

/**
 * Internal exception filter
 * Gets an InternalException in code and creates an error response
 */
@Catch(InternalException)
export class InternalExceptionFilter implements ExceptionFilter<InternalException> {
  catch(exception: InternalException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    Logger.error('Server error', exception.stack, 'Exception Handler');

    response.status(500).json({
      code: exception.code,
      message: exception.message,
      data: null,
    } as ApiResponse<void>);
  }
}
