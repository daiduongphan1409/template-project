import { HttpExceptionFilter, InternalExceptionFilter, ValidateExceptionFilter } from './exception.filter';

describe('ExceptionFilter', () => {
  it('HTTP exception filter should be defined', () => {
    expect(new HttpExceptionFilter()).toBeDefined();
  });
  it('Validate exception filter should be defined', () => {
    expect(new ValidateExceptionFilter()).toBeDefined();
  });
  it('Internal exception filter should be defined', () => {
    expect(new InternalExceptionFilter()).toBeDefined();
  });
});
