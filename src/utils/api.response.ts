import { ErrorCode } from './error.code';

export class ApiResponse<T> {
  code: ErrorCode;
  message: string;
  data: T;

  constructor(data?: T) {
    this.code = ErrorCode.SUCCESS;
    this.message = 'Success';
    this.data = data;
  }

  public static success<U>(data?: U): ApiResponse<U> {
    return {
      code: ErrorCode.SUCCESS,
      message: 'Success',
      data: data,
    } as ApiResponse<U>;
  }
}
