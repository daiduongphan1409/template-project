/* istanbul ignore file */
import { ConfigService } from '@nestjs/config';
import { ClientProvider, Transport } from '@nestjs/microservices';
import { EnvironmentVariables } from './config/env.validation';

export const KAFKA_CLIENT = 'KAFKA_DEFAULT_CLIENT';

export function getKafkaConfigs(configService: ConfigService<EnvironmentVariables>) {
  const sasl =
    configService.get('KAFKA_DEFAULT_MECHANISM') === 'NONE'
      ? null
      : {
          mechanism: configService.get('KAFKA_DEFAULT_MECHANISM'),
          username: configService.get('KAFKA_DEFAULT_USERNAME'),
          password: configService.get('KAFKA_DEFAULT_PASSWORD'),
        };

  return {
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: configService.get('KAFKA_DEFAULT_CLIENT_ID'),
        brokers: [configService.get('KAFKA_DEFAULT_BROKER_URL')],
        ssl: configService.get('KAFKA_DEFAULT_SSL', false, { infer: true }),
        sasl,
      },
      consumer: {
        groupId: configService.get('KAFKA_DEFAULT_GROUP_ID'),
        allowAutoTopicCreation: configService.get('KAFKA_DEFAULT_AUTO_CREATE_TOPIC', true, { infer: true }),
      },
    },
  };
}

export function configKafkaEventFactory(configService: ConfigService<EnvironmentVariables>): ClientProvider {
  return {
    ...getKafkaConfigs(configService),
  } as ClientProvider;
}
