export function encodeBase64(original: string) {
  return Buffer.from(original, 'binary').toString('base64');
}

export function createBasicAuthHeader(username: string, password: string) {
  const encodedAccessToken = encodeBase64(username + ':' + password);

  const headersRequest = {
    'Content-Type': 'application/json',
    Authorization: 'Basic ' + encodedAccessToken,
    apikey: username,
    Accept: 'application/json',
  };

  return headersRequest;
}

export function createBearerAuthHeader(username: string, token: string) {
  return {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + token,
    apikey: username,
    Accept: 'application/json',
  };
}
