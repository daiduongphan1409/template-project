import type { Config } from 'jest';

const config: Config = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.*\\.spec\\.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  collectCoverageFrom: ['**/*.(t|j)s'],
  coverageDirectory: '../report/coverage',
  testEnvironment: 'node',
  moduleDirectories: ['<rootdir>/src', 'node_modules'],
  moduleNameMapper: {
    '^src/(.*)$': '<rootDir>/$1',
  },
  coveragePathIgnorePatterns: [
    '<rootDir>/shared/postgres/*',
    '<rootDir>/opus-container-domain/opus-container-domain.module.ts',
    '<rootDir>/opus-container-domain/matrix-processor/matrix-processor.module.ts',
    '<rootDir>/opus-container-domain/matrix-processor/dynamic/post-action/script/*',
    '<rootDir>/opus-container-domain/matrix-processor/dynamic/script/base-file/*',
    '<rootDir>/shared/repository/repository.module.ts',
    '<rootDir>/dist/',
    '<rootDir>/node_modules/',
    '<rootDir>/shared/mongo/*',
    '<rootDir>/shared/api/*',
    '.entity.ts',
    '<rootDir>/utils/*',
    '.module.ts',
    '.dto.ts',
  ],
  coverageThreshold: {
    global: {
      lines: 40,
    },
  },
};

export default config;
